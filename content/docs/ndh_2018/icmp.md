+++
title = "ICMP"
description = "NDH 2018 - Forensic (50 pts)"
keywords = "Forensic, ICMP, Exfiltration, PCAP, tshark, wireshark, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-07-01T13:27:32+01:00
weight = 20
draft = false
bref = "NDH 2018 - Forensic (50 pts)"
toc = true
aliases = [
    "/docs/ndh_2018_icmp",
]
+++

NDH 2018 - ICMP
===============

| Event           | Challenge | Category | Points | Solves      |
| --------------- | --------- | -------- | ------ | ----------- |
| Nuit du hack 16 | ICMP      | Forensic | 50     | ¯\\_(ツ)_/¯ |

### Description

This forensic / network challenge is made by <a href="https://twitter.com/XCtzn">WorldCitizen</a> for the 16th edition of Nuit du hack (NDH). In this task we are going to deal with ICMP packet in a pcap.
You can find the ressource here: [analysis.pcap](/files/ndh_2018/icmp/analysis.pcap)

### TL;DR

As I said before, we got a pcap file with approximatly 50 icmp packets. In each packet we can see base64 encoded data.

Then with a little on-liner based on `tshark`, I was able to get the flag :-)

### Extraction

So, I start with this pcap:

<center>![](/img/ndh_2018/icmp/figure1.png)

__Fig 1 - ICMP packet with base64 encoded data__
</center>

I just wanted to get all `icmp request`, so to do that I... Filtered on IP source instead of read the manual :-D

But it worked with this command:

```bash
tshark -2 -r analysis.pcap -R "ip.src == 192.168.1.24" -T fields -e data | xxd -r -p
```



<center>![](/img/ndh_2018/icmp/figure2.png)
__Fig 2 - Get the base64 !__
</center>

The `xxd` command is here to unhex the content. In fact, tshark will give the hexadecimal value of the field data.

### Flag

Then, time to flag:

```bash
tshark -2 -r analysis.pcap -R "ip.src == 192.168.1.24" -T fields -e data | xxd -r -p | base64 -d | grep 'ndh'
```


<center>![](/img/ndh_2018/icmp/figure3.png)
__Fig 3 - w00t \o/__
</center>

> ndh2k18_017395f4c6312759

Maki
