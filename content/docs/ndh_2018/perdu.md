+++
title = "Perdu"
description = "NDH 2018 - Forensics (100 pts)"
keywords = "Forensic, PCAP, DNS, ICMP, wireshark, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-07-01T18:15:21+02:00
weight = 20
draft = false
bref = "NDH 2018 - Forensics (100 pts)"
toc = true
aliases = [
    "/docs/ndh_2018_perdu",
]
+++

NDH 2018: Perdu
===============

| Event                    | Challenge    | Category      | Points | Solves      |
|--------------------------|--------------|---------------|--------|-------------|
| NDH 2018                 | Perdu        | Forensics     | 100    |             |

Download: [perdu.pcap](/files/ndh_2018/perdu/perdu.pcap)

### TL;DR

This challenge consists in the analysis of a PCAP file containing containing evidence of a potential data exfiltration.

If you are not interested in the detailed answer to the question "How to solve this problem?",
the short answer is that using network attributes such as source ports, DNS names or ICMP loads
to try to hide and exfiltrate data is not a good idea since improper use can quickly lead to side effects
which can reduce or cancel our coverage.

The task was not that complex, but I thought sharing my PCAP analysis approach might be worthwhile.

### Methology

Reading the description of the challenge, we are informed that data are potentially hidden in data exchanges
that have been captured by a probe (PCAP file).

Assuming it's a real case, the probability of retrieving data from client queries is higher than from server responses.

Let's open the PCAP file on Wireshark:

<center>![pcap_file_wireshark_packets](/img/ndh_2018/perdu/pcap_file_wireshark_packets.png)</center>

OK, at first glance, the PCAP file only contain HTTP exchanges with the `perdu.com:80` HTTP server:

<center>![pcap_file_wireshark_not_http](/img/ndh_2018/perdu/pcap_file_wireshark_not_http.png)</center>

All the HTTP requests are tipically the same and always returns the same response:

```http
GET / HTTP/1.1
Host: perdu.com

HTTP/1.1 200 OK
Date: Tue, 19 Jun 2018 03:24:06 GMT
Server: Apache
Last-Modified: Thu, 02 Jun 2016 06:01:08 GMT
ETag: "cc-5344555136fe9"
Accept-Ranges: bytes
Content-Length: 204
Vary: Accept-Encoding
Content-Type: text/html

<html><head><title>Vous Etes Perdu ?</title></head><body><h1>Perdu sur l'Internet ?</h1><h2>Pas de panique, on va vous aider</h2><strong><pre>    * <----- vous &ecirc;tes ici</pre></strong></body></html>
```

But, if we analyze carefully the TCP responses of the HTTP server, we can quickly identify some suspicious responses:

```
A new tcp session is started with the same ports as an earlier session in this trace
```

This message is very interesting in such a context! In fact, if we consider that many requests are made
on the same server: source IP address, destination port and destination IP address are tipically the same,
while, the source port is calculated randomly to allow the network client to perform several parallel requests and avoid packet loss.

Looking closer at the source ports, we can quickly identify that all the ports are in the range [4000-4999].
Since we're looking for exfiltered data it's likely that it has been transmitted through these source ports using ASCII values.

Let's try to get data from these source ports by removing the leading `4` and only getting the ports that are associated with the `GET /` HTTP requests
using [this script](/files/ndh_2018/perdu/perdu.py):

```
   ==Phrack Inc.==

                    Volume One, Issue 7, Phile 3 of 10

=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
The following was written shortly after my arrest...

                       \/\The Conscience of a Hacker/\/

                                      by

                               +++The Mentor+++

                          Written on January 8, 1986
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

        Another one got caught today, it's all over the papers.  "Teenager
Arreested in Commputer Crime Scandal", "Hacker Arrested after Bank Tamperiing"...
        Damn kids.  They're all alike.

        But did you, in your three-piece psychology and 1950's technobrain,
ever take a look behind the eyes of the hacker?  Did you ever wonder what
made him tick, what forces shaped him, what may haave molded him?
        I am a hacker, enter my world...
        Mine is a world that begins with school... I'm smarter than most of
the other kidss, this crap they teach us bores me...
        Damn underachieever.  They're all alike.

        I'm in junior high or high school.  I've listened to teacherrs explain
for the fifteenth time how to reduce a fraction.  I understand it.  "No, Ms.
Smith, I didn't show my wwork.  I did it in my head...."
        Damn kid.  Probably copied it.  They're all alike.

        I made a discovery today.  I found a computer.  Wait a second, this is
cool.  It does what I want it to.  If it makes a mistake, it's because I
screwed it up.  Not because it doesn't like me...
                Or feels threatened by me...
                Or thinks I'm a smart ass...
                Or doesn't like teaching and shouldn't be here...
        Damn kid.  All he does is play games.  They're all alike.

        And then it happened... a door opened to a world... rushing through
the phone line like heroin through an addict's veins, an electronic pulse is
sent out, a refuge from the day-to-day incompetencies is sought... a board is
found.
        "This is it... this is where I belong..."
        I know everyone here... even if I've never met them, never talked to
them, may never hear from them again... I know you all...
        Damn kid.  Tying up the phone line again.  They're all alike...

        You bet your ass we're all alike... we've been spoon-fed baby food at
school when we hungered for steak... the bits of meat that you did let slip
through were pre-chewed and tasteless.  We've been dominated by sadists, or
ignored by the apathetic.  The few that had something to teach found us will-
ing pupils, but those few are like drops of water in the desert.

        This is our world now... the world of the electron and the switch, the
beauty of the baud.  We make use of a service already existing without paying
for what could be dirt-cheap if it wasn't run by profiteering gluttons, and
you call us criminals.  We explore... and you call us criminals.  We seek
after knowledge... and you call us criminals.  We exist without skin color,
without nationality, without religious bias... and you call us criminals.
You build atomic bombs, you wage wars, you murder, cheat, and lie to us
and try to make us believe it's for our own good, yet we're the criminals.

        Yes, I am a criminal.  My crime is that of curiosity.  My crime is
that of judging people by what they say and think, not what they look like.
My crime is that of outsmarting you, something that you will never forgive me
for.

        I am a hacker, and this is my manifesto.  You may stop this individual,
but you can't stop us all... after all, we're all alike.

                               +++The Mentor+++
Well done : ndh16_{e132697f156befd669df0726f06ab338f0a225da3267661cfa9bb720161b2af9}
```

Final flag:


> ndh16_{e132697f156befd669df0726f06ab338f0a225da3267661cfa9bb720161b2af9}


Creased
