+++
title = "BreizhK0inM1n3r"
description = "BreizhCTF 2018 - Prog (100 pts)"
keywords = "Programming, Blockchain, Crypto, Bruteforce, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-04-23T07:08:25+01:00
weight = 20
draft = false
bref = "BreizhCTF 2018 - BreizhK0inM1n3r (100 pts)"
toc = true
aliases = [
    "/docs/breizhctf_2018_breizhk0inm1n3r",
]
+++

BreizhCTF 2018: BreizhK0inM1n3r
===============================

| Event          | Challenge       | Category    | Points | Solves |
|----------------|-----------------|-------------|--------|--------|
| BreizhCTF 2018 | BreizhK0inM1n3r | Programming | 200    | 6      |


### Description

> Bitcoin is dead... I mean almost dead!
>
> So SaxX decided to launch a new service!
>
> Time to earn some breizhcoins!

### TL;DR

This task is about providing a proof of work to a server that will handle it and
put data into a blockchain.

The fictive, but realistic context of a custom blockchain allows here to perform
a pre-calculation of hash that can be validated by the server.

### Methology

As soon as we connect to the server, we're given the following message:

```bash
# ncat 148.60.87.243 9200
[...]

Bitcoin is dead... I mean almost dead! So SaxX decided to launch a new service! Time to earn some breizhcoins!

Can you provide me 42 different addresses that start with 1337 after being hashed with SHA-512?!

Here is a valid address that I can accept:
 - 1337d2c7af10cfa4a401738258af2dd54298590cdd94f41be474619e1e82653e7627864f40fbce58f79d3ca7c1439d14076853b0a8500253eda20d58d7165643

Btw, don't be a jerk and try to submit the same input twice. I keep a close watch MOFO!

Enough and like we said in France: "En voiture Simone"!

Press <ENTER> to start
```

The task here is simple, we just need to compute SHA-512 hashes that will start with `1337`, for example:

<center>![proof of work](/img/breizhctf_2018/breizhk0inm1n3r/proof_of_work.png)</center>

### Vulnerability

The server does not use previous hashes to commit our proof of work, so we can
pre-calculate hashes and send them in one shot in a very short time.

### Exploitation

The exploit is simple, we just need to compute hashes and send them to the remote
server:

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pwn import *
import hashlib

DEBUG = False
hash_ = []
index = 0

# Compute 42 hashes
while len(hash_) != 42:
    hash_candid = str(hashlib.sha512(str(index)).hexdigest())
    if hash_candid.startswith('1337') and hash_candid not in hash_:
        hash_ += [index]
        if DEBUG:
            print('Length: {}'.format(len(hash_)))
            print(hash_)
    index += 1

# Connect to remote server
connection = remote('148.60.87.243', 9200)

# Start hash listener
data = connection.recvuntil('Press <ENTER> to start')
print(data)
connection.send('\n')

# Send hashes one by one
i = 1
for index in hash_:
    data = connection.recvuntil('Input {} > '.format(i), drop=False)
    if DEBUG: print(data)
    connection.send(str(index))
    i += 1

# Get flag
while 'BZHCTF' not in data:
    data = connection.recvuntil('}')
    print(data)
```

Here we got the flag!

<center>![exploit](/img/breizhctf_2018/breizhk0inm1n3r/exploit.png)</center>

Creased
