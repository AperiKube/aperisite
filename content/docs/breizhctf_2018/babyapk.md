+++
title = "BabyAPK"
description = "BreizhCTF 2018 - Mobile"
keywords = "Android, APK, Reverse, Reverse engineering, RE, Jadx, XOR, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-04-23T07:08:25+01:00
weight = 10
draft = false
bref = "BreizhCTF 2018 - Mobile"
toc = true
aliases = [
    "/docs/breizhctf_2018_babyapk",
]
+++

# BabyAPK - BreizhCtf 2018

When participating to BreizhCTF one of the categories was mobile application reversing. This write up is about reversing the first mobile application that was provided to us : **BabyAPK**

### Decompilation

To reverse Android application, I usually start by reversing the source code . For that i use 2 tools : **Jadx** and **APKTool**  when **Jadx** is not sufficient.

For this chall, I only used Jadx with the command
```bash
jadx BabyAPK.apk
```
Once this command executed desassembled file of this applications are located in the folder BabyApk.

### Finding the vulnerable feature

Once the disassembly done, I first looked in the `fr/breizhctf/saxx/babyapk` folder where the source Java file are located.

Here are 3 files :
- BuildConfig.java : which contains the config of the Apk
- LoginActivity.java : which contains the interesting part of the application
- R.java : which is used to stored layout object references.

In the `LoginActivity`, after scrolling a little we can see an interesting function named `isPasswordValid` taking a String as parameter and validating it :

```java
private boolean isPasswordValid(String password) {
    String v3 = password;
    int v0 = 0;
    if (v3.length() == 45) {
        for (int v1 = 0; v1 < "kmqgwg]Tm3=NE_#$%$#!&#^_^~/4ouKJW@WE^(:p@_*##".length(); v1++) {
            if (")79$#!&#^l\\t<v\\x00Q\\x17\\x11HOXyD2k:!\\x18\\x040@xy\\x089g0\\x01_\\t\\x1c#oGF^".charAt(v1) != ("kmqgwg]Tm3=NE_#$%$#!&#^_^~/4ouKJW@WE^(:p@_*##".charAt(v1) ^ v3.charAt(v1))) {
                v0 = 1;
                Toast.makeText(this, "Seems I don't recognize you! go out :(", 0).show();
                break;
            }
        }
        if (v0 != 0) {
            return true;
        }
        Toast.makeText(this, "Hey buddy! It's you, Welcome :)", 0).show();
    } else {
        Toast.makeText(this, "Seems I don't recognize you! go out :(", 0).show();
    }
    if (password.length() <= 4) {
        return false;
    }
    return true;
}
```

Here we can see that , in order to compare the password, the program xor a String with another and compare each char of it.

### Getting the password

In order to get the password, all you have to do is to get back the xored result between the first 45 characters of

 `kmqgwg]Tm3=NE_#$%$#!&#^_^~/4ouKJW@WE^(:p@_*##`

and

`)79$#!&#^l\t<v\x00Q\x17\x11HOXyD2k:!\x18\x040@xy\\x089g0\x01_\t\x1c#oGF^`

Here is my python script :

```python
from Crypto.Util import strxor
a = b"kmqgwg]Tm3=NE_#$%$#!&#^_^~/4ouKJW@WE^(:p@_*##"[:45]
b = b")79$#!&#^l\t<v\x00Q\x17\x11HOXyD2k:!\x18\x040@xy\x089g0\x01_\t\x1c#oGF^"[:45]
print(strxor.strxor(a,b))
```
And the flag is :

**BZHCTF{w3_4r3_r34lly_gl4d_70_533_y0u_w3lc0me}**

[@Areizen](https://twitter.com/RomainKraft)
