+++
title = "Memory"
description = "AceBear 2018 - Forensic / MISC (951 pts)"
keywords = "Forensic, Dump, PDF, Steganography, Spammimic, Chromevisits, Volatility, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-01-28T19:19:07+01:00
weight = 20
draft = false
bref = "AceBear 2018 - Forensic / MISC (951 pts)"
toc = true
aliases = [
    "/docs/acebear_2018_memory",
]
+++

AceBear 2018: Memory
====================

| Event                    | Challenge | Category      | Points | Solves |
|--------------------------|-----------|---------------|--------|--------|
| AceBear Security Contest | Memory    | Misc/Forensic | 951    | 8      |

Download: [Google Drive](https://drive.google.com/open?id=1Qlgh8XpW3Q3-tTU-CkqfAZ-84unLNWsU)

Author: f4k3r

### Description

This forensic / MISC challenge is based on a memory dump analysis. No statment, few hints.

### TL;DR

`Memory` is a Windows memory dump, quickly identified with `Auto_Vol`. In the `C:\Users\Administrator\Documents` there are 2 `rtf` files:

* document.rtf: Containing a spam message
* usc_command.rtf: Containing a writeup of "Just Keyp Trying" challenge from PicoCTF 2017.

After a quick search on internet, there is a steganography technique called `spammimic`. It uses spam to hide the real message.

To encode / decode spammimic message: http://www.spammimic.com/

After that I obtained a base64 string. It's a google sheet identifier (in the URL).

And the flag is in the sheet.

### State of the art

Well, I start this challenge with a memory dump: `dump.raw`
After using `Auto_Vol`, it's a __Windows 7 SP1 32 bits__ (Win7SP1x86_23418) dump.

`Auto_Vol` did those commands:

* [chromevisits](#chrome-history) (special plugin): https://github.com/superponible/volatility-plugins
* cmdscan
* filescan
* mft_parser
* iehistory
* netscan
* ...

In fact, we need `filescan`, `chromevisits` and possibly `mft_parser`.
Let's start with Chrome history.

### Chrome history

```bash
$ volatility --plugins=/home/maki/Documents/Tools/plug_vol/volatility-plugins/ -f dump.raw --profile=Win7SP1x86_23418 chromevisits
44     34 2018-01-18 02:35:49.648389            START_PAGE;CHAIN_START_END;                                             n/a               1969603 file:///C:/Users/Administrator/Documents/Ethical%20Hacking.pdf                   Ethical Hacking.pdf                                                                   4     0 2018-01-18 02:44:53.977979       N/A    
...
28     26 2018-01-18 02:15:34.688888            TYPED;CHAIN_START;                                                    2 n/a                     0 https://docs.google.com/                                                         Đăng nhập - Tài khoản Google                                                   1     1 2018-01-18 02:15:34.688888        N/A       
...
```

I only show interesting parts. Now, we know that there is a `PDF` and an opened `Google Docs` session.

After analyzing the `$MFT`, I saw a lot of Google cookies. At this point I didn't knew if Google services were important or not in the challenge. So, let's continu with the PDF.

### PDF analysis

I opened the `filescan` output:

```bash
$ cat output/cmd_windows/filescan | grep -i pdf
0x000000001ff241d8      8      0 R--rwd \Device\HarddiskVolume2\Users\Administrator\Documents\Ethical Hacking.pdf

$ mkdir tmp && volatility -f dump.raw --profile=Win7SP1x86_23418 dumpfiles 0x000000001ff241d8 -D tmp/
Volatility Foundation Volatility Framework 2.6
DataSectionObject 0x1ff241d8   None   \Device\HarddiskVolume2\Users\Administrator\Documents\Ethical Hacking.pdf

$ file file.None.0x850f5770.dat
file.None.0x850f5770.dat: PDF document, version 1.5
```

I took `peepdf` to analyze this file, I spent a big hour on it and  finally nothing...

### User files

At that time, I realized that I did a big mistake, my previous `grep` hides __all__ user files...

```bash
$ cat filescan | grep Desktop
[Nothing useful]
$ cat filescan | grep Documents
0x000000001de69ce8      8      0 R--rwd \Device\HarddiskVolume2\Users\Administrator\AppData\Roaming\Microsoft\Windows\Libraries\Documents.library-ms
0x000000001de6a730      8      0 R--rwd \Device\HarddiskVolume2\Users\Administrator\Documents\desktop.ini
0x000000001e0ed5d0      8      0 R--rwd \Device\HarddiskVolume2\Users\Administrator\Documents\document.rtf
0x000000001fcaca30      8      0 R--rwd \Device\HarddiskVolume2\Users\Administrator\Documents\usb_command.rtf
0x000000001fcbc4c8      7      0 R--r-d \Device\HarddiskVolume2\Users\Administrator\Documents\GIF.gif
0x000000001fccbf80      8      0 R--rwd \Device\HarddiskVolume2\Users\Administrator\Documents\hackerrank.py
0x000000001ff241d8      8      0 R--rwd \Device\HarddiskVolume2\Users\Administrator\Documents\Ethical Hacking.pdf
```

Then I extracted `hackerrank.py` (at this time, I recovered it with the mft_parser).

```python
from __future__ import print_function
if __name__ == '__main__':
    n = int(raw_input())
    print(*range(1, n+1), sep='')
```

=> Nothing interesting

Next file: `GIF.gif`:

With the hint "Steganography" we could think about this file (fortunately, an administrator told me that it was _text_ steganography).

<center>![nyan cat](/img/acebear_2018/memory/GIF.gif)</center>

And the interesting part: `RTF` files. I converted them in PDF (much more readable).

> usb_command.pdf

```text
tshark -r data.pcapng -T fields -e usb.capdata > data2.txt
 //trich xuat du lieu goi tin
tshark -r data.pcap -V
 // xem can goi tin
tshark -r data.pcap -V | grep “Left”
 // xem phan du lieu goi tin
flag[pr355-0nwards-3a10134e]c
flag[pr355_0nwards_3a10134e]
flag[pr355_0nwards_deafcb32]
FLAG{PR#%%_)NWARDS_#A!)!#$E}C
09 f F
0f l L
04 a A
0a g G
2f [ {
13 p P
15 r R
20 3 #
22 5 %
22 5 %
2d - _
27 0 )
11 n N
1a w W
04 a A
15 r R
07 d D
16 s S
2d - _
20 3 #
04 a A
1e 1 !
27 0 )
1e 1 !
20 3 #
21 4 $
08 e E
30 ] }
01
010006 c C
```

> document.pdf

```text
Dear Colleague , Your email address has been submitted
to us indicating your interest in our briefing ! If
you are not interested in our publications and wish
to be removed from our lists, simply do NOT respond
and ignore this mail . This mail is being sent in compliance
with Senate bill 2516 , Title 3 , Section 305 . THIS
IS NOT MULTI-LEVEL MARKETING ! Why work for somebody
else when you can become rich in 48 DAYS ! Have you
ever noticed more people than ever are surfing the
web and the baby boomers are more demanding than their
parents . Well, now is your chance to capitalize on
this ! WE will help YOU turn your business into an
E-BUSINESS & decrease perceived waiting time by 180%
! You can begin at absolutely no cost to you . But
don't believe us . Mrs Jones who resides in Rhode Island
tried us and says "I was skeptical but it worked for
me" ! This offer is 100% legal ! So make yourself rich
now by ordering immediately ! Sign up a friend and
you'll get a discount of 50% . Thank-you for your serious
consideration of our offer ! Dear Salaryman ; Thank-you
for your interest in our briefing ! If you no longer
wish to receive our publications simply reply with
a Subject: of "REMOVE" and you will immediately be
removed from our club ! This mail is being sent in
compliance with Senate bill 1627 ; Title 1 ; Section
301 . This is a ligitimate business proposal ! Why
work for somebody else when you can become rich as
few as 48 weeks ! Have you ever noticed more people
than ever are surfing the web plus nearly every commercial
on television has a .com on in it . Well, now is your
chance to capitalize on this . We will help you increase
customer response by 110% and turn your business into
an E-BUSINESS ! You are guaranteed to succeed because
we take all the risk . But don't believe us . Prof
Simpson of Pennsylvania tried us and says "I was skeptical
but it worked for me" ! We assure you that we operate
within all applicable laws . So make yourself rich
now by ordering immediately . Sign up a friend and
you'll get a discount of 80% ! Thanks ! Dear Friend
; This letter was specially selected to be sent to
you ! We will comply with all removal requests ! This
mail is being sent in compliance with Senate bill 1623
; Title 9 ; Section 303 . THIS IS NOT MULTI-LEVEL MARKETING
. Why work for somebody else when you can become rich
within 81 DAYS . Have you ever noticed how many people
you know are on the Internet plus nobody is getting
any younger . Well, now is your chance to capitalize
on this ! We will help you process your orders within
seconds and use credit cards on your website ! You
can begin at absolutely no cost to you . But don't
believe us . Mrs Anderson of Colorado tried us and
says "My only problem now is where to park all my cars"
! This offer is 100% legal ! Do not go to sleep without
ordering ! Sign up a friend and you get half off .
Warmest regards ! Dear Business person , We know you
are interested in receiving cutting-edge info ! If
you no longer wish to receive our publications simply
reply with a Subject: of "REMOVE" and you will immediately
be removed from our club ! This mail is being sent
in compliance with Senate bill 1623 ; Title 5 , Section
308 ! This is NOT unsolicited bulk mail . Why work
for somebody else when you can become rich inside 23
DAYS ! Have you ever noticed nobody is getting any
younger and society seems to be moving faster and faster
. Well, now is your chance to capitalize on this !
WE will help YOU deliver goods right to the customer's
doorstep and use credit cards on your website ! You
are guaranteed to succeed because we take all the risk
. But don't believe us ! Ms Anderson of Montana tried
us and says "Now I'm rich, Rich, RICH" ! We are a BBB
member in good standing ! If not for you then for your
loved ones - act now . Sign up a friend and you'll
get a discount of 80% . Best regards .
```

* usb_command.pdf: is a writeup of "Just Keyp Trying" challenge from PicoCTF 2017 (I made a writeup too: https://maki.bzh/writeup/just_keyp_picoctf2k16/).
* document.pdf: contains a spam email text.

### Spammimic

After few research on Internet, there is a steganography techniques based on spam: `spammimic`.
Then, I tried to decode my email on: http://www.spammimic.com/decode.shtml

It gaves me:
> 1gwiPCJHiIrnZAwLB9q9ztZKrD9tbq65YnoZzXALnh6c

Base64 is not working, the admin told me "It's not base64"... And suddenly

<center><iframe src="https://giphy.com/embed/3oriO5Z63sxZbUTOcE" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></center>

The `Google services` found in the Chrome history! Google uses base64 identifier in the URL for each document!
So I tried on `Google Doc`, nothing.

### Google Sheet

Then I tried on `Google Sheet` and... w00t!

> https://docs.google.com/spreadsheets/d/1gwiPCJHiIrnZAwLB9q9ztZKrD9tbq65YnoZzXALnh6c/edit#gid=0

<center>![google sheet](/img/acebear_2018/memory/sheet.png)<br />
__Fig 1 - Google sheet__</center>

We can see written in yellow in A2 cell the flag.

### Flag

And here is the flag:
> AceBear{y0u_l00k_v3ry_pr3tty!}

Maki
