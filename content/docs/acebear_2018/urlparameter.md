+++
title = "Urlparameter"
description = "AceBear 2018 - Web (100 pts)"
keywords = "Web, Robots, Debug, RCE, preg_match, bypass, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2018-01-28T16:51:28+01:00"
weight = 20
draft = false
bref = "AceBear 2018 - Web (100 pts)"
toc = true
aliases = [
    "/docs/acebear_2018_urlparameter",
]
+++

AceBear Security Contest 2018: Urlparameter
===========================================

<center>![AceBear Logo](/img/acebear_2018/urlparameter/acebear.png)</center>

This challenge was the first of the 4 proposed by AceBear at this contest.

### Challenge

We follow the given link (http://35.196.45.11:8080/) and it shows an empty page, looking at the source code is no better.
So what we do is checking for a robots.txt file which could give us any idea of what is going on...

<center>![Existence of robots.txt](/img/acebear_2018/urlparameter/pic1.png)<br />
__Fig 1 - A clue for using /?debug__</center>

It seems we can browse the GET parameter debug and it displays us part of (whole ?) index.php script.

<center>![Filters that we have to bypass](/img/acebear_2018/urlparameter/pic2.png)<br />
__Fig2 - Part of (whole ?) index.php script__</center>

From that code we understand that there is kind of hard filtering implementation that is being used.
If you browse for functions in php, majority of those contain "\_", "read", "open", "dir"...
Plus, there it's case insensitive and only the most right parameters combination (key/value) will be processed !

Not all functions are enabled, we can't use functions like "include", "require", "echo", "print", etc., if we do it warns us that way:

<center>![Behaviour when the function is not enabled](/img/acebear_2018/urlparameter/pic3.png)<br />
__Fig 3 - Function not found !!__</center>

Finally we understand from tests that only the printf function can be used to display contents and it can't be combined to get the result of functions such as pathinfo()...

### Exploit

We spent a lot of time trying to bypass the filters with unusuals functions, however we discovered that %hex encoding in a URL are being interpreted after the $\_SERVER variable is setting his values...

So basically we just could use a hex encoding to use functions that are filtered like "system" to bypass the preg_match function !
The flaw was using the $\_SERVER["REQUEST_URI"] instead of using the $\_GET parameters !

As you can see below, if we use /?system=ls ./ with a hex encoding of the system word, it bypass the preg_match and process the system function !

<center>![system("ls ./") displays us the directory's content](/img/acebear_2018/urlparameter/pic4.png)<br />
__Fig 4 - preg_match bypassed !__</center>

By now we can go straight to the flag by using the same hex encoding to process /?system=cat flag-a-long-name-that-you-wont-know.php
We now check the source code looking for any flag inside a php script and VOILA

<center>![Retrieving the flag with system("cat flag-a-long-name-that-you-wont-know.php")](/img/acebear_2018/urlparameter/pic5.png)<br />
__Fig 5 - Flag inside the source code__</center>

> Flag: AceBear{I_did_something_stupid_with_url}

\_ACKNAK\_
