+++
title = "MyIdol"
description = "AceBear 2018 - MISC (971 pts)"
keywords = "Forensic, Misc, Mr Robot, OSINT, Steganography, Spectrogram, DeepSound, RSA, QRCode, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-01-28T18:42:43+01:00
weight = 20
draft = false
bref = "AceBear 2018 - MISC (971 pts)"
toc = true
aliases = [
    "/docs/acebear_2018_myidol",
]
+++

AceBear Security Contest 2018: My idol
======================================

<a href="https://git.bmoine.fr/writeup/tree/master/acebear_security_contest_2018/my_idol#fr-version" target="\_blank">FR</a>

### Challenge details

| Event                    | Challenge | Category      | Points | Solves |
|--------------------------|-----------|---------------|--------|--------|
| AceBear Security Contest | My idol   | Misc/Forensic | 971    | 6      |

Download: [call_him.ape](/files/acebear_2018/myidol/call_him.ape), [Google Drive](https://drive.google.com/file/d/1m8qj06PBx1jt3VWhOWGGTHr1OAXVFdPh/view)

Author: [komang4130](https://komang4130.wordpress.com/)

### Description

> Trying to listen and maybe you know his name, and you'll know one of his secret weapons.
>
> Something that he deleted from Michael Hansen may help you get in.

### TL;DR

If you are not interested in the detailed answer to the question "How to solve this problem?",
the short answer is that using attributes of our daily life and/or the public domain
to secure access to our data is not a good idea.

Having quickly identified the author's interest in the series Mr. Robot, digging a little deeper,
we were able to (more or less quickly) identify processes and techniques
used by the author to conceal his data.

This is just a reflection within reach of those who would have taken the time to watch
at least the first episode of the series.

Morality: Watch [Mr. Robot](http://www.usanetwork.com/mrrobot)!

### Methology

Reading the [description of the challenge](#description), we are advised that by listening to the soundtrack,
we would be likely to find a person's name and one of its secret weapons.

Listening to the soundtack, we can recognize quite easily a trailer of the season 1 of Mr. Robot
published on [YouTube](https://www.youtube.com/watch?v=xIBiJ_SzJTA).

Having a few hours of watching and analysis of the Mr. Robot series, this first part seems to be trivial.
Elliot uses different ways to hide data, but let's start with the simplest way to avoid wasting time.

As this is an audio stream, let's use our must-have audio processing tool: [Audacity](http://www.audacityteam.org/).

The format is not recognized by Audacity, let's try to import it in RAW and display the spectrogram:

![spectrogram](/img/acebear_2018/myidol/spectrogram.png)

Nothing... Hence, this is not [spectrogram-based steganography](https://solusipse.net/blog/post/basic-methods-of-audio-steganography-spectrograms/).

Another technique used by Elliot throughout the series is to conceal data in
audio files that he then burns to CD and hides in his room. To do this, it uses a
free and fairly simple to use software: [DeepSound](http://www.jpinsoft.net/DeepSound/).

Let's try to open the file in DeepSound:

![deepsound test](/img/acebear_2018/myidol/deepsound_test.png)

Yeah! We must now find the password used by the author to hide the data...

Here is a non-exhaustive list of attempts (with case variations):

 - `ER28-0652`: Elliot Alderson's employee id at AllSafe Security
 - `Mr. Robot`: the series' name
 - `Rami Malek`: actor playing Elliot Alderson
 - `Sam Esmail`: series director
 - `Flipper`: name of Lenny Shannon's dog that Elliot stole.
 - `Michael Hansen`: pseudonym used by Krista's ex-boyfriend
 - `Lenny Shannon`: real name of Michael Hansen
 - `Ashley Madison`: a (real) data leak that was used to gather information about Lenny Shannon
 - `Taxi`: used to track down Lenny Shannon
 - `Pierre Loti`: restaurant where Krista and Michael Hansen were having dinner
 - `Krista Gordon`: Elliot's psychotherapist

No variation seems to work... Let's keep thinking!

The file is named `call_me`!

While replaying the episodes in my head (yes, I'm a fan of the series!), I remember very precisely
the first time Elliot Alderson met Michael Hansen in the first episode:

Michael Hansen walks his dog Flipper around and is being aggressive. Elliot attends the scene and asks Michael Hansen
if he can use his phone to call his mother. Operating the social engineering leverage process,
Michael Hansen is forced to lend him his phone. After dialling his own number,
Elliot finally deletes its traces from the call log, allowing us to view its phone number
along with some of Michael Hansen's recent calls.

![mr robot call log](/img/acebear_2018/myidol/mr_robot_call_log.png)

We try again with Elliot's phone number: `(212) 555-0179`, it doesn't work...

After trying with the contacts names from the call log, I finally get back to the Elliot's phone number, but
try different variations:

 - `(212) 555-0179`
 - `(212)555-0179`
 - `212 555-0179`
 - `212-555-0179`
 - `555-0179`
 - `5550179`
 - `2125550179`

Yes!! The password is `2125550179`!

A [`weird`](/files/acebear_2018/myidol/weird) file is hidden in the sound file:

```raw
01359205248365658530218751046900529993719451836050699KKFmwth7962005420727881418598892923382GIfzpMEJDxXlkKzQwxLvUV354
1654892946776010272388584017314623691362754qEeYfOJ348091412907133545573039596290640125315408140410815621
...
```

At first glance, this file looks like a file from the encoding of binary data into base64
using [base64(1)](https://linux.die.net/man/1/base64) tool.

Let's try to decode the data:

```bash
# base64 -d weird >base64_decoded
# file base64_decoded
base64_decoded: data
# strings base64_decoded
yd+40
N+(eb
j\`JWz
TPdN
.U%,qXG
...
```

Nothing interesting when decoding the data, the data, the output binary file doesn't pass any test
(file system analysis, search for magic bytes, language analysis, etc.).

Let's try something else, in the series, the goal is to bring down the E Corp conglomerate!
In order to do this, the FSociety group encrypts all their data, but
keeps the private key of the key pair used to encrypt this data.

In the last episode of season 3, Elliot decides to cancel the operation by sending the encryption key
to E Corp's recovery team in order to recover access to the data.

![mr robot email private key](/img/acebear_2018/myidol/mr_robot_email_private_key.png)

Let's try to get the private key!

![email e corp](/img/acebear_2018/myidol/email_e_corp.png)

Once sent, you receive two automatic replies, one containing a ticket number
and the other one containing the [private key](/files/acebear_2018/myidol/priv.key)!

Let's regenerate the public key and try to decrypt the data:

```bash
# openssl asn1parse -in priv.key  # checking the integrity of the private key
# openssl rsa -in priv.key -pubout >pub.key  # public key generation
# openssl rsautl -decrypt -inkey priv.key -out flag < <(base64 -d weird)  # data decryption with private key
RSA operation error
140259665409280:error:0406506C:rsa routines:rsa_ossl_private_decrypt:data greater than mod len:../crypto/rsa/rsa_ossl.c:391:
# openssl rsautl -decrypt -inkey priv.key -out flag < <(head -n1 weird | base64 -d -)
RSA operation error
140490292888832:error:0407109F:rsa routines:RSA_padding_check_PKCS1_type_2:pkcs decoding error:../crypto/rsa/rsa_pk1.c:243:
140490292888832:error:04065072:rsa routines:rsa_ossl_private_decrypt:padding check failed:../crypto/rsa/rsa_ossl.c:477:
```

That doesn't seems to be the right way, too bad!

After few hours discussing with team members,
we end up remembering that QR Code is widely used in the series. What if
the data was not actually encoded in base64, but instead represents a QRCode?

Alphabetical characters `a-zA-Z` being white pixels and numeric characters `0-9` being black pixels? Let's try it out!

```bash
# git clone https://git.bmoine.fr/alpha-to-pixel
# python alpha-to-pixel/alpha_to_pixel.py -i weird
[info] Input file: weird
[info] Output image: output.png
[info] Image size: (220, 220)
[+] output.png created successfully!
```

<center>![output](/img/acebear_2018/myidol/output.png)</center>

A quick scan with a phone camera and it's done!

<center>![qr code scan](/img/acebear_2018/myidol/qr_code_scan.png)</center>

Final flag:


> AceBear{25_12_Ng0`i_nh4`_r4_d3`_!!!!k0_c00l}


Creased
