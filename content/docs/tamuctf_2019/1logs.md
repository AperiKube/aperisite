+++
title = "MicroServices - 1_Logs"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, PCAP, tshark, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/1logs",
]
+++

### Challenge details

| Event        | Challenge              | Category | Points | Solves |
|--------------|------------------------|----------|--------|--------|
| TamuCTF 2019 | MicroServices - 1_Logs | Forensic | 100    | 228    |

Download: [filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### Description
> Thanks for discovering the malicious IP. We will add it to our block list. We also got a disk image of the web server while you were working. Can you dig a little deeper for us?


> 1.  What user was the attacker able to login as?
> 2.  What is the date & time that the attacker logged in? (MM/DD:HH:MM:SS)

[filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### TL;DR

Mount image and read /var/log/auth.log

### Methology

Once the archive is finally downloaded, we’ll mount it in readonly to avoid screwing everything inside:

```bash
▶ mkdir aaa
▶ sudo mount -o ro filesystem.image aaa
```

We know the attacker’s IP (10.91.9.93) and we are looking for a connection. Let’s see what the `auth.log` file contains:

```bash
➜  microservices cat aaa/var/log/auth.log | grep '10.91.9.93'
Feb 17 00:06:04 ubuntu-xenial sshd[15799]: Accepted publickey for root from 10.91.9.93 port 41592 ssh2: RSA SHA256:lR4653Hv/Y9QthWvXFB2KkNPzQ1r8mItv83OgiCAR4g
```

We got all flags immediately:

```text
Flag 1: root
Flag 2: 02/17:00:06:04
```

#### Flag

`root`<br>
`02/17:00:06:04`

[Maki](https://twitter.com/maki_chaz)
