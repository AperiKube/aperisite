+++
title = "ReadingRainbow - 1_Discovery"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, PCAP, wireshark, tshark, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/1discovery",
]
+++

### Challenge details

| Event        | Challenge                     | Category | Points | Solves |
|--------------|-------------------------------|----------|--------|--------|
| TamuCTF 2019 | ReadingRainbow - 1_Discovery | Forensic | 100    | 263    |

Download: [capture.pcap](https://mega.nz/#!aihUgK6I!a9Lvt6R1bBKit_bE8oOSQAUCdnl9LX_5egidhK5veRM) - md5: e36ff23c6995e3595035982cced6c6a9

### Description
> 1.   What is the IP address of the host exfiltrating data?
> 2.   For how long did the exfiltration happen? (Round to the nearest second. Format: MM:SS)
> 3.   What protocol/s was used to exfiltrate data? (Alphabetical order, all caps, comma separated, with spaces - ex: ABCD, BBCD)
>
> Difficulty: easy
>
> 2/23 1:55 am Add extra info to 3


[capture.pcap](https://mega.nz/#!aihUgK6I!a9Lvt6R1bBKit_bE8oOSQAUCdnl9LX_5egidhK5veRM) - md5: e36ff23c6995e3595035982cced6c6a9

### TL;DR
I used wireshark and identified DET data into ICMP requests.

### Methology

It’s time to open this big PCAP file in Wireshark. Thanks to the first question, we know that the attack exfiltrates some data. The best filter for that is still: data.data:

![1_disco_chall11.png](/img/tamuctf_2019/readingrainbow/1_disco_chall11.png)
`Fig 1: Suspicious ICMP traffic`

We can see that our web server (192.168.11.4) is chatting with another host (192.168.11.7) via weird ICMP requests. Let’s see what these requests contain:
![1_disco_chall12.png](/img/tamuctf_2019/readingrainbow/1_disco_chall12.png)
`Fig 2: Filter on data`

If we get the first request with a `tshark`:

```bash
▶ tshark -r capture.pcap -Y "data.data && ip.dst == 192.168.11.4" -Tfields -e 'data.text' | head -n 1 | xxd -r -p
SEx4IRV.746f74616c6c795f6e6f7468696e672e706466.REGISTER.6156eab6691f32b8350c45b3fc4aadc1
```

The data formatted like this looks a lot like the [DET (Data Exfiltration Toolkit) framework](https://github.com/sensepost/DET). I had already talked about it in a writeup at the [SantHackLause 2018](http://maki.bzh/walkthrough/santhacklaus2018/#mission-impossible-2).

```text
Flag 1: 192.168.11.7
```

Now we have to determine the duration of the exfiltration. With a small filter on IPs, we see some interesting things in the `DNS`:
![1_disco_chall21.png](/img/tamuctf_2019/readingrainbow/1_disco_chall21.png)
`Fig 3: Suspicious domain name`

If we extract the last DNS request and this is indeed the last DET request, we should find a “DONE”:

```bash
▶ echo -n '534578344952562e35312e444f4e45' | xxd -r -p                          
SEx4IRV.51.DONE
```

Perfect, we know the first request with the “REGISTER” and the last one with the “DONE”.

![1_disco_chall23.png](/img/tamuctf_2019/readingrainbow/1_disco_chall23.png)
![1_disco_chall22.png](/img/tamuctf_2019/readingrainbow/1_disco_chall22.png)
`Fig 4: Timestamp`

```bash
▶ echo $((35.49-24.40))                               
11.090000000000003
```

The exfiltration lasted 11 minutes and 9 seconds:

```text
Flag 2: 11.09
```

The last step will be the simplest, we already have all the information, to find the protocols, we will do a little tshark trick:

```bash
▶ tshark -r capture.pcap -Y "ip.src == 192.168.11.4 && ip.dst == 192.168.11.7" -Tfields -e '_ws.col.Protocol' | sort | uniq
DNS
HTTP
ICMP
TCP
```

The “TCP” protocol is not counted since it is a transport protocol (see OSI Model).

```text
Flag 3: DNS, HTTP, ICMP
```

#### Flag

`192.168.11.7`<br/>
`11.09`<br/>
`DNS, HTTP, ICMP`

[Maki](https://twitter.com/maki_chaz)
