+++
title = "ReadingRainbow - 0_Network_Enumeration"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, PCAP, tshark, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/0networkenumeration",
]
+++

### Challenge details

| Event        | Challenge                               | Category | Points | Solves |
|--------------|-----------------------------------------|----------|--------|--------|
| TamuCTF 2019 | ReadingRainbow - 0_Network_Enumeration | Forensic | 100    | 611    |

Download: [capture.pcap](https://mega.nz/#!aihUgK6I!a9Lvt6R1bBKit_bE8oOSQAUCdnl9LX_5egidhK5veRM) - md5: e36ff23c6995e3595035982cced6c6a9

### Description
> Recently, the office put up a private webserver to store important information about the newest research project for the company. This information was to be kept confidential, as it’s release could mean a large loss for everyone in the office. Just as the research was about to be published, a competing firm published information eerily similar. Too similar… Time to take a look through the office network logs to figure out what happened.

[capture.pcap](https://mega.nz/#!aihUgK6I!a9Lvt6R1bBKit_bE8oOSQAUCdnl9LX_5egidhK5veRM) - md5: e36ff23c6995e3595035982cced6c6a9

### TL;DR
I used tshark and wc to get the ip and the count.

### Methology

In this task, the challenge deal with a PCAP file, I let you check my article about [PCAP analysis](http://maki.bzh/articles/wiresharkhowtobasic/).

The first flag is to find the internal IP address of a web server. Since PCAP is quite large, I just have to load it into `Capanalysis` and filter on the *SSL* and *HTTP* protocols, then filter on the IP that receives the most data:

![0_netenum_chall11.png](/img/tamuctf_2019/readingrainbow/0_netenum_chall11.png)
`Fig 1: Web server IP address`

It was the web server we were looking for:

```text
Flag 1: 192.168.11.4
```

Now the second challenge is to find the number of IP addresses that have connected to this webserver. Since we know his IP address, with `tshark` it’s pretty easy:

```bash
▶ tshark -r capture.pcap -Y "ip.dst == 192.168.11.4" -Tfields -e 'ip.src' | sort | uniq
128.194.165.200
172.217.6.138
172.226.209.130
192.168.1.1
192.168.11.5
192.168.11.7
192.168.11.8
192.168.11.9
35.222.85.5
35.224.99.156
52.43.40.243
54.213.168.194
91.189.92.38

▶ tshark -r capture.pcap -Y "ip.dst == 192.168.11.4" -Tfields -e 'ip.src' | sort | uniq | wc -l
13
```

Fortunately, all connections are done on the same day:

```text
Flag 2: 13
```


#### Flag

`192.168.11.4`<br/>
`13`

[Maki](https://twitter.com/maki_chaz)
