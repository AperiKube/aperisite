+++
title = "MicroServices - 4_Persistence"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, Docker, Image, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/4persistence",
]
+++

### Challenge details

| Event        | Challenge                     | Category | Points | Solves |
|--------------|-------------------------------|----------|--------|--------|
| TamuCTF 2019 | MicroServices - 4_Persistence | Forensic | 100    | 42     |

Download: [filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### Description
> Thanks for that information. We are working on how to recover from this breach. One of the things we need to do is remove any backdoors placed by the attacker. Can you identify what the attacker left behind?

> 1.   What is the new user that was created?
> 2.   What is the full name of the new docker image that was pulled down?

[filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### Methology

This challenge was rather simple given the information found earlier. In step 2 (2_Analysis), I saw that the $HOME of the `dev` user is `/root`, so we suspect that it is the new user :

```text
Flag 1: dev
```

Then, when I started investigating on docker containers (cf. 3_Forensics), I saw an image **Kali** named: `tamuctf/kaliimage`.

```text
Flag 2: tamuctf/kaliimage
```


#### Flag

`dev`<br/>
`tamuctf/kaliimage`

[Maki](https://twitter.com/maki_chaz)
