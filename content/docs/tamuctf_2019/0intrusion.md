+++
title = "MicroServices - 0_Intrusion"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, PCAP, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/0intrusion",
]
+++

### Challenge details

| Event        | Challenge                   | Category | Points | Solves |
|--------------|-----------------------------|----------|--------|--------|
| TamuCTF 2019 | MicroServices - 0_Intrusion | Forensic | 100    | 803    |

Download: [microservice.pcap](https://mega.nz/#!Gv5zAahB!afQTRfSLEE93xDDoZbi0EoGLrGzshAALLCS-1LwykdY) - md5: 18d2c48f5d03d5faa5cb4473f9819b4b

### Description
> Welcome to MicroServices inc, where do all things micro and service oriented!
Recently we got an alert saying there was suspicious traffic on one of our web servers. Can you help us out?

> 1.  What is the IP Address of the attacker?



[microservice.pcap](https://mega.nz/#!Gv5zAahB!afQTRfSLEE93xDDoZbi0EoGLrGzshAALLCS-1LwykdY) - md5: 18d2c48f5d03d5faa5cb4473f9819b4b

### TL;DR

I used wireshark to get the @IP with the most occurences.

### Methology

For this flag I don’t have any real analysis, I just opened the PCAP file and looked at the different TCP conversations. The IP that sends the most data and voila:

![0_intru_chall11.png](/img/tamuctf_2019/microservices/0_intru_chall11.png)
`Fig 1: Malicious IP `

```text
Flag: 10.91.9.93
```

#### Flag

`10.91.9.93`

[Maki](https://twitter.com/maki_chaz)
