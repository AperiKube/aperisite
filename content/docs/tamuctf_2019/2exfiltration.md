+++
title = "ReadingRainbow - 2_Exfiltration"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, PCAP, wireshark, tshark, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/2exfiltration",
]
+++

### Challenge details

| Event        | Challenge                        | Category | Points | Solves |
|--------------|----------------------------------|----------|--------|--------|
| TamuCTF 2019 | ReadingRainbow - 2_Exfiltration | Forensic | 100    | 136    |

Download: [capture.pcap](https://mega.nz/#!aihUgK6I!a9Lvt6R1bBKit_bE8oOSQAUCdnl9LX_5egidhK5veRM) - md5: e36ff23c6995e3595035982cced6c6a9

### Description
> Thanks for that information. Can you take a deeper dive now and figure out exactly how the attacker go in?
> 1.   What is the name of the service that was used to compromise the machine? (All lowercase)
> 2.   What is the md5sum of the initial compromising file?
> 3.   What specific line in the initial compromising file was the most dangerous? (Actual line, spaces in front don't matter)

[capture.pcap](https://mega.nz/#!aihUgK6I!a9Lvt6R1bBKit_bE8oOSQAUCdnl9LX_5egidhK5veRM) - md5: e36ff23c6995e3595035982cced6c6a9

### TL;DR
I reused the extracted DET informations and decoded it.

### Methology

These questions are a little bit easy since we know that the attacker used DET. First request contains both flags:

- SEx4IRV
- 746f74616c6c795f6e6f7468696e672e706466
- REGISTER
- 6156eab6691f32b8350c45b3fc4aadc1

The first hexadecimal sequence is the name of the encoded extracted file:

```bash
▶ echo -n '746f74616c6c795f6e6f7468696e672e706466' | xxd -r -p                              
totally_nothing.pdf
```

```text
Flag 1: totally_nothing.pdf
```

The second hexadecimal sequence is the MD5 hash of the encoded extracted file:

```text
Flag 2: 6156eab6691f32b8350c45b3fc4aadc1
```

#### Flag

`totally_nothing.pdf`<br/>
`6156eab6691f32b8350c45b3fc4aadc1`

[Maki](https://twitter.com/maki_chaz)
