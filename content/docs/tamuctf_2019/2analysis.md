+++
title = "MicroServices - 2_Analysis"
description = "TamuCTF 2019 - Forensic (100 pts)"
keywords = "Forensic, PCAP, tshark, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "TamuCTF 2019 - Forensic (100 pts)"
toc = true
aliases = [
    "/docs/2analysis",
]
+++

### Challenge details

| Event        | Challenge                  | Category | Points | Solves |
|--------------|----------------------------|----------|--------|--------|
| TamuCTF 2019 | MicroServices - 2_Analysis | Forensic | 100    | 136    |

Download: [filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### Description
> Thanks for that information. Can you take a deeper dive now and figure out exactly how the attacker go in?

> 1.   What is the name of the service that was used to compromise the machine? (All lowercase)
> 2.   What is the md5sum of the initial compromising file?
> 3.   What specific line in the initial compromising file was the most dangerous? (Actual line, spaces in front don't matter)

[filesystem.image](https://drive.google.com/uc?id=19zgsmqMZ_QltLYzWcCdxizV9Wipj-2NI&export=download) - md5: 490c78e249177e6478539f459ca14e87

### TL;DR

I found a docker-composes file.

### Methology

During an investigation, my first reflex is to go to the folders of the different users (`/home`, `/root`):

```bash
➜  microservices tree -a -f aaa/root
aaa/root
├── aaa/root/.bashrc
├── aaa/root/.cache
│   └── aaa/root/.cache/motd.legal-displayed
├── aaa/root/.profile
└── aaa/root/.ssh
    ├── aaa/root/.ssh/authorized_keys
    └── aaa/root/.ssh/id_rsa

➜  microservices tree -a -f aaa/home
aaa/home
└── aaa/home/ubuntu
    ├── aaa/home/ubuntu/.ansible
    │   └── aaa/home/ubuntu/.ansible/tmp
    │       └── aaa/home/ubuntu/.ansible/tmp/ansible-tmp-1550362148.9-21461470003029
    │           └── aaa/home/ubuntu/.ansible/tmp/ansible-tmp-1550362148.9-21461470003029/command
    ├── aaa/home/ubuntu/.bash_logout
    ├── aaa/home/ubuntu/.bashrc
    ├── aaa/home/ubuntu/.cache
    │   └── aaa/home/ubuntu/.cache/motd.legal-displayed
    ├── aaa/home/ubuntu/.data
    │   ├── aaa/home/ubuntu/.data/mysql
    │   │   ├── aaa/home/ubuntu/.data/mysql/aria_log.00000001
    │   │   ├── aaa/home/ubuntu/.data/mysql/aria_log_control
    │   │   ├── aaa/home/ubuntu/.data/mysql/customers
    │   │   │   ├── aaa/home/ubuntu/.data/mysql/customers/customer_info.frm
    │   │   │   ├── aaa/home/ubuntu/.data/mysql/customers/customer_info.ibd
    │   │   │   └── aaa/home/ubuntu/.data/mysql/customers/db.opt
    │   │   ├── aaa/home/ubuntu/.data/mysql/ib_buffer_pool
    │   │   ├── aaa/home/ubuntu/.data/mysql/ibdata1
[LOOOOOOTS OF MYSQL FILES]
    │   │   ├── aaa/home/ubuntu/.data/mysql/performance_schema
    │   │   │   └── aaa/home/ubuntu/.data/mysql/performance_schema/db.opt
    │   │   └── aaa/home/ubuntu/.data/mysql/tc.log
    │   └── aaa/home/ubuntu/.data/redis
    ├── aaa/home/ubuntu/docker-compose.yml
    ├── aaa/home/ubuntu/id_rsa.pub
    ├── aaa/home/ubuntu/logs
    │   ├── aaa/home/ubuntu/logs/access.log
    │   ├── aaa/home/ubuntu/logs/error.log
    │   └── aaa/home/ubuntu/logs/other_vhosts_access.log
    ├── aaa/home/ubuntu/.profile
    ├── aaa/home/ubuntu/.ssh
    │   └── aaa/home/ubuntu/.ssh/authorized_keys
    └── aaa/home/ubuntu/.sudo_as_admin_successful

13 directories, 112 files
```

We will search in the `/etc/passwd` file if there are no other users who have can access to a **bash**:

```bash
➜  microservices cat aaa/etc/passwd | grep bash
root:x:0:0:root:/root:/bin/bash
ubuntu:x:1000:1000:Ubuntu:/home/ubuntu:/bin/bash
dev:x:0:0:root:/root:/bin/bash
```

It’s funny, the user `dev` has `/root` as $HOME directory…
We see a `docker-composes.yml` file in the user’s directory **ubuntu**:

```bash
➜  microservices cat aaa/home/ubuntu/docker-compose.yml
version: '2'

services:
  web:
    image: tamuctf/webfront:latest
    restart: always
    ports:
      - "80:80"
    environment:
      - DATABASE_URL=mysql+pymysql://root:351BrE7aTQE8@db/customers
      - REDIS_URL=redis://cache:6379
    volumes:
      - ./logs:/var/log/apache2
      - /:/tmp
    depends_on:
      - db
    networks:
        default:
        internal:

  db:
    image: mariadb:10.2
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=351BrE7aTQE8
      - MYSQL_USER=user
      - MYSQL_PASSWORD=e68Qc2s0HsyR
    volumes:
      - .data/mysql:/var/lib/mysql
    networks:
        internal:
    # This command is required to set important mariadb defaults
    command: [mysqld, --character-set-server=utf8mb4, --collation-server=utf8mb4_unicode_ci, --wait_timeout=28800, --log-warnings=0]

  cache:
    image: redis:4
    restart: always
    volumes:
    - .data/redis:/data
    networks:
        internal:

networks:
    default:
    internal:
        internal: true
```

If you’re a little familiar with Docker, you’ve already found out where the attacker went. Line 13 of the file, the user is mounting the root directory in the /tmp folder of the container:

```text
- /:/tmp
```

As a result, the attacker compromised the webfront and got access to the host filesystem as root user. So he was able to retrieve the SSH private key from the host and logged in with.

The MD5 hash of the docker-composer.yml:

```bash
➜  microservices md5sum aaa/home/ubuntu/docker-compose.yml
a2111283f69aafcd658f558b0402fbc4  aaa/home/ubuntu/docker-compose.yml
```

```text
Flag 1: docker
Flag 2: a2111283f69aafcd658f558b0402fbc4
Flag 3: - /:/tmp
```

#### Flag

`docker`<br>
`a2111283f69aafcd658f558b0402fbc4`<br>
`- /:/tmp`

[Maki](https://twitter.com/maki_chaz)
