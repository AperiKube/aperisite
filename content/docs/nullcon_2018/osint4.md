+++
title = "OSINT4"
description = "Nullcon Hackim 2018 - OSINT (400 pts)"
keywords = "OSINT, Web, Pentest, Github, Slack, API, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-02-11T09:22:32+01:00
weight = 20
draft = false
bref = "Nullcon Hackim 2018 - OSINT (400 pts)"
toc = true
aliases = [
    "/docs/nullcon_2018_osint4",
]
+++

Nullcon Hackim 2018: OSINT4
===========================

| Event                    | Challenge | Category      | Points | Solves      |
|--------------------------|-----------|---------------|--------|-------------|
| Nullcon Hackim 2018      | OSINT 4   | OSINT         | 400    | ¯\\_(ツ)_/¯ |

### Description

This server is an staging/uat box but the developer has got a public IP on the same. Someone exploited the misconfiguration and got hold of the box.

Can you re-hack the server and get hold of attacker's secret flag.

Target: 54.85.105.103

### TL;DR

In this task I have to "hack" again a server. So I nmapped the IP, I got 22 and 80 ports open, there is a `.git` directory.

Then we take a look on previous commit and I can see a slack token, with the Slack API I get back `private` / `public` ssh keys and user information (username).

Then, with those informations I just log myself with username and keys. A little look about history and flag.

### Nmap

Then I have an IP address, first move: `nmap`.

```bash
# nmap -sSVC -T4 -O -p- -vvvv -oX full_port.xml 54.85.105.103
# xsltproc full_port.xml > full_port.html
```

<center>![Port scan](/img/nullcon_2018/osint4/port_scan.png)<br />
__Fig 1 - Port scan__</center>

We can see a `.git` directory, interesting!

### GitHub

Then I found this git:

> https://github.com/johnatcorp/corpnull

Let's dig in previous commit...

<center>![Git commit](/img/nullcon_2018/osint4/commit_git.png)<br />
__Fig 2 - Git commit__</center>

w00t! A Slack token and a Slack server:

> xoxp-302709175073-304111796230-302748987361-6d10dcd6f4cedc60900986def747f0e5

>   https://corpnull.slack.com

### Slack

I'm not familiar at all with Slack and even less with the API... Fortunately, the Slack API is really well done. You can request the API trough your web browser.

I created an email address in `@mail.com` (erbfzeiurgbmoezrogbum@mail.com) to create an account on this slack and generate a token.

#### auth.test

Then I'm checking the validity of the given token:

> https://slack.com/api/auth.test?token=xoxp-302709175073-304111796230-302748987361-6d10dcd6f4cedc60900986def747f0e5&pretty=1

```json
{
    "ok": true,
    "url": "https:\/\/corpnull.slack.com\/",
    "team": "corpnull",
    "user": "mikeatcorp",
    "team_id": "T8WLV5525",
    "user_id": "U8Y39PE6S"
}
```

Well, it looks to work, and I got the username `mikeatcorp`.

#### files.list

With this query:

> https://slack.com/api/files.list?token=xoxp-302709175073-304111796230-302748987361-6d10dcd6f4cedc60900986def747f0e5&pretty=1

I got:

```json
{
    "ok": true,
    "files": [
        {
            "id": "F8XC1KB37",
            "created": 1516723344,
            "timestamp": 1516723344,
            "name": "id_rsa.pub",
            "title": "id_rsa.pub",
            "mimetype": "text\/plain",
            "filetype": "text",
            "pretty_type": "Plain Text",
            "user": "U8Y39PE6S",
            "editable": true,
            "size": 745,
            "mode": "snippet",
            "is_external": false,
            "external_type": "",
            "is_public": true,
            "public_url_shared": false,
            "display_as_bot": false,
            "username": "",
            "url_private": "https:\/\/files.slack.com\/files-pri\/T8WLV5525-F8XC1KB37\/id_rsa.pub",
            "url_private_download": "https:\/\/files.slack.com\/files-pri\/T8WLV5525-F8XC1KB37\/download\/id_rsa.pub",
            "permalink": "https:\/\/corpnull.slack.com\/files\/U8Y39PE6S\/F8XC1KB37\/id_rsa.pub",
            "permalink_public": "https:\/\/slack-files.com\/T8WLV5525-F8XC1KB37-32b7572f04",
            "edit_link": "https:\/\/corpnull.slack.com\/files\/U8Y39PE6S\/F8XC1KB37\/id_rsa.pub\/edit",
            "preview": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7RUm542gWJ56hYOpp4iAMSmF\/WAQAO\/dKKg4+QC7EfA1NxRtDsfsB0ehQ1z+V+3CK7AJPeMnfsALPTSLfu0O8vXAuT+ja+UnjwFMuTLySAb1EN9G1S5jls1xcGYdAo4RcDNUFjDAVzq7PoZ7h58IVmpJXUlyey2BVN2kwclohJRKxxnm3D7o++XRz9hwdBZq6088cEmOwxUsztRSg3a0STvQzo3YLv2Uc3OuhEUi8Z7Pslo4qA1I04gGfhcS5wv8Yp3TQvpiyxcHhfYmjHvommedaGltGNZTNi5YnTvFwX6d3yJYKrb8Cflx+M9Mk7CJvmX1bTpmslZEsH+Jhosj03BbqWSAI2SZUeYk2Wz+I\/9WtCkaVHSuSRR4TyyEVBzLhaFAZbP\/2lN3R3jyLfW0i8g20\/6EExkjeNgYNolRTup\/+RJ2dbleWIK+zBxPj0EJ8bY0RRBvzRsavpJBfcKSC6yqEBRXBhljf44M5OTos6KMXTlA3Pff46TDy2AWULn9PC7rMit9Nm6xx4ArX\/b2ZDsCnOoeMWG0dehjePXsqu6rYqzi6utvpWDUbfISemfyoISVL1tLwNmhnHZcOgPmIOcx9xwbIvpaDjJAIopQcp4E3wmJ8TUvkaJzVWXVRw1cbp2kn7Si7dhgpWSrCAl9XmTUWLs+i+CagPxIfouTpVw== mikeatcorp@mail.com\n",
            "preview_highlight": "<div class=\"CodeMirror cm-s-default CodeMirrorServer\" oncopy=\"if(event.clipboardData){event.clipboardData.setData('text\/plain',window.getSelection().toString().replace(\/\\u200b\/g,''));event.preventDefault();event.stopPropagation();}\">\n<div class=\"CodeMirror-code\">\n<div><pre>ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7RUm542gWJ56hYOpp4iAMSmF\/WAQAO\/dKKg4+QC7EfA1NxRtDsfsB0ehQ1z+V+3CK7AJPeMnfsALPTSLfu0O8vXAuT+ja+UnjwFMuTLySAb1EN9G1S5jls1xcGYdAo4RcDNUFjDAVzq7PoZ7h58IVmpJXUlyey2BVN2kwclohJRKxxnm3D7o++XRz9hwdBZq6088cEmOwxUsztRSg3a0STvQzo3YLv2Uc3OuhEUi8Z7Pslo4qA1I04gGfhcS5wv8Yp3TQvpiyxcHhfYmjHvommedaGltGNZTNi5YnTvFwX6d3yJYKrb8Cflx+M9Mk7CJvmX1bTpmslZEsH+Jhosj03BbqWSAI2SZUeYk2Wz+I\/9WtCkaVHSuSRR4TyyEVBzLhaFAZbP\/2lN3R3jyLfW0i8g20\/6EExkjeNgYNolRTup\/+RJ2dbleWIK+zBxPj0EJ8bY0RRBvzRsavpJBfcKSC6yqEBRXBhljf44M5OTos6KMXTlA3Pff46TDy2AWULn9PC7rMit9Nm6xx4ArX\/b2ZDsCnOoeMWG0dehjePXsqu6rYqzi6utvpWDUbfISemfyoISVL1tLwNmhnHZcOgPmIOcx9xwbIvpaDjJAIopQcp4E3wmJ8TUvkaJzVWXVRw1cbp2kn7Si7dhgpWSrCAl9XmTUWLs+i+CagPxIfouTpVw== mikeatcorp@mail.com<\/pre><\/div>\n<div><pre><\/pre><\/div>\n<\/div>\n<\/div>\n",
            "lines": 2,
            "lines_more": 0,
            "preview_is_truncated": false,
            "channels": [
                "C8Y41NDEJ"
            ],
            "groups": [],
            "ims": [],
            "comments_count": 0
        },
        {
            "id": "F8YA9C46T",
            "created": 1516723342,
            "timestamp": 1516723342,
            "name": "id_rsa",
            "title": "id_rsa",
            "mimetype": "text\/plain",
            "filetype": "text",
            "pretty_type": "Plain Text",
            "user": "U8Y39PE6S",
            "editable": true,
            "size": 3243,
            "mode": "snippet",
            "is_external": false,
            "external_type": "",
            "is_public": true,
            "public_url_shared": true,
            "display_as_bot": false,
            "username": "",
            "url_private": "https:\/\/files.slack.com\/files-pri\/T8WLV5525-F8YA9C46T\/id_rsa",
            "url_private_download": "https:\/\/files.slack.com\/files-pri\/T8WLV5525-F8YA9C46T\/download\/id_rsa",
            "permalink": "https:\/\/corpnull.slack.com\/files\/U8Y39PE6S\/F8YA9C46T\/id_rsa",
            "permalink_public": "https:\/\/slack-files.com\/T8WLV5525-F8YA9C46T-3e2f445d5e",
            "edit_link": "https:\/\/corpnull.slack.com\/files\/U8Y39PE6S\/F8YA9C46T\/id_rsa\/edit",
            "preview": "-----BEGIN RSA PRIVATE KEY-----\nMIIJKAIBAAKCAgEAu0VJueNoFieeoWDqaeIgDEphf1gEADv3SioOPkAuxHwNTcUb\nQ7H7AdHoUNc\/lftwiuwCT3jJ37ACz00i37tDvL1wLk\/o2vlJ48BTLky8kgG9RDfR\ntUuY5bNcXBmHQKOEXAzVBYwwFc6uz6Ge4efCFZqSV1JcnstgVTdpMHJaISUSscZ5\ntw+6Pvl0c\/YcHQWautPPHBJjsMVLM7UUoN2tEk70M6N2C79lHNzroRFIvGez7JaO",
            "preview_highlight": "<div class=\"CodeMirror cm-s-default CodeMirrorServer\" oncopy=\"if(event.clipboardData){event.clipboardData.setData('text\/plain',window.getSelection().toString().replace(\/\\u200b\/g,''));event.preventDefault();event.stopPropagation();}\">\n<div class=\"CodeMirror-code\">\n<div><pre>-----BEGIN RSA PRIVATE KEY-----<\/pre><\/div>\n<div><pre>MIIJKAIBAAKCAgEAu0VJueNoFieeoWDqaeIgDEphf1gEADv3SioOPkAuxHwNTcUb<\/pre><\/div>\n<div><pre>Q7H7AdHoUNc\/lftwiuwCT3jJ37ACz00i37tDvL1wLk\/o2vlJ48BTLky8kgG9RDfR<\/pre><\/div>\n<div><pre>tUuY5bNcXBmHQKOEXAzVBYwwFc6uz6Ge4efCFZqSV1JcnstgVTdpMHJaISUSscZ5<\/pre><\/div>\n<div><pre>tw+6Pvl0c\/YcHQWautPPHBJjsMVLM7UUoN2tEk70M6N2C79lHNzroRFIvGez7JaO<\/pre><\/div>\n<\/div>\n<\/div>\n",
            "lines": 52,
            "lines_more": 47,
            "preview_is_truncated": true,
            "channels": [
                "C8Y41NDEJ"
            ],
            "groups": [],
            "ims": [],
            "comments_count": 0
        }
    ],
    "paging": {
        "count": 100,
        "total": 2,
        "page": 1,
        "pages": 1
    }
}
```

Then I was able to download the `public` and `private` keys with the following links:

* __Public__: https://files.slack.com/files-pri/T8WLV5525-F8XC1KB37/download/id_rsa.pub
* __Private__: https://files.slack.com/files-pri/T8WLV5525-F8YA9C46T/download/id_rsa

Time to pwn the server!

### SSH

Now let's logging to the server:

```bash
$ ssh mikeatcorp@54.85.105.103 -i /home/maki/Documents/Challenges/nullcon2k18/osint4/ssh_key/id_rsa
Welcome to Ubuntu 16.04.3 LTS (GNU/Linux 4.4.0-1047-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  Get cloud support with Ubuntu Advantage Cloud Guest:
    http://www.ubuntu.com/business/services/cloud

24 packages can be updated.
0 updates are security updates.


*** System restart required ***
Last login: Sun Feb 11 15:57:45 2018 from 47.247.203.150
mikeatcorp@ip-172-30-1-179:~$ cat .bash_history
...
locate flag
cat /secret/.supersecret/flag.txt
exit
mikeatcorp@ip-172-30-1-179:~$ cat /secret/.supersecret/flag.txt
hackim18{'455676878965435365788698546'}
```

w00t! \o/

### Flag

> hackim18{'455676878965435365788698546'}

Maki
