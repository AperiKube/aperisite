+++
title = "OSINT3"
description = "Nullcon Hackim 2018 - OSINT (300 pts)"
keywords = "OSINT, Leak, Pastebin, Snapchat, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-02-11T09:22:32+01:00
weight = 20
draft = false
bref = "Nullcon Hackim 2018 - OSINT (300 pts)"
toc = true
aliases = [
    "/docs/nullcon_2018_osint3",
]
+++

Nullcon Hackim 2018: OSINT3
===========================

| Event                    | Challenge | Category      | Points | Solves      |
|--------------------------|-----------|---------------|--------|-------------|
| Nullcon Hackim 2018      | OSINT 3   | OSINT         | 300    | ¯\\_(ツ)_/¯ |

### Description

Person is running a social engineering campaign. After initial inspection, his/her username was identified by our investigators. It was also found that this guy was signed up on Snapchat and Instachatbook around April 2017. However we cannot get hold of his phone number.

Username identified was 'example1234'
Please help our investigators find his number.

### TL;DR

With the email address, social media and date, I looked for database leak of those media.

First I found a pastebin note showing the last 2 digits of the phone number.

Secondly, I found a website that return you 8 first digits of your phone number depending on your username.

Then we got the complete phone number.

### Pastebin

In my research for database leak I find this [pastebin](https://pastebin.com/vA3BfJER).

> ('XXXXXXXX55', 'example1234', 'example1234@instachatbookk.com', ''),

I got last two digits \o/

### FindMySnap

5 minutes later I found the [FindMySnap](#) website (actually down, link removed due to domain jacking).

<center>![findmysnap](/img/nullcon_2018/osint3/findmysnap.png)<br />
__Fig 1 - example1234 phone number__</center>

And I got 8 first \o/

### Flag

> hackim18{'8157931155'}

Maki
