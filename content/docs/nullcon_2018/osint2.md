+++
title = "OSINT2"
description = "Nullcon Hackim 2018 - OSINT (200 pts)"
keywords = "OSINT, Flickr, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-02-11T09:22:32+01:00
weight = 20
draft = false
bref = "Nullcon Hackim 2018 - OSINT (200 pts)"
toc = true
aliases = [
    "/docs/nullcon_2018_osint2",
]
+++

Nullcon Hackim 2018: OSINT1
===========================

| Event                    | Challenge | Category      | Points | Solves      |
|--------------------------|-----------|---------------|--------|-------------|
| Nullcon Hackim 2018      | OSINT 2   | OSINT         | 200    | ¯\\_(ツ)_/¯ |

### Description

Annual audits have flagged an employee who is sharing data outside the company in some secret manner. A quick OSINT revealed his personal email id, i.e. zakripper@mail.com.

Can you find the secret?

### TL;DR

After few research I found a [Flickr account](https://www.flickr.com/photos/162289309@N03) with [two childs telling a secret](https://farm5.staticflickr.com/4707/28359721879_4ed9886805_o_d.jpg).

I downloaded the picture and just did a `strings` on it.

### Lullar

First, I looked for [doxing people via email address](https://www.facebook.com/permalink.php?id=376357029151762&story_fbid=376361642484634). Then I found the [Lullar](https://lullar-com-3.appspot.com/en) website.

<center>![](/img/nullcon_2018/osint2/lullar.png)<br />
__Fif 1 - Lullar website__</center>

I entered the email address and look for ALL social media, a lot of 404 not found.

### Flickr

But after a while, I found a [Flickr account](https://www.flickr.com/photos/162289309@N03) with this picture:

<center>![](/img/nullcon_2018/osint2/flickr.png)<br />
__Fif 2 - Flickr__</center>

### Picture analysis

After downloaded it I just made a `strings` on it:

```bash
$ strings 28359721879_4ed9886805_o.jpg
...
*@#|
{G@ Na
;o+%m[
l&e_zJ|..
syPpj
hackim18{'7h1515453cr3tm35543'}
syPpj
```

### Flag

> hackim18{'7h1515453cr3tm35543'}

Maki
