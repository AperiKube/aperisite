+++
title = "MISC2"
description = "Nullcon Hackim 2018 - MISC (100 pts)"
keywords = "MISC, Forensic, ICMP, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-02-11T09:22:32+01:00
weight = 20
draft = false
bref = "Nullcon Hackim 2018 - MISC (100 pts)"
toc = true
aliases = [
    "/docs/nullcon_2018_misc2",
]
+++

Nullcon Hackim 2018: MISC2
==========================

| Event                    | Challenge | Category      | Points | Solves      |
|--------------------------|-----------|---------------|--------|-------------|
| Nullcon Hackim 2018      | MISC 2    | MISC          | 100    | ¯\\_(ツ)_/¯ |

### Description

Find the transferred file

[challenge.pcapng](/files/nullcon_2018/misc2/misc2.pcapng)

### TL;DR

A classic data extract via ICMP. The data extracted was a plain text file (flag.txt) inside a `tar` archive, inside a `gzip` file.

### State of the art

So I'm starting to list all TCP conversation on Wireshark:

> Statistics > Conversation List > TCP

I see TLS communication with Google's certificate and IP address, then useless. There are HTTP requests on the `192.168.42.85:8000`, useless pictures.

If I remove all TCP communication, it remains ICMP. Let's take a look with this filter:

> ip.addr == 192.168.42.85 && icmp.resp_in

Well, when I can see last 2 bytes of each ICMP packet changing. Only with the hex value, time to extract it.

### CLI my best friend

Well with wireshark we can see 3 useless packets at the top and 4 at the bottom. Moreover, data we're looking for are hex data, tshark will hexlify them, so I need to decode data two times.

The little one liner:

```bash
$ tshark -r challenge.pcapng -Y "ip.addr == 192.168.42.85 && icmp.type == 0" -T fields -e data | head -n-4  | tail -n+4 | xxd -r -p | xxd -r -p > /tmp/output.raw
$ file /tmp/output.raw
/tmp/output.raw: gzip compressed data, last modified: Thu Feb  8 13:19:40 2018, from Unix
```

Great, I'm on the right path.

### Archivecption

Now, time to get the flag.

```bash
$ mv /tmp/output.raw /tmp/output.gz
$ gzip -d /tmp/output.gz
$ file /tmp/output
/tmp/output: POSIX tar archive
$ tar -tf /tmp/output
flag.txt
$ tar xf /tmp/output
$ cat flag.txt
hackim18{'51mpL3st_Ch4ll3ng3_s0lv3d'}
```

### Flag

> hackim18{'51mpL3st_Ch4ll3ng3_s0lv3d'}

Maki
