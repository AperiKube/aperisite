+++
title = "OSINT1"
description = "Nullcon Hackim 2018 - OSINT (100 pts)"
keywords = "OSINT, Virustotal, Javascript, Dropper, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-02-11T09:22:32+01:00
weight = 20
draft = false
bref = "Nullcon Hackim 2018 - OSINT (100 pts)"
toc = true
aliases = [
    "/docs/nullcon_2018_osint1",
]
+++


Nullcon Hackim 2018: OSINT1
===========================

| Event                    | Challenge | Category      | Points | Solves      |
|--------------------------|-----------|---------------|--------|-------------|
| Nullcon Hackim 2018      | OSINT 1   | OSINT         | 100    | ¯\\_(ツ)_/¯ |

### Description

This is an OSINT challenge, it's rare and the Nullcon made it very well! The statement:

One of our systems has been infected by a ransomware.The message says My username is your password. Wait for further instructions.

We have been able to identify the JS file used to download the ransomware.

Here is the MD5: '__151af957b92d1a210537be7b1061dca6__'.

Can you help us to unlock the machine?

### TL;DR

In this task the author gaves us a ransomware hash. After passing it in [virustotal](https://www.virustotal.com/fr/file/879cba341ab69f16ad2cccefc985fd9e6a1bb37c084f2a40e861b52342d143a0/analysis/).
I saw the __DSAdaDSDA.js__ JS dropper for __Nemucod__ ransomware.

After a little looking on Google, I found the [Hybrid Analysis report](https://www.hybrid-analysis.com/sample/611f55dc3d7b88d8000aa54bb571752f9b14889d913805ae5824187c1cc73371?environmentId=100) and found the username __n923wUc__ in the _HTTP Traffic_.

### VirusTotal

In forensic world, when we have suspicious malware or script or whatever, the first move is: make the hash and go on [virustotal](https://www.virustotal.com).
If the hash is known, then there is an analysis and probably userful comments from other analysts.

Here, it's exactly what I did. I put the given hash to virustotal and then:

<center>![fig1](/img/nullcon_2018/osint1/vt_fig1.png)<br />
__Fig 1 - Malicious you said ?__</center>

We can see th JS dropper and Nemucod malware.

BTW, there is decrypter on [NoMoreRansom](https://www.nomoreransom.org/en/decryption-tools.html#NemucodAES).

### HybridAnalysis

After that I looked for a more verbose analysis, then I find:

* https://www.reverse.it/sample/b6af388005ba3dbb9e0f79e704ae70470bc1cce20affbd9dbdf42bd8548ccf94?environmentId=100
* https://www.hybrid-analysis.com/sample/50d6111ced473456a1c9e111c18bdb60f2f9f607800fd795c627751e79aacc9b?environmentId=100
* https://www.hybrid-analysis.com/sample/611f55dc3d7b88d8000aa54bb571752f9b14889d913805ae5824187c1cc73371?environmentId=100

One of those link got the flag. But at this point I don't knew it!

After a lot of search into strings, extracted files... I ended up to forget what I looked for...

### Username ??

I'm looking for a username! In a great despair, I did a CTRL+F "USERNAME" in the first link and found:

> USERNAME=skcV5tg

w00t \o/ ...Or not, it doesn't flag... Admin told me that is not the right username, dammit.

I did the same thing on the second link and... Nothing...

And finally on the third link:

> USERNAME=n923wUc

### Flag

> hackim18{'n923wUc'}

Maki
