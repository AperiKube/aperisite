+++
title = "Werizchacha"
description = "BreizhCTF 2019 - Reverse (25 pts)"
keywords = "Go, Reverse, Strcmp, , WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-04-14T13:00:00+02:00"
weight = 20
draft = false
bref = "BreizhCTF 2019 - Reverse (25 pts)"
toc = true
+++

BreizhCTF 2019: Werizchacha
============================================

### Challenge details

| Event                    | Challenge                 | Category      | Points | Solves      |
|--------------------------|---------------------------|---------------|--------|-------------|
| BreizhCTF 2019           | Werizchacha               | Reverse          | 25    | ?           |

Challenge: [werizchacha](/files/breizhctf_2019/werizchacha) md5sum : fdee16febd35909b93ccd22147d60df4

### TL;DR

Easy reverse, break at strcmp :)

### Methodology

#### Recon with Ltrace

First launch the binary with ltrace :

```
# ltrace ./werizchacha aa
strlen("AQDHYGBSEzj2x^G0c0mf^hm^O030m^Rh"...)                                                                                       = 36
strcmp("BREIZHCTF{k3y_H1d1ng_in_P141n_Si"..., "aaa")                                                                                = -31
puts("That's not the flag."That's not the flag.
)                                                                                                        = 21
+++ exited (status 1) ++++
```

There is some string that look alike a flag but it's truncated.

#### GDB time


```
gdb ./werizchacha
```

We search strcmp in the `main` function : `disass main`


![](https://i.imgur.com/I0s2CP1.png)


break at `main+220` where **strcmp** is called : `b *main+220`
then run : `r aaaa`


![](https://i.imgur.com/QEGucBa.png)

Got an easy flag :smile: 

#### Flag

```
BREIZHCTF{k3y_H1d1ng_in_P141n_SiGhT}
```

@Areizen
