+++
title = "Dig Deep"
description = "Xiomara CTF 2018 - Forensic (150 pts)"
keywords = "Forensic, Carving, GIT, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-02-26T23:20:34+01:00
weight = 20
draft = false
bref = "Xiomara CTF 2018 - Forensic (150 pts)"
toc = true
aliases = [
    "/docs/xiomara_2018_dig_deep",
]
+++

Xiomara CTF 2018: Dig Deep
==========================

### Challenge details

| Event            | Challenge | Category | Points | Solves      |
|------------------|-----------|----------|--------|-------------|
| Xiomara CTF 2018 | Dig Deep  | Forensic | 150    | ¯\\_(ツ)_/¯ |

Download: [Google Drive](https://drive.google.com/file/d/1LRdLOJscGrFGkiTv8URkj9SpwG-zD9_b/view)

### TL;DR

If you are not interested in the detailed answer to the question "How to solve this problem?",
the short answer is that you should never trust the file deletion process and ensure that you
perform secure file deletion, especially for sensitive files or data.

Having quickly identified deleted files from on a file carving process, digging a little deeper,
we were able to get access to confidential data.

### Methology

Download archive and extract it:

```bash
# 7z x private.7z
```

The archive seems to contain a disk image, let's check it:

```bash
# file private.dd
private.dd: DOS/MBR boot sector, code offset 0x3c+2, OEM-ID "mkfs.fat", sectors/cluster 4, root entries 512, Media descriptor 0xf8, sectors/FAT 200, sectors/track 32, heads 64, sectors 204800 (volumes > 32 MB), serial number 0x2e594bed, unlabeled, FAT (16 bit)
```

Let's recover all files from this volume (including deleted ones):

```bash
# foremost -i private.dd -o out/
```

What an interesting output, it seems that this volume contains a git repository:

<center>![file_carving](/img/xiomara_2018/dig_deep/file_carving.png)</center>

A significant part of data consists of JPEG image files, let's check the EXIF data for useful information:

```bash
# find out/ -type f -exec exiftool {} \;
```

Nothing interesting here, let's check the other files...

We finally found that the repository is located in a ZIP file:

```bash
# unzip out/zip/00037165.zip -d git/
# pushd git/
```

Let's checkout branches and commits of this repository:

```bash
# git branch -a -vv
* master                1096fc8 [origin/master: en avance de 5] Still 1 week left
  remotes/origin/HEAD   -> origin/master
  remotes/origin/master 75f9b8f Initial commit
# git log -u
```

<center>![git_log](/img/xiomara_2018/dig_deep/git_log.png)</center>

Here we got the flag!

Final flag:

> xiomara{wow_autopsy_&_git_is_cool}


Creased
