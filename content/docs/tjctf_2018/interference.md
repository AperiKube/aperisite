+++
title = "Interference"
description = "TJCTF 2018 - MISC (15 pts)"
keywords = "MISC, Steganography, Image, XOR, stegsolve, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-08-07T18:19:25+01:00
weight = 20
draft = false
bref = "TJCTF 2018 - MISC (15 pts)"
toc = true
aliases = [
    "/docs/tjctf_2018_interference",
]
+++

TJCTF 2018: Interference
====================================

### Challenge details

| Event      | Challenge     | Category      | Points | Solves      |
|------------|---------------|---------------|--------|-------------|
| TJCTF 2018 | Interference  | Miscellaneous | 15     | 270 solves  |

Download: [interference.zip](/files/tjctf_2018/interference/01a68dd1c1db3bb4eb69853b32cb4dbbc22c6c05abf994733e99b928c20c5295_interference.zip) - md5: ab1e7054649da290274a02f0e01b3d40

### Description

Author:  jfrucht25
> I was looking at some images but I couldn't see them clearly. I think there's some [interference](/files/tjctf_2018/interference/01a68dd1c1db3bb4eb69853b32cb4dbbc22c6c05abf994733e99b928c20c5295_interference.zip).

### TL;DR

We had 2 images composed by white and black pixels. We used Stegsolve to apply
operations: `NOT(IMG1 ^ SUB(IMG1,IMG2))`.

### Methology

#### Looking at the files

The first thing we did was opening the files contained in [interference.zip](/files/tjctf_2018/interference/01a68dd1c1db3bb4eb69853b32cb4dbbc22c6c05abf994733e99b928c20c5295_interference.zip).<br/>
The archive was containing 2 images named v1.png and v2.png :

<style>.zimg100 img{width: 100px;height: 100px;}</style>
<center class="zimg100">
![v1.png](/files/tjctf_2018/interference/v1.png?w=100&h=100)
![v2.png](/files/tjctf_2018/interference/v2.png?w=100&h=100)
</center>

By seeing the two files, I decided to do some operations with them such as xor.

#### Operations

To test differents opetations, I used the tool named ["Stegsolve" by Caesum](/files/pragyan_2018/pictorial_mess/Stegsolve.jar).
The tool is written in Java (jar file) and allow us to combine images ("Image Combiner").<br/>
I saw a different image while using the SUB operation, I decided to save it to solved.bmp.

<center>![solved.bmp](/files/tjctf_2018/interference/solved.bmp?w=100&h=100)</center>

Then I started to do operations between v1 and v3 and here it is (I added an extra XOR then a NOT)!

<center>![final.png](/files/tjctf_2018/interference/final.png?w=100&h=100)</center>

# FLAG

By decoding qrcode we got

`tjctf{m1x1ing_and_m4tchIng_1m4g3s_15_fun}`


[Zeecka](https://twitter.com/Zeecka_)
