+++
title = "Ssleepy"
description = "TJCTF 2018 - Forensics (70 pts)"
keywords = "Forensic, Wireshark, SSL, FTP, TLS, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-08-09T08:19:25+01:00
weight = 20
draft = false
bref = "TJCTF 2018 - Forensics (70 pts)"
toc = true
aliases = [
    "/docs/tjctf_2018_ssleepy",
]
+++

# TJCTF 2018 - Ssleepy

| Event      | Challenge | Category  | Points | Solves |
| ---------- | --------- | --------- | ------ | ------ |
| TJCTF 2018 | Ssleepy   | Forensics | 70     | 123    |

### Description

This is a network task, I made a similar challenge for a school CTF, very nice to learn how to deal with Wireshark and SSL. The statement:

I found this super suspicious [transmission](https://static.tjctf.org/99870da89e552d13905dbff3fe0543ca336c4c425cb723e3f4b6c0e91a6e23e7_ssleepy.pcapng) lying around on the floor. What could be in it?

Downloadable link: https://mega.nz/#!uGp3zahY!lQnuoea-mYlBLgOzUxLwV3QX-p5Oe8YIfixrix9UaT0

(xz archive, then `xz -d <archvie_name.xz>`)

MD5 : __d6c6b47b0f944966b1afc355c84ed593__

### TL;DR

In this task the author gaves us a packet capture file (pcap). At the first look, we actually see `FTP` and `TLS` traffic.

After digging a bit in the FTP traffic, we get a `SSL private key`. I used this key to decrypt SSL packets and get a JPG picture containing the flag.

------

### State of the art

At the beginning of this challenge, it contains some common traffic, `FTP` and `SSL`. In the `FTP` traffic, an archive is transferred: `key.zip`

![1533804020114](/files/tjctf_2018/ssleepy/tjctf_ssleepy_fig1_networkTraffic.png)

<center>

__Fig 1__ - Network traffic

</center>



### Private key

Let's extract this archive. Just a little `right click -> Follow -> TCP stream` on the highlight packet in the previous Figure gave me the archive. `ZIP` are easy to identified, first two bytes are `PK`

![1533804215055](/files/tjctf_2018/ssleepy/tjctf_ssleepy_fig2_zipArchive.png)

<center>

__Fig 2__ - Packet containing ZIP archive

</center>



Just save it as brut data and you will have a `ZIP` archive.

```bash
$ file archive.raw
archive.raw: Zip archive data, at least v2.0 to extract

$ 7za x archive.raw
[...]
Processing archive: archive.raw

Extracting  ../user/ftp/files/private/server_key.pem

Everything is Ok

Size:       887
Compressed: 924

$ cat user/ftp/files/private/server_key.pem
-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQDJ3egkT4J5h4klOdnXiPB8wra4dFMZXPYZk3NWBYmlu5ImhMg7
Iao7qrv/rNe4SNN/vbTLmuo/BJ0/L5O1QDYaZMFt63tFnhTjMWPAtx7uUGBJ+JZk
PobNutrv463/tlTV0yvncL0x6Epapgtrur1b8vAl60nqcBFLxQ6EUGEUpQIDAQAB
AoGBAIywHnkn/MwMPPX30q4Xn1ukLMpzL/MuScpbbHeYVm1uQ5aa1h34AXeiL3Iq
mRGzl8uggF+icP1IjvsZgn1A6jLOATCKhmaoEldHj4OciaGpys3InFkY8sqlgjV+
OHwbYYVt7a3hrcphN6I5nUAbK8VXz7ne3r3PwN6DIubqfeMhAkEA/nPdOGK3Cgdx
vpTHnEK39XwWHOjDJURTsW78KNo1sqQbDYhGQAAlvXNoClz4dpjoVCxkIuHZ3zMC
+5IrKW0KaQJBAMsYLSGyM2WCl9NAcQ2l9lFQ1matTNy6cFmZckRv15nrF/X2mrtM
BqyxJAY/rw/FjEoQde30kWr4ZrpbI7DzWN0CQQD8Km7t1kZ3VfFPYbqADU6ppZN0
iuf6IsCectLK2ZWluCRnQMn92yeLnLdardA+GEMGSAfk2dZE8BTgo3bK0xkhAkBU
X9/oHtuBcabXye7t56QUlkvsblT/YqloX/p7+icNOAFFW0VfSK+BMTqxKeX13HFz
F7GWwUNkuHwdMd37Fq41AkBJC0pZtkbzdUzwZre8XylK8rI62xJUUdOmyowaSmi/
vEJTGtlc2+b5ztlsYjzsPFaI49fw8QNawtZj1e3CRc7w
-----END RSA PRIVATE KEY-----
```



_Note_: You can extract this archive by running `binwalk` on the PCAP ;)



### SSL decryption

I just have to add this private key to Wireshark and decrypt the SSL traffic. To do that, in Wireshark:

`Edit -> Preferences -> Protocols -> SSL-> Edit -> RSA keys list`

<center>

![1533804958734](/files/tjctf_2018/ssleepy/tjctf_ssleepy_fig3_sslConfig.png)

__Fig 3__ - SSL configuration in Wireshark

</center>



Back to the traffic, and it appears some `HTTP GET` request, containing a file `flag.jpg`.

<center>

![1533805103341](C:\Users\Maki\AppData\Local\Temp\1533805103341.png)

__Fig 4__ - HTTP traffic

</center>



### Picture extraction

Now, I know where is the flag. After checking `Decrypted SSL traffic` by doing `right click -> Follow -> SSL stream`, I finally found the JPG.

To identified a JPG, I used the same method as the archive. File signature of a `JPEG` file is `FF D8` in the header and a `JFIF` string.

<center>

![1533805745891](/files/tjctf_2018/ssleepy/tjctf_ssleepy_fig5_jpgIdentification.png)

__Fig 5__ - JPG identification

</center>

For the extract, I'll doing the same action as before, extracting as brut data the _Blue_ traffic. But here, there is not only the `JPG` file, we got `HTML` requests too, so our system is not able to find the file type.

```bash
$ file flag.raw
flag.raw: data
```



To saves us, I will use my favorite carving file tool: `binwalk`.

```bash
$ binwalk flag.raw

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
45            0x2D            JPEG image data, JFIF standard 1.01

$ foremost flag.raw
Processing: flag.raw
|*|

$ file output/jpg/00000000.jpg
output/jpg/00000000.jpg: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, baseline, precision 8, 1024x450, frames 3

# It smells flag :D
```

_Note:_ Sometime the extract argument of binwalk `-e` does not work properly, others tools like `formost` or `scalpel` works. You can also force extraction with binwalk by using `--dd=".*"` argument.



### Flag

<center>

![1533806184145](/files/tjctf_2018/ssleepy/tjctf_ssleepy_fig6_flag.png)

</center>

> TJCTF{WIRESHARK_OR_SHARKWIRE?}

<center><b>Maki</b></center>
