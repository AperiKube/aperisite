+++
title = "Ess Kyoo Ell"
description = "TJCTF 2018 - Web (40 pts)"
keywords = "Web, SQLi, SQL Injection, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-08-07T19:19:25+01:00
weight = 20
draft = false
bref = "TJCTF 2018 - Web (40 pts)"
toc = true
aliases = [
    "/docs/tjctf_2018_ess_kyoo_ell",
]
+++

TJCTF 2018: Ess Kyoo Ell
====================================

### Challenge details

| Event      | Challenge     | Category      | Points | Solves      |
|------------|---------------|---------------|--------|-------------|
| TJCTF 2018 | Ess Kyoo Ell  | Web           | 40     | 124 solves  |


### Description

Author: okulkarni

> Find the IP address of the admin user! (flag is tjctf{[ip]})

### Methology

Once on the challenge, we meet a login page. We're trying to connect using phony credentials and got a surprise.

<center>![1.PNG](/files/tjctf_2018/ess_kyoo_ell/1.PNG)</center>

A nice SQL error: ```no such column: password```. So I fire up Burp and send the request to the *Repeater*.

That error takes me thinking that the SQL request is like: ```WHERE email='x@x.x' AND password='xxx'```.
So ```WHERE first_http_post_param='first_http_post_value' AND second_http_post_param='second_http_post_value' ...```.

With that in mind, we can extend to test the existence of columns in the table. *id* is pretty much present in every SQL table, so let's try.

<center>![2.PNG](/files/tjctf_2018/ess_kyoo_ell/2.PNG)</center>

Ho, what a nice response, we get the table structure and the result for ```WHERE id=1```.
Knowing that the *admin* must probably be called *'admin'*, let's test ```username=admin```.

<center>![3.PNG](/files/tjctf_2018/ess_kyoo_ell/3.PNG)</center>

Yay!

### Flag

**tjctf{145.3.1.213}**

*DrStache*
