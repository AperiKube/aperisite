+++
title = "Grid parser"
description = "TJCTF 2018 - Forensics (45 pts)"
keywords = "Forensic, PNG, Excel, fcrackzip, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-08-09T08:19:25+01:00
weight = 20
draft = false
bref = "TJCTF 2018 - Forensics (45 pts)"
toc = true
aliases = [
    "/docs/tjctf_2018_gridparser",
]
+++

# TJCTF 2018 - Grid Parser

| Event      | Challenge   | Category  | Points | Solves |
| ---------- | ----------- | --------- | ------ | ------ |
| TJCTF 2018 | Grid Parser | Forensics | 45     | 74     |

### Description

This is forensics task, not very difficult, with a pinch of guessing. The statement:

I found this [grid](https://static.tjctf.org/ecdb2ff56241299271bf44268880e46a304f50d212ae05dab586e3843ad59d50_movies.grid) while doing some parsing. Something about it just doesn't seem right though...

Downloadable link: https://mega.nz/#!Pax30S6D!rC-eTwOaEUZwV5WxvvjW0UR5-RD5MrP6_L3EsGTOaHo

(xz archive, then `xz -d <archvie_name.xz>`)

MD5 : __5259c642e8737f06a615699a29dcd2b9__

### TL;DR

In this task the author gaves us an Excel file. As all Office file, it's possible to _unzip_ them. With a quick look, we notice a PNG file in decompressed data.

This PNG is not displayed in the Excel file. Then, `binwalk` extract a little encrypted zip from the PNG file.

Finally, I used `fcrackzip`  to bruteforce the password (secret key = `px`).

---

### State of the art

So, I started this task with a `grid` file. But in fact it was an Excel file:

```bash
$ file ecdb2ff56241299271bf44268880e46a304f50d212ae05dab586e3843ad59d50_movies.grid
ecdb2ff56241299271bf44268880e46a304f50d212ae05dab586e3843ad59d50_movies.grid: Microsoft OOXML

$ mv ecdb2ff56241299271bf44268880e46a304f50d212ae05dab586e3843ad59d50_movies.grid chall.xlsx
```



This file contains lot of Marvel movies, some useless external link etc...

![1533798058576](/files/tjctf_2018/gridparser/tjctf_gridParser_excel.png)

<center>

__Fig 1__ - Excel data

</center>



### Unzip them all

Now, let's decompress the Excel file and see which files this archive really contain.

```bash
$ 7za x chall.xls

7-Zip (A) [64] 9.20  Copyright (c) 1999-2010 Igor Pavlov  2010-11-18
p7zip Version 9.20 (locale=en_US.UTF-8,Utf16=on,HugeFiles=on,8 CPUs)

Processing archive: chall.xls

Extracting  [Content_Types].xml
Extracting  _rels
Extracting  _rels/.rels
Extracting  xl
[...]
Everything is Ok

Folders: 7
Files: 21
Size:       248367
Compressed: 47716

$ tree .
.
├ ~$chall.xls
├ ~$chall.xlsx
├ chall.xlsx
├ [Content_Types].xml
├ _rels
└ xl
    ├ comments1.xml
    ├ drawings
    │   ├ drawing1.xml
    │   ├ drawing2.xml
    │   ├ drawing3.xml
    │   ├ drawing4.xml
    │   └ vmlDrawing1.vml
    ├ media
    │   └ password.png
    ├ _rels
    │   └ workbook.xml.rels
    ├ sharedStrings.xml
    ├ styles.xml
    ├ workbook.xml
    └ worksheets
        ├ _rels
        │   ├ sheet1.xml.rels
        │   ├ sheet2.xml.rels
        │   ├ sheet3.xml.rels
        │   └ sheet4.xml.rels
        ├ sheet1.xml
        ├ sheet2.xml
        ├ sheet3.xml
        └ sheet4.xml

7 directories, 23 files
```



Ok, now we see a `password.png` file not present in the Excel tab. Let's dig.



### Hidden ZIP

 A quick analysis shows us a hidden `ZIP archive`. So let's extracting it.

```bash
$ binwalk password.png

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             PNG image, 207 x 138, 8-bit grayscale, non-interlaced
134           0x86            Zlib compressed data, best compression
1780          0x6F4           Zip archive data, encrypted at least v1.0 to extract, compressed size: 41, uncompressed size: 29, name: flag.txt
1981          0x7BD           End of Zip archive

$ binwalk -e password.png
$ ls -la
-rwxrwxrwx 1 maki maki   223 Aug  9 09:07 6F4.zip
-rwxrwxrwx 1 maki maki 28704 Aug  9 09:07 86
-rwxrwxrwx 1 maki maki  1869 Aug  9 09:07 86.zlib
-rwxrwxrwx 1 maki maki    29 Aug  3 17:02 flag.txt
```



Unfortunately, `flag.txt` file contains garbage... When I tried to decompress the file, the archive asks for a password.



### Bruteforce time

I doing CTF since quite month now (years ?), so with my little experience I can say something:

> When a ZIP file appears, hashcat / john / fcrackzip are your best friends!

And it's the case here. The `password.png` file shows 2 stars, then let's try a bruteforce on 2 characters using `fcrackzip`.

```bash
$ fcrackzip --brute-force --charset a1 --length 1-3 --use-unzip 6F4.zip


PASSWORD FOUND!!!!: pw == px
# w00t \o/

$ 7za x 6F4.zip
[...]
Extracting  flag.txt
Enter password (will not be echoed) :


Everything is Ok

Size:       29
Compressed: 223
```



And now, just `cat` the extracted file to get the flag!



### Flag

> tjctf{n0t_5u5_4t_4LL_r1gHt?}



<center>__Maki__</center>
