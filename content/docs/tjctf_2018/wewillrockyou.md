+++
title = "We will rock you"
description = "TJCTF 2018 - Forensics (75 pts)"
keywords = "Forensic, Wallet, Bruteforce, Cryptocurrency, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-08-09T08:19:25+01:00
weight = 20
draft = false
bref = "TJCTF 2018 - Forensics (75 pts)"
toc = true
aliases = [
    "/docs/tjctf_2018_wewillrockyou",
]
+++

# TJCTF 2018 - We Will Rock You

| Event      | Challenge        | Category  | Points | Solves |
| ---------- | ---------------- | --------- | ------ | ------ |
| TJCTF 2018 | We Will Rock You | Forensics | 75     | 45     |

### Description

This is a <strike>forensic</strike> bruteforce task, if you get the right tools with the right wordlist, it goes well. The statement:

Help! I lost the password to my Dogecoin [wallet](https://static.tjctf.org/9d7e6203fb6e2c14646c63bea94e48001b9317c86bec229c3e063904a168dfad_wallet.dat). I had like 10 Dogecoin in there. If you get the password, you'll get the flag! Flag format: tjctf{*password*}

Downloadable link: https://mega.nz/#!OXxhxKpT!CZGnoHTvneTjfzkxvHD0VCXa_j9h5sqWZastamgm4_I

(xz archive, then `xz -d <archvie_name.xz>`)

MD5 : __e52d74fe8e2eff36559b9e17d93ae4f3__

### TL;DR

In this task the author gaves us a cryptocurrency wallet. You just have to bruteforce the wallet with `bruteforce-wallet` on GitHub and the `RockYou` wordlist.

* bruteforce-wallet: https://github.com/glv2/bruteforce-wallet
* Rockyou: https://wiki.skullsecurity.org/Passwords

------

### State of the art

A wallet in cryptocurrency is encrypted using a strong cipher suite. But, it remains a little file encrypted with a key, so I just have to find this key. With the name of the task `We Will Rock You`, I immediately think to `Rockyou` wordlist the bruteforce it.



### Bruteforce

After a quick search on Google, I find this GitHub repo: https://github.com/glv2/bruteforce-wallet

```bash
$ git clone https://github.com/glv2/bruteforce-wallet.git
$ ./autogen.sh
$ sudo apt install libdb-dev libssl-dev -y
$ ./configure
$ make
$ ./bruteforce-wallet -t 6 -f rockyou_dic ../9d7e6203fb6e2c14646c63bea94e48001b9317c86bec229c3e063904a168dfad_wallet.dat
Warning: using dictionary mode, ignoring options -b, -e, -l, -m and -s.

Tried passwords: 1
Tried passwords per second: inf
Last tried password: zeufhezuf

Password found: tinkerbell
```



### Flag

> tjctf{tinkerbell}

<center>__Maki__</center>
