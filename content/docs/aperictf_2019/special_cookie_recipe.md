+++
title = "Special Cookie Recipe"
description = "Aperi'CTF 2019 - Steganography (50 pts)"
keywords = "Steganography, Casse, XSS, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Steganography (50 pts)"
toc = true
+++

Aperi'CTF 2019 - Special Cookie Recipe
============================================

### Challenge details

| Event          | Challenge             | Category      | Points | Solves |
|----------------|-----------------------|---------------|--------|--------|
| Aperi'CTF 2019 | Special Cookie Recipe | Steganography |  50    |  20    |

> Un ami m'a donné une recette pour faire des cookies, mais il me semble qu'il a quelque chose d'autre à me dire... Mais quoi ?
> Aide moi à retrouver son message s'il te plaît !

<u>**Fichier&nbsp;:**</u> [chall_cookies.txt](/files/aperictf_2019/special_cookie_recipe/chall_cookies.txt) - md5sum: 44fdec242d0b4472fed42ebe89d45355


### TL;DR

Le message a été caché dans la casse. Il suffit de prendre un "0" pour chaque lettre minuscule, un "1" pour chaque lettre majuscule, et passer le tout en ascii ! :)


### Methogolody

Voici le fichier original:

```
rEcette CoOkIes pouR 20 cOokIes :

IngReDIeNTS :
-75G dE SuCRe en POuDRe
-1 OeuF
-1 sAChet De lEVuRE CHiMIQuE
-125g De FArINe
-1/2 brIQue De CrEME fRaicHE EpAissE
-100G de ChOcOLAT EnViron

preparation :
1 : verser dans un saladier : sucre, oeuf, creme
2 : melanger
3 : rajouter farine et levure
4 : melanger
5 : ajouter les pepites de chocolat et melanger
6 : prechauffer le four a 180deg
7 : disposer les cookies sur du papier sulfurise
8 : laisser cuire environ 10 minutes
9 : enjoy
```

Le message a été caché dans la casse. Il suffit de prendre un "0" pour chaque lettre minuscule, un "1" pour chaque lettre majuscule, et passer le tout en ascii ! :)

```python
#!/usr/bin/env python2

with open("chall_cookies.txt", "r") as f:
    encoded = f.read()

flag_bin = str()
for e in encoded:
    if e in string.lowercase:
        flag_bin += "0"
    if e in string.uppercase:
        flag_bin += "1"
print flag_bin
chars = map(''.join, zip(*[iter(flag_bin)]*8))
flag = str()
for c in chars:
    flag += chr(int(c, 2))
print flag
```

### Flag

Flag : `APRK{ciboulette}`


*Happy hacking !*

[Laluka](https://twitter.com/TheLaluka)
