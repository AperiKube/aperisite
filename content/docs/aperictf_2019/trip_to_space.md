+++
title = "Trip to space"
description = "Aperi'CTF 2019 - Steganography (50 pts)"
keywords = "Steg, Steganography, Stega, Gamma, , WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Steganography (50 pts)"
toc = true
+++

Aperi'CTF 2019 - Trip to space
============================================

### Challenge details

| Event                    | Challenge     | Category      | Points | Solves      |
|--------------------------|---------------|---------------|--------|-------------|
| Aperi'CTF 2019           | Trip to space | Steganography | 50     |  38         |

Votre lecteur d'image vous ment-il ?

Challenge: [Trip_to_space.zip](/files/aperictf_2019/trip_to_space/Trip_to_space.zip) - md5sum : a93693fb6f6ebc813510e60582e9d8d3

### Methodology

#### Open it !

Open the image in a web browser or in a decent image viewer.

Gnome, Mobile, Windows... :
![bug.jpg](/files/aperictf_2019/trip_to_space/bug.jpg)

Web browser, MAC, ... :
![ok.jpg](/files/aperictf_2019/trip_to_space/ok.jpg)

#### Explanation

Some image viewer implement gamma correction. This correction has a real impact on the image (here, the flag is displayed or not).

Fore more information about gamma correction and gamma steganography:
- [Gamma steganography](https://carlmastrangelo.com/blog/gamma-steganography)
- [Gamma error in picture scaling](http://www.ericbrasseur.org/gamma.html)

#### Flag

`APRK{G4mm4-St3g4n0gr4phy}`

[Zeecka](https://twitter.com/Zeecka_)
