+++
title = "x32 Advanced"
description = "Aperi'CTF 2019 - MISC (175 pts)"
keywords = "Prog, Programmation, Shellcoding, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - MISC (175 pts)"
toc = true
+++

Aperi'CTF 2019: x32 Advanced
============================================

### Challenge details

| Event                    | Challenge    | Category | Points | Solves      |
|--------------------------|--------------|----------|--------|-------------|
| Aperi'CTF 2019            | x32 Advanced | MISC     | 175    | 11          |

Maintenant que vous vous êtes fait la main, prouvez que vous êtes à la hauteur de votre mission. Votre tâche est d'écrire un programme qui récupérera 30 caractères de STDIN et appliquera l'opération XOR avec la clé "X" avant d'afficher le résultat sur STDOUT.

```bash
nc x32.aperictf.fr 32322
```

<u>**Note&nbsp;:**</u> Lisez [la documentation](/files/aperictf_2019/x32_intro/documentation.zip) de ce langage avant de commencer.<br/>
<u>**Ressource complémentaire&nbsp;:**</u> [la documentation](/files/aperictf_2019/x32_intro/documentation.zip) - md5sum: 5ac1ef34b4641b319281e65d80e84411

### Methodology

This task is a little bit more complicated and requires the use of almost every x32 instructions.

There can be multiple solutions but the following code performs the desired operation :

```
---- START ----
set A1, 0x1E
sub SP, 0x1E
in SP
set R4, SP      # stores a reference to the buffer
set R3, 0x0     # counter
<for_loop>      # label definition
set R2, R4
add R2, R3
load R1, R2     # takes byte of the input starting from the top
xor R1, 0x58    # 0x58 = 'X'
store R2, R1    # overwrite the byte on the stack
add R3, 0x1     # move on to the next one
cmp R3, 0x1F    # reached the end ?
jl for_loop     # if not, loop
out R4          # print the result
---- END ----
```

```
Test 1/10 : SUCCESS
Test 2/10 : SUCCESS
Test 3/10 : SUCCESS
Test 4/10 : SUCCESS
Test 5/10 : SUCCESS
Test 6/10 : SUCCESS
Test 7/10 : SUCCESS
Test 8/10 : SUCCESS
Test 9/10 : SUCCESS
Test 10/10 : SUCCESS
Your code passed all the tests, here you go :
APRK{1_Th1nk_Y0u_4r3_r34dy_f0r_Th15}
```

### Flag

`APRK{1_Th1nk_Y0u_4r3_r34dy_f0r_Th15}`

ENOENT
