+++
title = "F*ck it"
description = "Aperi'CTF 2019 - Mobile (175 pts)"
keywords = "obfuscation, mobile, frida, APK, fuck it, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Mobile (175 pts)"
toc = true
+++

# Aperi'CTF 2019 - F*ck It

### Challenge details

| Event                    | Challenge | Category      | Points | Solves      |
|--------------------------|-----------|---------------|--------|-------------|
| Aperi'CTF 2019           | F*ck it   | Mobile        | 175    | 4           |

Oula ! Ça risque de prendre du temps ... (oui, il faut retrouver le mot de passe ;) )

Fichier: [app-debug.s.apk](/files/aperictf_2019/fuck_it/app-debug.s.apk) - md5sum : a2ab0db5735b36dc51165de1b1d4d0d4

### Frida

This application is obfuscated. The easiest way to solve this task is to use [frida](https://github.com/frida/frida):

```javascript
// frida -U -l script.js -f com.example.challenge --no-pause
Java.perform(function(){
    const CryptoUtil = Java.use("com.example.challenge.CryptoUtil")

    CryptoUtil.af7bd616b.implementation = function(x){
        var out = this.af7bd616b(x)
        
        
        var buffer = Java.array('byte', out);
        console.log(buffer.length);
        var result = "";
        for(var i = 0; i < buffer.length; ++i){
            result+= (String.fromCharCode(buffer[i]));
        }
        console.log(result);

        return out
    }
})
```

### Flag

`APRK{ObfuscationIsCancer}`

Areizen
