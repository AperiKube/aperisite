+++
title = "x32 Intro"
description = "Aperi'CTF 2019 - MISC (100 pts)"
keywords = "Prog, Programmation, Shellcoding, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - MISC (100 pts)"
toc = true
+++

Aperi'CTF 2019: x32 Intro
============================================

### Challenge details

| Event                    | Challenge | Category | Points | Solves      |
|--------------------------|-----------|----------|--------|-------------|
| Aperi'CTF 2019            | x32 Intro | MISC     | 100    | 14          |

La société ENO.corp fabrique des cartes à puce pour microcontrôleurs.
Ces cartes sont programmables grâce au langage d'assemblage inventé spécialement pour l'occasion: x32 !

Votre mission est de devenir un expert afin d'analyser les microcontrôleurs qu'ils vendent et les exploiter.

Afin de vous familiariser avec ce nouveau langage, votre tâche est d'écrire un mini-programme qui récupérera 10 caractères de STDIN et les affichera sur STDOUT.

```bash
nc x32.aperictf.fr 32321
```

<u>**Note&nbsp;:**</u> Lisez [la documentation](/files/aperictf_2019/x32_intro/documentation.zip) de ce langage avant de commencer.<br/>
<u>**Ressource complémentaire&nbsp;:**</u> [la documentation](/files/aperictf_2019/x32_intro/documentation.zip) - md5sum: 5ac1ef34b4641b319281e65d80e84411

### Methodology

The following code is self explanatory. The only difficulty was thinking to save space on the stack before writing, otherwise an out of bounds exception occurs. The server is kind enough to give details on where a problem is located and the causes, should it be during assembling, running or testing.

```
---- START ----
set A1, 0xa     # We will receive and send 10 bytes
sub SP, 0xa     # Reserve enough space on the stack to hold our input
in SP           # Reads A1 bytes from STDIN and stores it in the stack at SP
out SP          # Writes A1 bytes from the stack at SP, to STDOUT
---- END ----
```

```
Test 1/10 : SUCCESS
Test 2/10 : SUCCESS
Test 3/10 : SUCCESS
Test 4/10 : SUCCESS
Test 5/10 : SUCCESS
Test 6/10 : SUCCESS
Test 7/10 : SUCCESS
Test 8/10 : SUCCESS
Test 9/10 : SUCCESS
Test 10/10 : SUCCESS
Your code passed all the tests, here you go :
APRK{b4by_x32_t0_b3g1n}
```

### Flag

`APRK{b4by_x32_t0_b3g1n}`

ENOENT
