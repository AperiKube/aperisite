+++
title = "Émoji 🥳"
description = "Aperi'CTF 2019 - MISC (50 pts)"
keywords = "MISC, Programming, Count, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - MISC (50 pts)"
toc = true
+++

Aperi'CTF 2019 - Émoji 🥳
============================================

### Challenge details

| Event                    | Challenge      | Category | Points | Solves      |
|--------------------------|----------------|----------|--------|-------------|
| Aperi'CTF 2019           | Émoji 🥳       | MISC     | 50     | 24         |

Comptez le nombre d'emoji demandé par le serveur. Vous avez 3 secondes.

`https://emoji.aperictf.fr`

### TL;DR

It was a programming challenge. We were given a website with a random list of emoji and we had to count the number of a given emoji.

### Methodology

<center>![start.png](/files/aperictf_2019/emoji/start.png)</center><br>

The web page ask us to count the number of emoji for 2 given (random) emoji and post the result in less than 3 seconds.

For this I made a python script:

```python
import requests
import time

url = "http://emoji.aperictf.fr/"

s = requests.session()
r = s.get(url).text

recherche = r.split("nombre de ")[1]

r1,r2 = recherche.split(" et de ")

nb1 = r.count(r1) - 2
nb2 = r.count(r2[0]) - 2

r = s.post(url,data={"nb1":nb1,"nb2":nb2}).text

print(r)
```

Output:

```html
<!doctype html>
<html>
<head>
    <meta charset="UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" href="files/style.css"/>
    <link rel="shortcut icon" href="files/logo.png">
</head>
<body>
    <form action="/" method="post"><div id="form">
    APRK{Aimemoj1}    </div></form>
<script src="files/jquery-3.4.0.min.js"></script>
<script src="files/js.js"></script>
</body>
</html>
```

#### Flag

`APRK{Aimemoj1}`

[Zeecka](https://twitter.com/Zeecka_)
