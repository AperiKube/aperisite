+++
title = "Elliot"
description = "Aperi'CTF 2019 - OSINT (100 pts)"
keywords = "OSINT, Zip, Bruteforce, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - OSINT (100 pts)"
toc = true
+++

Aperi'CTF 2019 - Elliot
============================================

### Challenge details

| Event                    | Challenge     | Category  | Points | Solves      |
|--------------------------|---------------|-----------|--------|-------------|
| Aperi'CTF 2019           | Elliot        | OSINT     | 100    |   1         |

Un zip chiffré a été volé au personnage fictif "Elliot Alderson" de la série TV "Mr. Robot". 
Ce dernier utiliserai des mots de passes inférieurs à 30 caractères basés sur des informations personelles et publiques.

Afin d'ouvrir son fichier zip, vous effectuerez une étape d'OSINT dans le but de générer une wordlist.

Challenge: [Private_Data.zip.zip](/files/aperictf_2019/elliot/Private_Data.zip) - md5sum : ec67cc5e6bf208d4070e848bbf459124

### Methodology

#### OSINT

To construct our wordlist, we need to do some research on Elliot Alderson.
Thanks to internet, we can grab some page with a lot of information and we can construct a little profile:
```text
Elliot Alderson
Mr. Robot
17/09/1986
New York
Evil Corp
E Corp
Allsafe
Fsociety
Angela
Darlene
```

#### Permutations

From this little wordlist, we'll do some permutations. We can also split the birth date, use lowercase/uppercase... and concatenate each words !
For this, you can use python. I decided to use [BEWgor](https://github.com/berzerk0/BEWGor) (ported to python3) which already do that !
Once runned, I got a ~1 M lines wordlist named "BEWGor_Wordlist.txt" including passwords such as "EvilCorpfsocietydarlene" or "ELLIOTAngelaECORP".

#### Hash and Bruteforce

To bruteforce the zip file you can use fcrackzip but it's pretty slow. I decided to use "john" and "zip2john":

```bash
zip2john Private_Data.zip > zip.hash
john zip.hash --wordlist=BEWGor_Wordlist.txt
```

```text
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 8 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
Elliot1986Fsociety (Private_Data.zip/Secret_Contact.txt)
1g 0:00:00:00 DONE (2019-07-09 11:19) 12.50g/s 6553Kp/s 6553Kc/s 6553KC/s ANGELAecorp17986..Elliotevil-corpNy
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

We recovered the password `Elliot1986Fsociety`!

Now we can access the file Secret_Contact.txt:

```text
Flag is : APRK{Elliot1986Fsociety}
```

#### Flag

`APRK{Elliot1986Fsociety}`

[Zeecka](https://twitter.com/Zeecka_)
