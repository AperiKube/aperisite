+++
title = "Sometimes"
description = "Aperi'CTF 2019 - Web (50 pts)"
keywords = "Web, Default, Credentials, Admin, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Web (50 pts)"
toc = true
+++

Aperi'CTF 2019 - Sometimes
============================================

### Challenge details

| Event                    | Challenge      | Category | Points | Solves      |
|--------------------------|----------------|----------|--------|-------------|
| Aperi'CTF 2019           | Sometimes      | Web      | 50     |  61         |

Vous avez découvert un portail de serveur C&C sur l'entreprise de votre client. Retrouvez la liste des machines infectées par le botnet.

`https://sometimes.aperictf.fr`

### Methodology

<center>![form.png](/files/aperictf_2019/sometimes/form.png)</center><br>

The website is a C&C pannel. We can start enumerate and try default credentials like "admin" as user and "admin" as password.
And...

<center>![flag.png](/files/aperictf_2019/sometimes/flag.png)</center><br>

#### Flag

`APRK{F4k3-Ru$$1an-C2}`

[Zeecka](https://twitter.com/Zeecka_)
