+++
title = "Pick up the phone 2"
description = "Aperi'CTF 2019 - Forensic (1O0 pts)"
keywords = "SIP, call, VOIP, replay, audio, sound, DTMF, encoding, forensic, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Forensic (1O0 pts)"
toc = true
+++

# Aperi'CTF 2019 - Pick up the phone 2

### Challenge details

| Event          | Challenge           | Category  | Points | Solves |
|----------------|---------------------|-----------|--------|--------|
| Aperi'CTF 2019 | Pick up the phone 2 | Forensic  | 100    | 8      |

We’re given a [call.pcap](/files/aperictf_2019/pick_up_the_phone_2/call.pcap) PCAP file.

Task description:

 > Following the President's attack on our company's sales manager, our engineering team designed a new authentication system based on a single-use token (HOTP).
 >
 > In order to retrieve this token, users can call a dedicated voice service and retrieve a new token on their LCD screen.
 >
 > Despite the security guidelines, some users tend to create a token list by calling the voice service several times in order to use them later...
 >
 > However, our VoIP communication system appears to have been tapped and one of our CEO's unused tokens leaked.
 >
 > Investigate and find the leaked authentication token to allow our technicians to revoke it.

### TL;DR

Replay RTP Stream with Wireshark on a virtual audio device, decode the DTMF sequences.

### PCAP analysis

Since, we're given a PCAP file, let's enumerate the protocols using `tshark`:

```bash
tshark -r files/call.pcap -T fields -e frame.protocols | sort | uniq | grep -vP "((tcp)|(udp)|(data))$"
```

```
eth:ethertype:ip:udp:rtp
eth:ethertype:ip:udp:sip
eth:ethertype:ip:udp:sip:sdp
```

Ok, this capture seems to contain a SIP/RTP conversation, that's perfect! Let's analyze it using Wireshark!

First, let's filter the packets using the `rtp` filter:

![wireshark_rtp_filter](/img/aperictf_2019/pick_up_the_phone_2/wireshark_rtp_filter.png)

Using Wireshark, there's an interesting feature when we're anaylzing an RTP stream that allows us to replay voice session under `Telephony > RTP > RTP Streams`:

![wireshark_rtp_streams](/img/aperictf_2019/pick_up_the_phone_2/wireshark_rtp_streams.png)

There is only one RTP stream containing a [G.722](https://en.wikipedia.org/wiki/G.722) encoded communication, we can replay it using the RTP player under `Analyze > Play Streams`:

![wireshark_rtp_player](/img/aperictf_2019/pick_up_the_phone_2/wireshark_rtp_player.png)

Listening to the RTP stream, we can notice that there's no voice at all, only a beep-sequence which after few web searches appears to be [dual-tone multi-frequency signaling](https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling) sequence.

To decode the DTMF sequence, we can use our auditory abilities to recognize and decode the touch tones or use decoders.

Since the payload's 50 seconds long, we'll focus on using a decoder here.

### Exporting DTMF sequences

Using a Linux environment, we can create a virtual audio device to replay the RTP stream and record it using Audacity.

Firstly, we need to load the `module-null-sink` module on PulseAudio in order to create an audio sink device:

```bash
pactl load-module module-null-sink sink_name=virtual-sound-device
```

Now using the `pavucontrol` GUI interface, we can check that our virtual device has been created:

![pulseaudio_virtual_sound_device](/img/aperictf_2019/pick_up_the_phone_2/pulseaudio_virtual_sound_device.png)

If our audio sink device is listed, we can now run Audacity and start a recording session on the default line using a mono channel:

![audacity_recording](/img/aperictf_2019/pick_up_the_phone_2/audacity_recording.png)

On the `pavucontrol`, let's just switch the recording device associated to Audacity from `Built-in Audio Analog Stereo` to `Monitor of Null Output`:

![pulseaudio_audacity_recoding_sink_device](/img/aperictf_2019/pick_up_the_phone_2/pulseaudio_audacity_recoding_sink_device.png)

We can now replay the RTP stream on Wireshark using the `virtual-sound-device` and see the output directly on Audacity:

![wireshark_rtp_replay_sink](/img/aperictf_2019/pick_up_the_phone_2/wireshark_rtp_replay_sink.png) ![audacity_dtmf](/img/aperictf_2019/pick_up_the_phone_2/audacity_dtmf.png)

When the entire conversation is recorded, we can stop the capture and export the audio to a PCM 8KHz WAV file!

### Decoding DTMF sequences

Now, we just need to split the entire conversation in multiple samples with a maximum of 10 seconds and decode them using [dialabc](http://dialabc.com/sound/detect/index.html):

![dialabc_decoding](/img/aperictf_2019/pick_up_the_phone_2/dialabc_decoding.png)

Another faster and neat way (credits to [Yann BREUT](https://www.linkedin.com/in/yann-breut-30229614a) from [Airbus CyberSecurity](https://www.linkedin.com/company/airbuscyber/) for this solution) is to decode the DTMF sequences using [`multimon-ng`](https://github.com/EliasOenal/multimon-ng):

```bash
multimon-ng -t wav -a dtmf dtmf.wav
```

We should obtain the following result:

```raw
65#80#82#75#123#80#72#114#51#52#107#49#110#54#125
```

We recognize the use of a dialpad which only allows the use of the following charset:

```raw
123456789*0#
```

We can assume that the message is composed of ASCII decimal values separated by `#`. Let's decode it using Python:

```python
message = '65#80#82#75#123#80#72#114#51#52#107#49#110#54#125'
decoded = ''.join(map(chr, map(int, message.split('#'))))
print(decoded)
```

The final flag is `APRK{PHr34k1n6!}`

*Happy Hacking!*

[*Creased*](https://twitter.com/Creased_)
