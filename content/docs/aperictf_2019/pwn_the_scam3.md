+++
title = "Pwn The Scam 3 - Blog"
description = "Aperi'CTF 2019 - OSINT (100 pts)"
keywords = "OSINT, Reverse Lookup, hostname, virustotal, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - OSINT (100 pts)"
toc = true
+++

Aperi'CTF 2019 - Pwn The Scam 3 - Blog
============================================

### Challenge details

| Event                    | Challenge             | Category | Points | Solves      |
|--------------------------|-----------------------|----------|--------|-------------|
| Aperi'CTF 2019           | Pwn The Scam 3 - Blog | OSINT    | 100    |   7         |

Un [site de scam Bitcoin](http://ylsspycahtqrv3u2.onion) a été découvert sur TOR. Vous avez été missionné pour en prendre le contrôle.

> *Pwn The Scam* est un challenge d'OSINT, il n'y a pas de vulnerabilité web à exploiter!
> Format de flag : *APRK{flag}*.

Maintenant que vous avez en possession l'IP, continuez sur cette piste.

### TL;DR
Reverse lookup with `137.74.112.46` on Virus Total leads to `nothing-here.com`. Flag is on contact page.

### Methodology

#### Reverse lookup

In order to get more information, we need a list of every domain names and vhosts. For this, we can use the IP on different search engine such as [https://ping.eu/rev-lookup/](https://ping.eu/rev-lookup/), [Google](https://google.com), [Yandex](https://yandex.com) and more specific search engine like [Virus Total](https://www.virustotal.com/) or [Community riskiq](https://community.riskiq.com).

Finally, we got results with
[https://community.riskiq.com/search/137.74.112.46](https://community.riskiq.com/search/137.74.112.46) : [`nothing-here.com`](http://nothing-here.com)
and [https://www.virustotal.com/#/ip-address/137.74.112.46](https://www.virustotal.com/#/ip-address/137.74.112.46) : [`nothing-here.com`](http://nothing-here.com).

<center>![dns.jpeg](/files/aperictf_2019/pwn_the_scam3/dns.jpeg)</center>

#### Crawling

Now that we have the new hostname, we can browse the website.

<center>![bitcoins.jpeg](/files/aperictf_2019/pwn_the_scam3/bitcoins.jpeg)</center>
<br>
Once again the website is about bitcoins ! On the last page (contact) we got the flag in "informations" field.
<br><br>
<center>![contact.jpeg](/files/aperictf_2019/pwn_the_scam3/contact.jpeg)</center>

#### Flag

`APRK{fR0M_IP_2_dN5}`

Challenge by [DrStache](https://twitter.com/drstache_) , WriteUp by [Zeecka](https://twitter.com/Zeecka_)
