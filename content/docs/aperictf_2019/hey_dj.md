+++
title = "Hey DJ"
description = "Aperi'CTF 2019 - OSINT (175 pts)"
keywords = "OSINT, Soundcloud, Mail, Exif, Metadata, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - OSINT (175 pts)"
toc = true
+++

Aperi'CTF 2019 - Hey DJ
============================================

### Challenge details

| Event                    | Challenge | Category | Points | Solves      |
|--------------------------|-----------|----------|--------|-------------|
| Aperi'CTF 2019           | Hey DJ    | OSINT    | 175    |   4         |

Votre ami vous assure que sa compositrice préférée `Twisore` garde son identité secrète. Prouvez-lui le contraire en investiguant.
Le flag est sous la forme `APRK{SHA1(NOMPRENOM)}`.<br>
Par exemple, si l'artiste s'appelle `Foo BAR`, alors le flag serait `APRK{f100629727ce6a2c99c4bb9d6992e6275983dc7f}`.<br>
Note: Il s'agit d'un challenge **réaliste**.<br>
<br>
Credits: Compositeur original des morceaux - [LIND](https://soundcloud.com/reallind) (ne fait pas partie du challenge)<br>

### TL;DR
Search for the profile on [soundcloud](https://soundcloud.com) , download the available files. Run `exiftool` on it and get the name from metadata. Second part is to send a mail to [twisore999@gmail.com](mailto:twisore999@gmail.com) (in the description). We'll get an automatic answer containing the first name.

### Methodology

#### Social Network

Since `Twisore` is an artiste, I'll try to look for her on differents social networks such as... [soundcloud](https://soundcloud.com) !
I search the pseudo `Twisore` on the website and got one artist: [Twisore](https://soundcloud.com/twisore).


<center>![searchuser.png](/files/aperictf_2019/hey_dj/searchuser.png)</center><br/>
Search URL : [https://soundcloud.com/search/people?q=Twisore](https://soundcloud.com/search/people?q=Twisore)<br>
<br>
Profile:<br>
<br>
<center>![profile1.png](/files/aperictf_2019/hey_dj/profile1.png)</center>

#### Informations gathering

First of all, we got an email address in the profile description: **[twisore999@gmail.com](mailto:twisore999@gmail.com)**.

After listening to the sounds we do not get any information.
We can notice that two of the five sound can be download: `Concealed(edit Short)` and `Nephelim (preview)` (see here *"Télécharger un fichier"* in French).

![download.png](/files/aperictf_2019/hey_dj/download.png)

We download the files (need to be connected) and got 2 files:
- Twisore - Concealed(edit short).mp3
- Twisore - Nephelim (preview).mp3

I ran exiftool on the two files to check for metadata:

```bash
exiftool "Twisore - Concealed(edit short).mp3"
```
```text
ExifTool Version Number         : 10.40
File Name                       : Twisore - Concealed(edit short).mp3
Directory                       : .
File Size                       : 8.5 MB
File Modification Date/Time     : 2019:05:06 17:04:53+02:00
File Access Date/Time           : 2019:05:06 17:04:58+02:00
File Inode Change Date/Time     : 2019:05:06 17:04:53+02:00
File Permissions                : rwxrwxr-x
File Type                       : MP3
File Type Extension             : mp3
MIME Type                       : audio/mpeg
MPEG Audio Version              : 1
Audio Layer                     : 3
Audio Bitrate                   : 320 kbps
Sample Rate                     : 44100
Channel Mode                    : Joint Stereo
MS Stereo                       : On
Intensity Stereo                : Off
Copyright Flag                  : True
Original Media                  : True
Emphasis                        : None
ID3 Size                        : 56009
Picture MIME Type               : image/jpeg
Picture Type                    : Front Cover
Picture Description             :
Picture                         : (Binary data 53799 bytes, use -b option to extract)
Title                           :
Artist                          :
Album                           :
Year                            :
Comment                         :
Genre                           : None
Duration                        : 0:03:41 (approx)
```

No information on this one, lets check the other one:

```bash
exiftool "Twisore - Nephelim (preview).mp3"
```
```text
ExifTool Version Number         : 10.40
File Name                       : Twisore - Nephelim (preview).mp3
Directory                       : .
File Size                       : 1463 kB
File Modification Date/Time     : 2019:05:06 15:26:43+02:00
File Access Date/Time           : 2019:05:06 15:26:47+02:00
File Inode Change Date/Time     : 2019:05:06 15:26:43+02:00
File Permissions                : rwxrwxr-x
File Type                       : MP3
File Type Extension             : mp3
MIME Type                       : audio/mpeg
MPEG Audio Version              : 1
Audio Layer                     : 3
Audio Bitrate                   : 320 kbps
Sample Rate                     : 44100
Channel Mode                    : Joint Stereo
MS Stereo                       : On
Intensity Stereo                : Off
Copyright Flag                  : True
Original Media                  : True
Emphasis                        : None
ID3 Size                        : 56086
User Defined Text               : (PATH) C:\Users\J.MOORE\My Music
Picture MIME Type               : image/jpeg
Picture Type                    : Front Cover
Picture Description             :
Picture                         : (Binary data 53799 bytes, use -b option to extract)
Title                           :
Artist                          :
Album                           :
Year                            :
Comment                         :
Genre                           : None
Duration                        : 0:00:36 (approx)
```

We got a "PATH" tag:
```text
User Defined Text               : (PATH) C:\Users\J.MOORE\My Music
```

In this path we can recover the name: `MOORE` and the first letter of the firstname: `J`.

#### First Name

We can now have a look to the email addresse we found: **[twisore999@gmail.com](mailto:twisore999@gmail.com)**. Sadly, there is no information about this addresse on internet. It is time force social engineering ! Let's send our best email to get in touch with the composer:

<center>![mail.png](/files/aperictf_2019/hey_dj/mail.png)</center><br>

I wait a few seconds and got an automatic response:

<center>![response.png](/files/aperictf_2019/hey_dj/response.png)</center><br>

And that's it, the composer just gave us her name: `Julietta`

#### Compute the Flag

```bash
echo -n "MOOREJULIETTA" | sha1sum
```
```text
a445a4b4e031c3b7c933a5b28ea825bccbe86da5
```

#### Flag

`APRK{a445a4b4e031c3b7c933a5b28ea825bccbe86da5}`

[Zeecka](https://twitter.com/Zeecka_)
