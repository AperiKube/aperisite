+++
title = "I need a new track now"
description = "Aperi'CTF 2019 - Steganography (175 pts)"
keywords = "Steg, Steganography, Stega, Audio, Sound, WAV, RIFF, File Format, Header, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Steganography (175 pts)"
toc = true
+++

Aperi'CTF 2019 - I need a new track now
============================================

### Challenge details

| Event                    | Challenge                 | Category      | Points | Solves      |
|--------------------------|---------------------------|---------------|--------|-------------|
| Aperi'CTF 2019           | I need a new track now    | Steganography | 175    |   1         |

Votre ami audiophile vous a laissé un message dans une de ses compositions.
Sauriez-vous le retrouver ?

Challenge: [I_Need_A_New_Track_Now.wav](/files/aperictf_2019/i_need_a_new_track/I_Need_A_New_Track_Now.wav) - md5sum : f2511ab63365e24b94ef0c575cdbfdca

### TL;DR

We read the WAV specification, saw the "DataSize" block in header attributes, increased the value, listen to the end of the wavfile and got the flag.

### Methodology

#### RTFM

I opened the file with audacity and starting few search. Here was the original file curve:

![original.png](/files/aperictf_2019/i_need_a_new_track/original.png)

After testing the most known techniques, we took a break and focus on the name of the challenge: "I need a new track now". This may mean that we should pass to an other track. Maybe is the track "after" the one we got ? Maybe should-we extend the track ?
I decided to have a look at the WAF (RIFF) file format:

![wiki.png](/files/aperictf_2019/i_need_a_new_track/wiki.png)

I saw the `DataSize` line which set the number of block composing the sound. Lets increase this value (note that the value is coded with little endian on 4 bytes). The DataSize is set right after the "data" constant.

I opened the file with [hexed.it](https://hexed.it) (you can use any hex editor) and increased the DataSize value from `F4 08 DA 00` to `F4 08 DA 01`:

![hexedit.png](/files/aperictf_2019/i_need_a_new_track/hexedit.png)

Press Ctrl+S and open the modified file with your favorite listening software. Here is the edited file: [I_Need_A_New_Track_Now_fixed.wav](/files/aperictf_2019/i_need_a_new_track/I_Need_A_New_Track_Now_fixed.wav) - md5sum : cfb718065465a44b794b369904dfeb19

![extended.png](/files/aperictf_2019/i_need_a_new_track/extended.png)

Here we can listen the end of the file, aka "the new track" and got the flag:

`Bravo, le flag est APRK{R351Z3_M3}, le tout en majuscule.`

#### Flag

`APRK{R351Z3_M3}`

[Zeecka](https://twitter.com/Zeecka_)
