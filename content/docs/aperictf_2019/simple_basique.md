+++
title = "Simple Basique"
description = "Aperi'CTF 2019 - Cryptography (50 pts)"
keywords = "Crypto, Cryptography, Hexadecimals, Base64, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Cryptography (50 pts)"
toc = true
+++

# Aperi'CTF 2019 - Simple Basique

### Challenge details

| Event                    | Challenge      | Category      | Points | Solves      |
|--------------------------|----------------|---------------|--------|-------------|
| Aperi'CTF 2019           | Simple Basique | Cryptography  | 50     | 60          |

Un apprenti cryptologue souhaite communiquer avec vous:

`%51%56%42%53%53%33%74%43%4e%48%4d%78%63%58%55%7a%58%31%4d%78%62%58%42%73%5a%53%46%39%43%67%3d%3d`

### Methodology

Looking at the charset, we got an hexadecimals ciphertext, lets decode it on a python shell:


```python
Python 2.7.13 (default, Sep 26 2018, 18:42:22)
[GCC 6.3.0 20170516] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import urllib
>>> urllib.unquote_plus("%51%56%42%53%53%33%74%43%4e%48%4d%78%63%58%55%7a%58%31%4d%78%62%58%42%73%5a%53%46%39%43%67%3d%3d")
'QVBSS3tCNHMxcXUzX1MxbXBsZSF9Cg=='
>>> import base64
>>> base64.b64decode(urllib.unquote_plus("%51%56%42%53%53%33%74%43%4e%48%4d%78%63%58%55%7a%58%31%4d%78%62%58%42%73%5a%53%46%39%43%67%3d%3d"))
'APRK{B4s1qu3_S1mple!}\n'
```

### Flag

`APRK{B4s1qu3_S1mple!}`

[Zeecka](https://twitter.com/Zeecka_)
