+++
title = "Andrologger"
description = "Aperi'CTF 2019 - Web (125 pts)"
keywords = "Mobile, APK, Freemarker, Spring, SSTI, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Web (125 pts)"
toc = true
+++

# Aperi'CTF 2019 - Andrologger

### Challenge details

| Event                    | Challenge   | Category | Points | Solves      |
|--------------------------|-------------|----------|--------|-------------|
| Aperi'CTF 2019           | Andrologger | Web      | 125    | 18          |

Vous avez récemment reçu le sample d'un keylogger qui sévit depuis un serveur de votre réseau interne, reprenez le contrôle de celui-ci en l'attaquant.

<u>**Fichiers&nbsp;:**</u> [app-release.apk](/files/aperictf_2019/andrologger/app-release.apk) - md5sum: ae2914751c7b52a94deff89d9a86dc84

### Methodologie

Il faut tout d'abord trouver l'url du serveur en reversant l'application.
Pour ça Jadx, Apktool ou même un String sur le dex de l'apk suffiront.

Ensuite on comprend en reversant le code que le malware envoit ses logs en body par JSON sur https://andrologger.aperictf.fr/HjRn9hbrrKbE44CS

Aprés un peu de recherche on voit que le serveur est un serveur Spring avec Freemarker et que l'on peut l'exploiter avec une Server Side Template Injection:

`${"freemarker.template.utility.Execute"?new()("cat /flag.txt")}`


### Flag

Flag : `APRK{kjlqsdlkqsdqskldjqlskdqlskdjqskldjqslkdqklsdqskldjqjqsdklqskldqj}`

Areizen
