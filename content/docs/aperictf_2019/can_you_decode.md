+++
title = "CAN you decode"
description = "Aperi'CTF 2019 - Hardware (100 pts)"
keywords = "Hardware, CAN, Car, Hacking, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Hardware (100 pts)"
toc = true
+++

Aperi'CTF 2019 - CAN you decode
============================================

### Challenge details

| Event                    | Challenge      | Category | Points | Solves      |
|--------------------------|----------------|----------|--------|-------------|
| Aperi'CTF 2019           | CAN you decode | Hardware | 100    |   5         |

During a car surveillance mission of a Magi'Kube member,
he would have acted strangely by making a series of headlamp calls with his Peugeot 407.
The agency could not identify to whom this message or its content was addressed.
One of your colleagues had infiltrated a malicious ECU on the car's CAN data bus.
You were able to retrieve the dump of the `0x046` frames of the light system from the bus.
Your task is therefore to retrieve the content of this message.

The flag is of the form `APRK XXX XXX XXX ...`.

You will need to change it to `APRK{XXX_XXX_XXX_...}`.

<u>**Files&nbsp;:**</u><br/>
- [BSI_BSM_opt_cmd.dump](/files/aperictf_2019/can_you_decode/SI_BSM_opt_cmd.dump) - md5sum: f69cd012281ab2d5a584b72676a64bc2<br/>
- [OPT_CONTROL.txt](/files/aperictf_2019/can_you_decode/OPT_CONTROL.txt) - md5sum: cde2fb924ac38300740ecdb494f6e93e

### Methodology

1. Find the location of the high beam headlights in the frame.

```raw
------------------------------------------------------------------------
OPT_CONTROL   |                     FR                     |    RE     |
------------------------------------------------------------------------
FR_LANT_S     | xx.. ....  .... ....  .... ....  .... .... | .... .... |
CODE_BEAM_S   | ..xx ....  .... ....  .... ....  .... .... | .... .... |
HIGH_BEAM_S   | .... x...  .... ....  .... ....  .... .... | .... .... |
FR_FOG_S      | .... .xx.  .... ....  .... ....  .... .... | .... .... |
Free1         | .... ...x  .... ....  .... ....  .... .... | .... .... |
RTC_PWM_DC    | .... ....  xxxx xxxx  .... ....  .... .... | .... .... |
LTC_PWM_DC    | .... ....  .... ....  xxxx xxxx  .... .... | .... .... |
R_BLINKING    | .... ....  .... ....  .... ....  x... .... | .... .... |
L_BLINKING    | .... ....  .... ....  .... ....  .x.. .... | .... .... |
BLACKOUT      | .... ....  .... ....  .... ....  ..x. .... | .... .... |
Free5         | .... ....  .... ....  .... ....  ...x xxxx | .... .... |
RE_LANT_S     | .... ....  .... ....  .... ....  .... .... | xx.. .... |
STOP_S        | .... ....  .... ....  .... ....  .... .... | ..x. .... |
REVERSE_S     | .... ....  .... ....  .... ....  .... .... | ...x .... |
RE_FOG_S      | .... ....  .... ....  .... ....  .... .... | .... x... |
TRAIL_FOG_S   | .... ....  .... ....  .... ....  .... .... | .... .x.. |
R_BLINK_S     | .... ....  .... ....  .... ....  .... .... | .... ..x. |
L_BLINK_S     | .... ....  .... ....  .... ....  .... .... | .... ...x |
------------------------------------------------------------------------
```

- High beam position : *HIGH_BEAM_S*
- Dipped-beam position : *CODE_BEAM_S*
  - (You can also solve with this one since it corresponds to the opposite of the high beam in this dump).
- Occultation between high and dipped-beam headlamps : *BLACKOUT*
  - (You can also solve with this one since it corresponds to the switch between the high beam and low beam headlights).

2. Dump all *HIGH_BEAM_S* bits.

3. Identify morse code

In seconds.

| short | long |
|-------|------|
| 0.5   | 2    |

Blank intervals :

| Between morse letters | Between letters | Between words |
|-----------------------|-----------------|---------------|
| 0.5                   | 2               | 4             |

4. Decode morse

```
.- .--. .-. -.-/-.-- ----- ..-/-.-. ....- -./-... .-.. .---- -. -.-/-... .-.. .---- -. -.-/- ----- -----
APRK Y0U C4N BL1NK BL1NK T00
```

### Flag

`APRK{Y0U_C4N_BL1NK_BL1NK_T00}`
