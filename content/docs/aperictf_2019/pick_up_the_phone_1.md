+++
title = "Pick up the phone 1"
description = "Aperi'CTF 2019 - Forensic (50 pts)"
keywords = "Forensic, SIP, Call, VOIP, Replay, Audio, Sound, WriteUp, CTF, Aperi'CTF, Apéri'CTF, Aperi'CTF, ApériCTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Forensic (50 pts)"
toc = true
+++

# Aperi'CTF 2019 - Pick up the phone 1

### Challenge details

| Event                    | Challenge            | Category  | Points | Solves      |
|--------------------------|----------------------|-----------|--------|-------------|
| Aperi'CTF 2019           | Pick up the phone 1  | Forensic  | 50     | 34          |

Le directeur commercial de notre entreprise a été victime d'une attaque au président. Investiguez et rammenez une preuve de cette attaque.

Challenge: [call.pcap](/files/aperictf_2019/pick_up_the_phone_1/call.pcap) - md5sum: ab8626c4e8fd088db4b08dc88663fa9a

### TL;DR

Replay SIP Stream with wireshark.


### Methodology

Since the file is a pcap file, we can open it with Wireshark

```bash
wireshark call.pcap &
```

![wireshark1.png](/img/aperictf_2019/pick_up_the_phone_1/wireshark1.png)

The main protocols on the pcap is "SIP" and RTP. SIP is a session protocol mostly used in VoIP communication. We can extract from the pcap the following session (Telephony > VoIP Call/SIP Flow > Flow):

![graph.png](/img/aperictf_2019/pick_up_the_phone_1/graph.png)
![graph2.png](/img/aperictf_2019/pick_up_the_phone_1/graph2.png)

We can certify that there is only one call of 18.105 seconds (906 packets).

Wireshark has an RTP Player embbed which let us replay the VoIP call. Some of wireshark does'nt have this features. For this I decided to use Wireshark-qt on my debian (Telephony > VoIP Call > Play stream):

![wave.png](/img/aperictf_2019/pick_up_the_phone_1/wave.png)

Once played we got:

```
Ok, c'était plutôt simple mais voici le flag en majucule: APRK{S1MPL3_SIP_C4LL}.
```

#### Flag

`APRK{S1MPL3_SIP_C4LL}`

[Zeecka](https://twitter.com/Zeecka_)
