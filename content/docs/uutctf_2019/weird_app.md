+++
title = "Weird Application"
description = "UUTCTF 2019 - Reverse (443 pts)"
keywords = "Reverse,Frida, Android , WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-04-25T10:00:00+02:00"
weight = 20
draft = false
bref = "UUTCTF 2019 - Reverse (443 pts)"
toc = true
+++

UUTCTF 2019: Weird Application
============================================

### Challenge details

| Event                    | Challenge                 | Category      | Points | Solves      |
|--------------------------|---------------------------|---------------|--------|-------------|
| UUTCTF 2019           | Werizchacha               | Reverse          | 443    | 9           |

Challenge: [WeirdApp.apk](/files/uutctf_2019/WeirdApp.apk) md5sum : 2e2a1e81963fc592a3d6254db3dc8d80

### TL;DR

Anti Emulation, Anti Root and obfuscation bypass with Frida

### Methodology

That challenge was an Android reverse challenge, my favourite kind of challenges :)

We were given an apk called weird APK, so first thing I did after downloading it was to open it in `Jadx-gui`

#### Strings deobfuscation

Once opened in `jadx-gui` I quickly saw that there was a class with a lot of anti-root/anti-emulator and that the strings were obfuscated.

<center>
<img src="https://i.imgur.com/4kDUFkg.png">
    <p>
        Anti-root / Anti-emulation
    </p>
</center>



<center>
<img src="https://i.imgur.com/0orv48B.png">
    <p>
        String obfuscation
    </p>
</center>


The anti-reverse were a little bit boring to bypass so I used my joker Frida.

I wanted to see the strings that were used, but I couldn't run the app as I want since it was killed by anti-reverse on my emulator. But I saw that the obfuscation of strings was using a kind of id so I made a for loop with Frida calling only this function.

```js
Java.perform(function(){
    const a = Java.use("a.a.a.a")
    for(i=0;i<100;i++){
        console.log(i,a.a(i))
    }
})
```

And it gave me this result : 

```bash
➜  WeirdApp frida -U -f com.sam.ctf -l bypass1.js --no-pause
     ____
    / _  |   Frida 12.4.7 - A world-class dynamic instrumentation toolkit
   | (_| |
    > _  |   Commands:
   /_/ |_|       help      -> Displays the help system
   . . . .       object?   -> Display information about 'object'
   . . . .       exit/quit -> Exit
   . . . .
   . . . .   More info at http://www.frida.re/docs/home/
Spawned `com.sam.ctf`. Resuming main thread!                            
[Android Emulator 5554::com.sam.ctf]-> 0 md5
1 AES
2 AES/ECB/PKCS7Padding
3 md5
4 UTF-16LE
5 AES
6 AES/ECB/PKCS7Padding
7 UTF-16LE
8 try more
9 uutctfandroidpassword
10 b0uy+Ww1Xd8QMgmEkahXs9O1IQzeK6FZ1FBe9KqX1Xg=

11 com.sam.ctf
12 android.permission.RECEIVE_SMS
13 android.permission.RECEIVE_SMS
14 /cache/e.txt
15 http://162.223.94.245/flag.txt
16 /cache/e.txt
17 
18 download::
19 /cache/e.txt
20 
21 uutctfandroidpassword
22 encript::
23 flag is: uutctf{
24 }
25 android.provider.Telephony.SMS_RECEIVED
26 pdus
27 858584678470
28 UUTCTFAndroidSMS
29 SMSBroadcastReceiver
30 onReceive: 
31 try more
32 try more
33 android.provider.Telephony.SMS_RECEIVED
34 SMSBroadcastReceiver
Error: java.lang.StringIndexOutOfBoundsException: length=72; index=72                                   
    at frida/node_modules/frida-java/lib/env.js:224
    at input:1
    at [anon] (/repl1.js:4)
    at frida/node_modules/frida-java/lib/vm.js:42
    at E (frida/node_modules/frida-java/index.js:348)
    at frida/node_modules/frida-java/index.js:334
    at input:1
Process terminated
```

### Getting the flag

Now that I had the strings I found 3 strings interesting : 
```
15 http://162.223.94.245/flag.txt
...
23 flag is: uutctf{
24 }
```

I opened the link in my browser and got a weird Base64 : 
```
xNO7gYSu6DhHEjQyJFlaM1Jc2/F3J7YT3Oq62P6Qfs7eoOWTM10wY8v5HEJWjOg6
```

So I searched were the strings with id 23 and 24 were used :

<center>
    <img src="https://i.imgur.com/AMRJp3o.png">
</center>

Ok now we now were the flag is built.

Since we are on a `onPostExecuteCallback` we can guess he is using the `flag.txt` content we got on the website.
After looking the important part of this function was here :

<center>
    <img src="https://i.imgur.com/8iEop5V.png">
</center>

So I called the same with Frida :) (str is the content that we got on the website).


```
Java.perform(function(){
    const a = Java.use("a.a.a.a")
    const decoder = Java.use("com.sam.ctf.a")
    const JavaString = Java.use('java.lang.String');
    const Base64 = Java.use("android.util.Base64");
    var exampleString1 = JavaString.$new('xNO7gYSu6DhHEjQyJFlaM1Jc2/F3J7YT3Oq62P6Qfs7eoOWTM10wY8v5HEJWjOg6');
    console.log(decoder.a(a.a(21),Base64.decode(exampleString1.getBytes(),0)));

})
```

And tada ! here is the flag :


```bash
➜  WeirdApp frida -U -f com.sam.ctf -l bypass.js --no-pause 
     ____
    / _  |   Frida 12.4.7 - A world-class dynamic instrumentation toolkit
   | (_| |
    > _  |   Commands:
   /_/ |_|       help      -> Displays the help system
   . . . .       object?   -> Display information about 'object'
   . . . .       exit/quit -> Exit
   . . . .
   . . . .   More info at http://www.frida.re/docs/home/
Spawned `com.sam.ctf`. Resuming main thread!                            
[Android Emulator 5554::com.sam.ctf]-> 0b4uscated_Andr0id
Process terminated
```


#### Flag

```
uutctf{0b4uscated_Andr0id}
```

[@Areizen](https://twitter.com/RomainKraft)
