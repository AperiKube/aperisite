+++
title = "Step by Step"
description = "Quals Sogeti Cyber E-scape 2019 - Steg (495 pts)"
keywords = "Steganography, Guessing, Crypto, LSB, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = "2019-02-20T10:50:28+01:00"
weight = 10
draft = false
bref = "Quals Sogeti Cyber E-scape 2019 - Steg (495 pts)"
toc = true
aliases = [
    "/docs/stepbystep",
]
+++

### Challenge details

| Event                           | Challenge    | Category      | Points | Solves      |
|---------------------------------|--------------|---------------|--------|-------------|
| Quals Sogeti Cyber E-scape 2019 | Step by Step | Steganography | 495    | 17          |

Download: [Step1.wav](/files/sogeti_qual_2019/stepbystep/Step1.wav) - md5: 217f7288cf4027740dc41761e3bad9fd

### Description (original)
>Des fichiers sensibles de la Shadow League ont été récupérés lors du hack d'un de leurs serveurs mais le contenu est protégé par un mot de passe. 
>D'après nos renseignements, ce mot de passe serait caché quelque part à l'intérieur d'un film de 1981 que les membres de la League avait téléchargé.
>Mon contact à Washington me dit que nos experts sont parvenus à isoler une séquence suspecte dans la musique du film. Cependant, malgré leurs efforts, ils n'ont rien pu en extraire.
>Ta mission, si tu l'acceptes, est de plonger au coeur de ce fichier audio pour en extraire le précieux sésame.

>Bonne chance !

>NB: Lors de l'analyse du film, nos troupes d'élite ont remarqué qu'une suite de chiffres apparaissaient à plusieurs reprises dans un coin l'image: 733051743.
>Ce sera peut être utile à un moment.

>Le format de flag **n'est pas** SCE{flag}

### Description [EN]
>Shadow League sensitive files have been recovered during the hack of one of their servers but the content is protected by a password.
>According to our information, this password is hidden somewhere inside a 1981 movie that members of the League had downloaded.
>My contact in Washington tells me that our experts have managed to isolate a suspicious sequence in the music of the film. However, despite their efforts, they could not extract anything.
>Your mission, if you accept it, is to dive into the heart of this audio file to extract the precious sesame.

>Good luck !

>NB: During the analysis of the film, our elite troops noticed that a series of numbers appeared repeatedly in a corner of the image: 733051743.
>It may be useful at one point.

>Flag format **is not** SCE{flag}.

### TL;DR
This was a Steganography / "dcode" cryptography challenge. Some of the parts were hard because we had to find the right tool.

### Ante Scriptum

The first time I solved the challenge, my thinking was: part of this challenge wasn't real Steganography and rested on finding the right tool more than understanding the hiding process (ie. Step 1). Moreover, the end of the challenge was only "dcode crypto" challenge with part of guessing. Hints were hidden during the challenge and were supposed to help us. Finally, I'll not describe here the whole searching part. My friends and I have been searching/enumerating a lot to get each step. Now, by reading at my own W.U., I think this challenge was cool thanks to the number of steps but maybe not really tested ? 

### Solving

#### Step 1

We had a wav file named [Step1.wav](/files/sogeti_qual_2019/stepbystep/Step1.wav).<br/>
During our steganography analysis, I used `exiftool` to look if there were any audio comment, and there it is:
```bash
$exiftool Step1.wav

ExifTool Version Number         : 11.08
File Name                       : Step1.wav
Directory                       : .
File Size                       : 16 MB
File Modification Date/Time     : 2019:02:18 14:19:54+01:00
File Access Date/Time           : 2019:02:25 11:38:47+01:00
File Inode Change Date/Time     : 2019:02:25 11:36:16+01:00
File Permissions                : rw-r--r--
File Type                       : WAV
File Type Extension             : wav
MIME Type                       : audio/x-wav
Encoding                        : Microsoft PCM
Num Channels                    : 2
Sample Rate                     : 44100
Avg Bytes Per Sec               : 176400
Bits Per Sample                 : 16
Comment                         : STEP1: Buckethead, Fevrier 2002, 14
Duration                        : 0:01:35
```

We got STEP1 Hint:
- STEP1: Buckethead, Fevrier 2002, 14

This hind leads to the Buckethead album [Funnel Weaver](https://fr.wikipedia.org/wiki/Funnel_Weaver) released in february. The 14th music of this album is named `Channel of Secrets`. After trying multiple audio steganography techniques, I've been looking for more specific wav steganography tool on Google. After many repo on Github, we've been using this one: ["GotchaPython - StegWav"](https://github.com/GotchaPython/stegwav/). Fun fact, in the source code of [StegWav](https://github.com/GotchaPython/stegwav/), we can notice than even the developper don't know what he's doing: 

```C
// Merge files / do some magic
for(int i=44,o=i-44;i<s1;i+=2,o=i-44)
    f1[i]&=254,f1[i]|=o/16<s2?(f2[o/16]>>(7-o%16/2))&1:0;
```

With the tool, we finaly got a file with data in it (see Step 2).<br/>
```bash
gcc extract.c -o extract
./extract Step1.wav Step1
```

#### Step 2

We continued on Step 2. File was recognized as data but binwalk identified a BMP image in the content. I decided to extract it with foremost.

```bash
foremost Step1
cd output
cd bmp
mv 00000000.bmp Step2.bmp
```

<center>![Step2.bmp](/img/sogeti_qual_2019/stepbystep/Step2.bmp)</center>

`strings` command on Step2.bmp gave us an other hint (yes, again for STEP1 ... ? Maybe they were thinking about Step2 ?) :<br>
- STEP1: Split Enz, Novembre 1978. Bonus track: Gentle Giant, 1976, 7<br/>

First part stands for the music `I see Red` by Split Enz in 1978.<br/>
Second part refers to `I Lost My Head` track by Gentle Giant, in their "Interview" album (1976) (7 for the 7th tack of the album).

#### Step 3

I decided to put the image on my tool ["Aperi'Solve"](https://www.aperisolve.fr/) that performs bit analysis.
I found data hidden in the red LSB:

<center>![redlsb.bmp](/img/sogeti_qual_2019/stepbystep/redlsb.bmp)</center>

Zsteg with "all" option available on Aperi'Solve show us multiple data. I decided to run it on my computer:

```bash
zsteg Step2.bmp --all
... [SNIP] ...
b1,r,lsb,xy         .. text: "STEP3: King Crimson, 1969, 2\r\nklesh than-zie dzeh ne-zhoni ca-yeilth shi-da wol-la-chee a-woh ah-losz dzeh\r\ndzeh besh-do-tliz be-tkah ah-jad a-chi ah-jah ah-losz a-chin tlo-chin tse-nill be-tkah n"
... [SNIP] ...
... [SNIP] ...
```

I extracted the whole string with:

```bash
zsteg Step2.bmp -E 'b1,r,lsb,xy' > Step3
strings Step3 | head -n 3 > Step3.txt
cat Step3.txt
```

```text
STEP3: King Crimson, 1969, 2
klesh than-zie dzeh ne-zhoni ca-yeilth shi-da wol-la-chee a-woh ah-losz dzeh
dzeh besh-do-tliz be-tkah ah-jad a-chi ah-jah ah-losz a-chin tlo-chin tse-nill be-tkah ne-ahs-jah be-tkah tsah tkin no-da-ih dzeh nash-doie-tso yeh-hes naki-alh-deh-da-al ah-jah a-chi ah-ya-tsinne moasi ne-zhoni toish-jeh tsah ne-zhoni shi-da be-tkah ne-ahs-jah ne-ahs-jah a-chi yeh-hes tkin yeh-hes moasi no-da-ih tkin tsa-na-dahl tse-nill lin a-woh astsanh ah-jah dibeh tse-nill tsin-tliti be-tkah cla-gi-aih ah-nah wol-la-chee a-kha klizzie tsa-na-dahl gah than-zie be-tkah tla-gin besh-do-tliz tsah tse-nill a-chi ah-losz tsin-tliti than-zie cla-gi-aih tlo-chin us-dzoh ca-yeilth ah-ya-tsinne a-keh-di-glini al-na-as-dzoh be-tkah a-woh be-tkah ah-nah yeh-hes be-tkah tsah tlo-chin tse-nill a-chi a-chin tsa-na-dahl moasi be-tas-tni yeh-hes be-tkah yeh-hes besh-do-tliz wol-la-chee be-tkah be-tkah be-tkah shi-da ba-ah-ne-di-tinin ah-tad a-chin klesh naki-alh-deh-da-al gah a-woh ah-nah be-la-sana besh-do-tliz be-tkah tsah jeha tlo-chin dah-nes-tsa no-da-ih than-zie tsin-tliti lha-cha-eh be-tkah be-tkah be-la-sana tse-nill da-ahl-zin be wol-la-chee yeh-hes klizzie#####FIN DE TRANSMISSION#####
```

We got the 3rd Hint:
- STEP3: King Crimson, 1969, 2

Some google search gave us the title `I Talk to the Wind` which refers to the Navajo people.

Now we can remove the hint from the first line of Step3.txt.

#### Step 4

Looking at the hint and the cipher text in Step3.txt, we can identify the alorithm: the Navajo Code.

To decode it, I decided to pass the text in full uppercase without any linebreak:

```text
KLESH THAN-ZIE DZEH NE-ZHONI CA-YEILTH SHI-DA WOL-LA-CHEE A-WOH AH-LOSZ DZEH DZEH BESH-DO-TLIZ BE-TKAH AH-JAD A-CHI AH-JAH AH-LOSZ A-CHIN TLO-CHIN TSE-NILL BE-TKAH NE-AHS-JAH BE-TKAH TSAH TKIN NO-DA-IH DZEH NASH-DOIE-TSO YEH-HES NAKI-ALH-DEH-DA-AL AH-JAH A-CHI AH-YA-TSINNE MOASI NE-ZHONI TOISH-JEH TSAH NE-ZHONI SHI-DA BE-TKAH NE-AHS-JAH NE-AHS-JAH A-CHI YEH-HES TKIN YEH-HES MOASI NO-DA-IH TKIN TSA-NA-DAHL TSE-NILL LIN A-WOH ASTSANH AH-JAH DIBEH TSE-NILL TSIN-TLITI BE-TKAH CLA-GI-AIH AH-NAH WOL-LA-CHEE A-KHA KLIZZIE TSA-NA-DAHL GAH THAN-ZIE BE-TKAH TLA-GIN BESH-DO-TLIZ TSAH TSE-NILL A-CHI AH-LOSZ TSIN-TLITI THAN-ZIE CLA-GI-AIH TLO-CHIN US-DZOH CA-YEILTH AH-YA-TSINNE A-KEH-DI-GLINI AL-NA-AS-DZOH BE-TKAH A-WOH BE-TKAH AH-NAH YEH-HES BE-TKAH TSAH TLO-CHIN TSE-NILL A-CHI A-CHIN TSA-NA-DAHL MOASI BE-TAS-TNI YEH-HES BE-TKAH YEH-HES BESH-DO-TLIZ WOL-LA-CHEE BE-TKAH BE-TKAH BE-TKAH SHI-DA BA-AH-NE-DI-TININ AH-TAD A-CHIN KLESH NAKI-ALH-DEH-DA-AL GAH A-WOH AH-NAH BE-LA-SANA BESH-DO-TLIZ BE-TKAH TSAH JEHA TLO-CHIN DAH-NES-TSA NO-DA-IH THAN-ZIE TSIN-TLITI LHA-CHA-EH BE-TKAH BE-TKAH BE-LA-SANA TSE-NILL DA-AHL-ZIN
```

Navajo code work as a monoalphabetic substitution. I wrote a quick python script to decode the ciphertext. First, I took the alphabet from [this website](https://www.apprendre-en-ligne.net/crypto/codes/navajo.html) but after, I managed to use par of [this one](https://ww2db.com/other.php?other_id=29) which invert some "E" and "?" in our clear text. More over I got issue with parentheses order. Here is my final decode script for step 4:

```python
import string
    
dico = {
    "WOL-LA-CHEE":"A",
    "BE-LA-SANA":"A",
    "TSE-NILL":"A",
    "NA-HASH-CHID":"B",
    "SHUSH":"B",
    "TOISH-JEH":"B",
    "MOASI":"C",
    "TLA-GIN":"C",
    "BA-GOSHI":"C",
    "BE":"D",
    "CHINDI":"D",
    "LHA-CHA-EH":"D",
    "AH-JAH":"E",
    "DZEH":"E",
    "AH-NAH":"E",
    "CHUO":"F",
    "TSA-E-DONIN-EE":"F",
    "MA-E":"F",
    "AH-TAD":"G",
    "KLIZZIE":"G",
    "JEHA":"G",
    "TSE-GAH":"H",
    "CHA":"H", 
    "LIN":"H",
    "TKIN":"I",
    "YEH-HES":"I",
    "A-CHI":"I",
    "TKELE-CHO-G":"J", 
    "AH-YA-TSINNE":"J",
    "YIL-DOI":"J",
    "JAD-HO-LONI":"K",
    "BA-AH-NE-DI-TININ":"K",
    "KLIZZIE-YAZZIE":"K",
    "DIBEH-YAZZIE":"L", 
    "AH-JAD":"L",
    "NASH-DOIE-TSO":"L",
    "TSIN-TLITI":"M",
    "BE-TAS-TNI":"M", 
    "NA-AS-TSO-SI":"M",
    "TSAH":"N",
    "A-CHIN":"N",
    "A-KHA":"O",
    "TLO-CHIN":"O",
    "NE-AHS-JAH":"O",
    "CLA-GI-AIH":"P",
    "BI-SO-DIH":"P",
    "NE-ZHONI":"P",
    "CA-YEILTH":"Q",
    "GAH":"R",
    "DAH-NES-TSA":"R", 
    "AH-LOSZ":"R",
    "DIBEH":"S",
    "KLESH":"S",
    "D-AH":"T",
    "A-WOH":"T",
    "THAN-ZIE":"T",
    "SHI-DA":"U",
    "NO-DA-IH":"U",
    "A-KEH-DI-GLINI":"V",
    "GLOE-IH":"W",
    "AL-NA-AS-DZOH":"X",
    "TSAH-AS-ZIH":"Y",
    "BESH-DO-TLIZ":"Z",
    "NAKI-ALH-DEH-DA-AL":":",
    "TSA-NA-DAHL":",",
    "US-DZOH":"-",
    "ASTSANH":"(",
    "DA-AHL-ZIN":".",
    "DA-AHL-ZHIN":";",
    "BI-TSA":";",
    "NA-DAHL":";",
    "BE-TKAH":" "
}

cipher = """klesh than-zie dzeh ne-zhoni ca-yeilth shi-da wol-la-chee a-woh ah-losz dzeh
dzeh besh-do-tliz be-tkah ah-jad a-chi ah-jah ah-losz a-chin tlo-chin tse-nill be-tkah ne-ahs-jah be-tkah tsah tkin no-da-ih dzeh nash-doie-tso yeh-hes naki-alh-deh-da-al ah-jah a-chi ah-ya-tsinne moasi ne-zhoni 5 toish-jeh tsah ne-zhoni shi-da be-tkah ne-ahs-jah ne-ahs-jah a-chi yeh-hes tkin yeh-hes moasi no-da-ih tkin tsa-na-dahl tse-nill lin a-woh astsanh ah-jah dibeh tse-nill tsin-tliti be-tkah cla-gi-aih ah-nah wol-la-chee a-kha klizzie tsa-na-dahl gah than-zie be-tkah tla-gin besh-do-tliz tsah tse-nill a-chi ah-losz tsin-tliti than-zie cla-gi-aih tlo-chin us-dzoh ca-yeilth ah-ya-tsinne a-keh-di-glini al-na-as-dzoh be-tkah a-woh be-tkah ah-nah yeh-hes be-tkah tsah tlo-chin tse-nill a-chi a-chin tsa-na-dahl moasi be-tas-tni yeh-hes be-tkah yeh-hes besh-do-tliz wol-la-chee be-tkah be-tkah be-tkah shi-da ba-ah-ne-di-tinin ah-tad a-chin klesh naki-alh-deh-da-al gah a-woh ah-nah be-la-sana besh-do-tliz be-tkah tsah jeha tlo-chin dah-nes-tsa no-da-ih than-zie tsin-tliti lha-cha-eh be-tkah be-tkah be-la-sana tse-nill da-ahl-zin be wol-la-chee yeh-hes klizzie"""
cipher = cipher.upper().split()

out = ""
for c in cipher:
    if c in dico:
        out += dico[c]
    else:
        out += c
print(out)
```

We got the following clear text:

```text
STEPQUATRE
EZ LIERNOA O NIUELI:EIJCP5BNPU OOIIIICUI,AHT(ESAM PEAOG,RT CZNAIRMTPO-QJVX T EI NOAIN,CMI IZA   UKGNS:RTEAZ NGORUTMD  AA.DAIG
```

We removed "STEPQUATRE" keywords and save it as "Step4.txt".

#### Step 5

The new cipher text let us think that the charset was already correct (we could compute the [index of coincidence](https://en.wikipedia.org/wiki/Index_of_coincidence) but it's not necessary). At this point, my team decided to test lots of [dcode.fr](https://www.dcode.fr/) tools and finally found the [Columnar transposition cipher](https://www.dcode.fr/columnar-transposition-cipher). Here is the settings we used:

<center>![dcode.jpg](/img/sogeti_qual_2019/stepbystep/dcode.jpg)</center>

Output:
```
STEP5: ZEBRE ANTILOPE IGUANE, ZORRO ANTONIO IGNACIO, ZIRCONIUM ACTINIUM IRIDIUM, ZETA ALPHA IOTA :-(. EQEDUIJSAKJVAIGCXMGNP 
```

We got Step 5 hint:
- STEP5: ZEBRE ANTILOPE IGUANE, ZORRO ANTONIO IGNACIO, ZIRCONIUM ACTINIUM IRIDIUM, ZETA ALPHA IOTA :-(

And saved `EQEDUIJSAKJVAIGCXMGNP` in "Step5.txt" file.

The Step 5 hint can be decode as `ZAI ZAI ZAI ZAI :-(` which refers to `Joe Dassin - Siffler sur la colline`. This title reffers to the "Hill cipher".

#### Step 6

For this last step, we used the hill cipher with our cipher text `EQEDUIJSAKJVAIGCXMGNP` and the key given at the beginning of the challenge `733051743`. We used the [dcode tool](https://www.dcode.fr/chiffre-hill) for this task. Note that the key has been set in a 3x3 matrix, verticaly:


<center>![dcode2.jpg](/img/sogeti_qual_2019/stepbystep/dcode2.jpg)</center>

### Flag

`UGOTDAFLAGNOWGETALIFE`

[Zeecka](https://twitter.com/Zeecka_) & Teammates <3
