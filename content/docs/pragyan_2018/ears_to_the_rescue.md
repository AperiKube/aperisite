+++
title = "Ears to the rescue"
description = "Pragyan CTF 2018 - Forensic (150 pts)"
keywords = "Forensic, Audio, Quantizatio, audacity, WriteUp, CTF, Aperi'Kube, Apéri'Kube, AperiKube, ApériKube"
date = 2018-03-02T18:19:25+01:00
weight = 20
draft = false
bref = "Pragyan CTF 2018 - Forensic (150 pts)"
toc = true
aliases = [
    "/docs/pragyan_2018_ears_to_the_rescue",
]
+++

Pragyan CTF 2018: Ears to the rescue
====================================

### Challenge details

| Event            | Challenge          | Category | Points | Solves |
|------------------|--------------------|----------|--------|--------|
| Pragyan CTF 2018 | Ears to the rescue | Forensic | 150    | 52     |

Download: [audible.wav](/files/pragyan_2018/ears_to_the_rescue/audible.wav)

### Description

> Jack is trapped inside a room. All he can see and hear is a sound, his ears hear only the specific sound.
>
> He needs to escape, help him figure out what the sound means.

### Methology

Reading the [description of the challenge](#description), we are advised that by listening to the soundtrack,
we would be likely to help Jack to escape from a room.

As this is an audio stream, let's use our must-have audio processing tool: [Audacity](http://www.audacityteam.org/).

<center>![audacity](/img/pragyan_2018/ears_to_the_rescue/audacity.png)</center>

From this interesting output, we can quickly identify eight levels of quantization, as follows:

<center>![3-bit quantization](/img/pragyan_2018/ears_to_the_rescue/3_bit_quantization.png)</center>

Let's try to implement this quantization process using Python: [decode.py](/files/pragyan_2018/ears_to_the_rescue/decode.py)

```bash
# python decode.py
The flag is :- pctf{SuRf1n6_th3~4udI0_w4v3s.f0r^lif3}
```

Here we got the flag!

Final flag:

> pctf{SuRf1n6_th3~4udI0_w4v3s.f0r^lif3}


Creased
