+++
draft= false
title = "About us"
description = "Team definition"
+++

<style>
ul li img {
  float: left;
  display: block;
  margin-right: 20px;
  margin-bottom: 30px;
}

ul li {
  clear: both;
}

ul li .member {
  font-size: 2em;
  text-transform: uppercase;
  font-weight: 500;
  color: #534b3a;
}

.rootme {
  background:black;
}
</style>

## Team members

<ul style="list-style-type:none">
  <li>
    <span class="member">\_ACKNAK\_</span>
    <img src="/img/avatar/acknak.png" width="150" height="150" /><br />
    Romain KOSZYK<br />
    Pentester<br />
    <a href="https://twitter.com/_ACKNAK_" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Twitter _ACKNAK_" /></a>
    <a href="https://www.root-me.org/RomainK" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Root-me _ACKNAK_" /></a>
    <a href="https://acknak.fr/" target="\_blank"><img width="50" height="50" src="/img/avatar/acknak.png" alt="\_ACKNAK\_ Logo" title="Infosec Blog" /></a>
  </li>
  <li>
    <span class="member">Areizen</span>
    <img src="/img/avatar/Areizen.jpg" width="150" height="150" /><br />
    Romain KRAFT<br />
    <a href="https://twitter.com/Areizen_" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Areizen Twitter" /></a>
    <a href="https://www.root-me.org/Areizen-36111" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Areizen Root-me" /></a>
    <a href="https://gitlab.com/Areizen" target="\_blank"><img width="50" height="50" src="/img/avatar/gitlab.png" alt="git Logo" title="Areizen GitHub" /></a>
    <a href="https://www.areizen.fr" target="\_blank"><img width="50" height="50" src="/img/avatar/Areizen.jpg" alt="Areizen Logo" title="Areizen Blog" /></a>
    <a href="https://www.linkedin.com/in/romain-kraft-795210158/" target="\_blank"><img width="50" height="50" src="/img/avatar/linkedin-logo.png" alt="git Logo" title="Areizen LinkedIn" /></a>
  </li>
  <li>
    <span class="member">Creased</span>
    <img src="/img/avatar/creased.jpg" width="150" height="150" /><br />
    Baptiste MOINE<br />
    Reverse engineering, electronics and hacking enthusiast.<br />
    <a href="https://twitter.com/Creased_" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Creased Twitter" /></a>
    <a href="https://www.root-me.org/Creased" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Creased Root-me" /></a>
    <a href="https://www.bmoine.fr/" target="\_blank"><img width="50" height="50" src="https://www.bmoine.fr/assets/images/favicons/ios-120.png" alt="Root-me Logo" title="Creased Blog" /></a>
    <a href="https://git.bmoine.fr/" target="\_blank"><img width="50" height="50" src="/img/avatar/gitlab.png" alt="git Logo" title="Creased GitLab" /></a>
    <a href="https://www.linkedin.com/in/bmoine/" target="\_blank"><img width="50" height="50" src="/img/avatar/linkedin-logo.png" alt="git Logo" title="Creased LinkedIn" /></a>
  </li>
  <li>
    <span class="member">DrStache</span>
    <img src="/img/avatar/drstache.jpg" width="150" height="150" /><br />
    Florian CHARBONNEAU<br />
    Web • OSINT<br />
    <a href="https://twitter.com/DrStache_" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Twitter DrStache" /></a>
    <a href="https://www.root-me.org/DrStache" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Root-me DrStache" /></a>
  </li>
  <li>
    <span class="member">ENOENT</span>
    <img src="/img/avatar/enoent.png" width="150" height="150" /><br />
    Florian PICCA<br />
    French IT security student, CTF player and low-level enthusiast. Math is beautiful.<br />
    Reverse • Crypto • Pwn<br />
    <a href="https://twitter.com/ENOENT_" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Twitter ENOENT" /></a>
    <a href="https://www.root-me.org/Florian-49364" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Root-me ENOENT" /></a>
    <a href="https://bitsdeep.com" target="\_blank"><img width="82" height="50" src="/img/avatar/logo_blog_enoent.png" alt="Blog Logo" title="Blog ENOENT" /></a>
    <a href="https://github.com/florianpicca" target="\_blank"><img width="50" height="50" src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="git Logo" title="ENOENT GitHub" /></a>
  </li>
  <li>
    <span class="member">Haax</span>
    <img src="/img/avatar/haax.png" width="150" height="150" /><br />
    Wannabe Pentester & Infosec Engineer (Graduated ENSIBS Vannes). Interested in offensive security and intelligence gathering (OSINT). Always learning!<br />
    <a href="https://twitter.com/Haax9_" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Haax Twitter" /></a>
    <a href="https://www.root-me.org/Haax?lang=fr" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Haax Root-me" /></a>
    <a href="https://haax.fr/en/" target="\_blank"><img width="50" height="50" src="/img/avatar/haax_logo.png" alt="Blog Logo" title="Haax Blog" /></a>
    <a href="https://github.com/Haax9" target="\_blank"><img width="50" height="50" src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="git Logo" title="Haax GitHub" /></a>
  </li>
  <li>
    <span class="member">Iptior</span>
    <img src="/img/avatar/iptior.png" width="150" height="150" /><br />
    Erwan<br />
    Graduated ENSIBS. CTF Player. Like challenge and hacking is a big one.<br />
    <a href="https://twitter.com/ErwanFily" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Iptior Twitter" /></a>
    <a href="https://www.root-me.org/Erwan-40806" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Iptior Root-me" /></a>
    <a href="https://github.com/Iptior/" target="\_blank"><img width="50" height="50" src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="git Logo" title="Iptior GitHub" /></a>
  </li>
  <li>
    <span class="member">Laluka</span>
    <img src="/img/avatar/laluka-blog.jpg" width="150" height="150" /><br />
    Louka JACQUES-CHEVALLIER<br />
    Engineer, Hacker, Eternel Learner... =^~^=<br/>
    ~ Sharing is Caring ~<br/>
    <a href="https://twitter.com/TheLaluka" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Laluka Twitter" /></a>
    <a href="https://www.root-me.org/DeveLooper" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Laluka Root-me" /></a>
    <a href="https://thinkloveshare.com" target="\_blank"><img width="50" height="50" src="/img/avatar/laluka.png" alt="Blog Logo" title="Laluka Blog" /></a>
    <a href="https://github.com/TheLaluka/" target="\_blank"><img width="50" height="50" src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="git Logo" title="Laluka GitHub" /></a>
    <a href="https://www.linkedin.com/in/louka-jacques-chevallier-6b2460125" target="\_blank"><img width="50" height="50" src="/img/avatar/linkedin-logo.png" alt="git Logo" title="Laluka LinkedIn" /></a>
  </li>
  <li>
    <span class="member">Maki</span>
    <img src="/img/avatar/maki.png" width="150" height="150" /><br />
    Security and electronic enthusiast, CTF player w/ Aperi'Kube, pentester, forensic analyst and goodies hunter!<br />
    <a href="https://twitter.com/maki_mitz" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Maki Twitter" /></a>
    <a href="https://www.root-me.org/Maki-37744" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Maki Root-me" /></a>
    <a href="https://www.maki.bzh/" target="\_blank"><img width="50" height="50" src="https://www.shareicon.net/data/128x128/2016/10/18/845151_food_512x512.png" alt="Blog Logo" title="Maki Blog" /></a>
  </li>
  <li>
    <span class="member">TomTomBinary</span>
    <img src="/img/avatar/tom.png" width="150" height="150" /><br />
    Thomas DUBIER<br />
    ENSIBS student, passionate about low-level security, CTF player w/ Aperi'Kube. Reverser / Pwner.<br />
    <a href="https://twitter.com/tomtombinary" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="TomTomBinary Twitter" /></a>
    <a href="https://www.root-me.org/Tomtombinary?lang=fr" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="TomTomBinary Root-me" /></a>
    <a href="http://tomtombinary.xyz/" target="\_blank"><img width="50" height="50" src="/img/avatar/logo_tom.png" alt="Root-me Logo" title="TomTomBinary Blog" /></a>
    <a href="https://github.com/Tomtombinary" target="\_blank"><img width="50" height="50" src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" alt="git Logo" title="TomTomBinary GitHub" /></a>
  </li>
  <li>
    <span class="member">Zeecka</span>
    <img src="/img/avatar/zeecka.png" width="150" height="150" /><br />
    Alex GARRIDO<br />
    Pentester and CTF Player. Interest in Steganography.<br />
    Steg • Prog • Web<br />
    <a href="https://twitter.com/Zeecka_" target="\_blank"><img width="50" height="50" src="https://www.cycleto.ca/sites/default/files/twitter-icon-circle-logo.png" alt="Twitter Logo" title="Zeecka Twitter" /></a>
    <a href="https://root-me.org/Zeecka" target="\_blank"><img width="50" height="50" src="https://www.root-me.org/IMG/siteon0.svg" alt="Root-me Logo" title="Zeecka Root-me" /></a>
    <a href="https://www.zeecka.fr/" target="\_blank"><img width="50" height="50" src="/img/avatar/zeecka.png" alt="Zeecka.fr logo" title="Zeecka.fr" /></a>
    <a href="https://www.linkedin.com/in/alex-garrido-5a6454110/" target="\_blank"><img width="50" height="50" src="/img/avatar/linkedin-logo.png" alt="git Logo" title="Zeecka LinkedIn" /></a>
  </li>
</ul>
<br /><br />
