#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import socket # Because pwntools doesn't work with python3 :(
from assembler import Assembler

def recvuntil(msg, drop=False):
    buffer = b""
    while msg not in buffer:
        buffer += conn.recv(1)
    if drop:
        buffer = buffer[:-len(msg)]
    return buffer

def solve():
    recvuntil(b"---- START ----\n")
    code = recvuntil(b"---- END ----\n", drop=True).decode("utf-8")

    asm = Assembler()
    byteCode = asm.compile(code)
    length = len(byteCode)
    recvuntil(b"What length is your byte code ?\n")
    conn.send('{}\n'.format(length).encode('utf-8'))
    recvuntil(b'Give me your byte code :\n')
    conn.send(byteCode)
    print(conn.recv(1024).decode('utf-8').strip())


conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect(("x32.aperictf.fr", 32324))
for i in range(1000):
    solve()
print(conn.recv(1024).decode('utf-8').strip())
conn.close()
