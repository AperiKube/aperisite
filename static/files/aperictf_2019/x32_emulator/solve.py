#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import socket # Because pwntools doesn't work with python3 :(
from emulator import Emulator
import binascii

def recvuntil(msg, drop=False):
    buffer = b""
    while msg not in buffer:
        buffer += conn.recv(1)
    if drop:
        buffer = buffer[:-len(msg)]
    return buffer

def solve():
    recvuntil(b"Byte code in hex : ")
    code = recvuntil(b"\n", drop=True).decode("utf-8")
    code = binascii.unhexlify(code)

    emu = Emulator(code)
    emu.run()
    expected = emu.STDOUT
    recvuntil(b"What is the outut of this programm ?\n")
    conn.send(expected)
    print(conn.recv(1024).decode('utf-8').strip())


conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn.connect(("x32.aperictf.fr", 32323))
for i in range(300):
    solve()
print(conn.recv(1024).decode('utf-8').strip())
conn.close()
