#!/usr/bin/env python2
# -*- coding:utf-8 -*-

from pwn import *
import binascii

def ntos(x):
    n = hex(x)[2:].rstrip("L")
    if len(n)%2 != 0:
        n = "0"+n
    return binascii.unhexlify(n)

def ston(x):
    n = binascii.hexlify(x)
    return int(n,16)

def btos(b):
    n = int(b,2)
    return ntos(n)

def stob(s):
    return bin(ston(s))[2:].rstrip("L")

def get_blocks(data, size_block=16):
    return [data[i:i+size_block] for i in range(0, len(data), size_block)]


def send(m):
    conn.recvuntil("Give me your plaintext:\n")
    conn.sendline(m.encode('hex'))
    conn.recvline()
    result = conn.recvline().strip()
    return result.decode("hex")

def getSwap(i):
    m = '0'*i+'1'
    m = m.ljust(16*8, "0")
    r = stob(send(btos(m).rjust(16*8, '\x00'))).zfill(16*8)
    return r.find("1")

conn = remote("black-box.aperictf.fr", 9897)
conn.recvline()
cipher = conn.recvline().strip()
log.info(cipher)
cipher = cipher.decode("hex")

swap = []

for i in range(16*8):
    swap.append(getSwap(i))

result = ''
for b in get_blocks(cipher):
    bi = stob(b).zfill(16*8)
    decrypted = [0]*(16*8)
    for i in range(16*8):
        decrypted[i] = bi[swap[i]]
    result += ''.join(decrypted)

log.success(btos(result))
conn.close()
