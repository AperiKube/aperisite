#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#define BANNER " _____                      ___  ___           _     _\n/  __ \\                     |  \\/  |          | |   (_)\n| /  \\/_ __ __ _ _____   _  | .  . | __ _  ___| |__  _ _ __   ___\n| |   | '__/ _` |_  | | | | | |\\/| |/ _` |/ __| '_ \\| | '_ \\ / _ \\\n| \\__/| | | (_| |/ /| |_| | | |  | | (_| | (__| | | | | | | |  __/\n \\____|_|  \\__,_/___|\\__, | \\_|  |_/\\__,_|\\___|_| |_|_|_| |_|\\___|\n                      __/ |\n                     |___/     ---  Reynholm Industries  ---\n\n"
#define FUN_CMD "echo %s | fold -w2 | rev | tac | perl -p -e 's/[\n]//g'"
#define BUF_SIZE 255

char *base64(const char *message) {
    BIO *bio, *b64;
    BUF_MEM *bptr;
    char *buff;

    b64 = BIO_new(BIO_f_base64());  // create a base64 filter BIO.
    bio = BIO_new(BIO_s_secmem());  // create a sink BIO and use the secure heap for buffer storage.

    BIO_push(b64, bio);  // Add the sink BIO to the base64 filter BIO chain.
    // BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL); // Write everything in one line
    BIO_write(b64, message, strlen(message));  // Write the message to the base64 filter BIO.
    BIO_flush(b64);  // EOF.

    BIO_get_mem_ptr(b64, &bptr);  // Copy the base64 filter BIO pointer.

    // Copy the base64 encoded data into the buffer.
    buff = (char *)malloc(bptr->length);
    memcpy(buff, bptr->data, bptr->length-1);
    buff[bptr->length-1] = 0;

    // Free the entire BIO chain (including bio).
    BIO_free_all(b64);

    return buff;
}

void strip_char(char *str, const char chr) {
    int length = strlen(str) - 1;

    // Check if we've stripped every occurences of the chr at the end of the string.
    while (str[length] == chr) {
        str[length] = 0x0;
        length--;
    };
}

char *replace_char(char *str, const char find, const char replace){
    char *current_pos = strchr(str, find);

    while (current_pos){
        *current_pos = replace;
        current_pos = strchr(current_pos, find);
    }

    return str;
}

// Get string from standard input.
void fget_str(const char message[], char *buf, const int max_buf_size) {
    char junk_char;

    printf("%s", message);

    /* Fill the buffer with user input and then convert the buffer to char. */
    if (fgets(buf, max_buf_size, stdin) == NULL) {
        printf("Enter a string!");
    }

    /* Eventually flush the input buffer in order to prevent overflow issues. */
    if (buf[strlen(buf) - 1] != 0xa) {  /* Use strlen as index to handle non-full buffer. */
        while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
            /* printf("Flushing! Removing 0x%x from input.\n", junk_char); */
        }
    } else {
        buf[strlen(buf) - 1] = 0x0;
    }
}

// Get int from standard input.
int fget_int(const char message[], char *buf, const int max_buf_size) {
    int integer, finished = 0;
    char junk_char;

    do {
        printf("%s", message);

        // Fill the buffer with user input and then convert the buffer to integer.
        if (fgets(buf, max_buf_size, stdin) != NULL) {
            if (sscanf(buf, "%d", &integer) != 1) {  // Buffer can't be converted to integer.
                fprintf(stderr, "Please enter a valid integer!\n");
            } else {
                finished = 1;
            }
        }

        if (buf[strlen(buf) - 1] != 0xa) {  // Use strlen as index to handle non-full buffer.
            while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
                // printf("Flushing! Removing 0x%x from input.\n", junk_char);
            }
        }
    } while (!finished);

    return integer;
}

// Encode a string.
void encode() {
    char *buf = malloc(BUF_SIZE);
    char *b64;
    char *cmd;
    int cmd_size;

    fget_str("Your string: ", buf, BUF_SIZE);

    // Encode the string into base64.
    b64 = base64(buf);

    // Blame Moss for this shit...
    strip_char(b64, 0x3d);
    strip_char(b64, 0xa);
    replace_char(b64, 0x2b, 0x20);
    cmd_size = strlen(b64) + strlen(FUN_CMD);
    cmd = malloc(cmd_size);
    snprintf(cmd, cmd_size, FUN_CMD, b64);
    system(cmd);
    printf("\n");

    // Free heap allocs.
    free(buf);
    free(b64);
    free(cmd);
}

// Encode a string.
void decode() {
    printf("Sorry... Moss don't want you to decode your strings :/\n");
}

int main() {
    int choice;
    char buf[sizeof(int)];

    setvbuf(stdout, NULL, _IONBF, 0);  // stdout is unbuffered.
    setvbuf(stderr, NULL, _IONBF, 0);  // stderr is unbuffered.

    do {
        printf(BANNER);
        printf("1 - Encode\n");
        printf("2 - Decode\n");
        printf("0 - Leave\n");
        choice = fget_int("Your choice:\n=> ", buf, sizeof(buf));

        switch (choice) {
            case 1:
                encode();
                break;
            case 2:
                decode();
                break;
            case 0:
                break;
            default:
                printf("Wrong choice!\n");
        }
    } while (choice != 0);

    return EXIT_SUCCESS;
}