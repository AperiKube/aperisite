#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Author: ENOENT

from emulator import Emulator

if __name__ == "__main__":
    compiled = open("binary.bin", "rb").read()
    try:
        emu = Emulator(compiled, debug=True)
        emu.run()
    except Exception as inst:
        x, y = inst.args
        print("{} : {}".format(x,y))
