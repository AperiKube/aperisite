import struct
import numpy as np
import math

key = ""
cipher = []
f = open("../files/crackme.exe","rb")

# extract key

l = [0x10890,0x108E0,0x10920,0x108B0,0x10880,0x10910,0x108F0,0x108C0,0x10870,0x108D0,0x108A0,0x10900]
for pos in l:
	f.seek(pos)
	d1 = f.read(8)
	d2 = f.read(8)
	x1 = struct.unpack("d",d1)[0]
	x2 = struct.unpack("d",d2)[0]
	key+=chr(int(x1))+chr(int(x2))
	
f.seek(0x10858)
data = f.read(8)
last_c = chr(int(struct.unpack("d",data)[0]))
key = key + last_c

l = [
	0x10950, # +00h
	0x10960, # +10h
	0x109A0, # +20h
	0x109B0, # +30h
	0x10940, # +40h
	0x10A10, # +50h
	0x109E0, # +60h
	0x10970, # +70h
	0x10980, # +80h
	0x10930, # +90h
	0x10A00, # +A0h
	0x109F0, # +B0h
	0x10A20, # +C0h
	0x10A30, # +D0h
	0x10990, # +E0h
	0x109D0, # +F0h
	0x109C0, # +100h
	0x10860, # +110h
	]
for pos in l:
	f.seek(pos)
	d1 = f.read(8)
	d2 = f.read(8)
	x1 = struct.unpack("d",d1)[0]
	x2 = struct.unpack("d",d2)[0]
	cipher.append(x1)
	cipher.append(x2)

flag=""
m = np.matrix(np.array(np.frombuffer(key,dtype=np.uint8),dtype=np.float32).reshape(5,5))
for i in range(0,35,5):
	message = np.matrix(np.array(cipher[i:i+5],dtype=np.float32).reshape(5,1))
	res = m.I.dot(message)
	for i in range(0,5):
		flag+=chr(int(round(res[i])))
print(flag)
	
	
