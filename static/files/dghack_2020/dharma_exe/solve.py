#!/usr/bin/env python3
import hashlib

from z3 import *

DEBUG = False

s = Solver()

inp = BitVec('inp', 64)

# make sure that the input is not negative and finish with 0x31337 in hexadecimal
s.add(And(inp >= 0))

# equation from stage2
s.add((0xc80 * (inp ^ 0xc80 & 0x42) / 0x68625361) - 0x6e334f32 == 0)
s.add(inp & 0xffff == 0xfb4c)

while s.check() == sat:  # while there's solution
    model = s.model()  # get solution
    valid_inp = model[inp].as_long()
    s.add(inp != valid_inp)  # make sure that we're not getting the same input value
    flag_candidate = hashlib.sha256(str(valid_inp).encode()).hexdigest()  # the flag corresponds to the sha256 hash digest of our input
    if DEBUG: print(f'{valid_inp} => {flag_candidate}')
    if flag_candidate.endswith('04440d69'):  # we know that the final flag ends with 04440d69 according to the challenge description
        print(f'Flag: {flag_candidate}')
        break