import idautils
import idc

def to_long(reg_dict):
    """Return 4 registers values to long."""
    keys = sorted(reg_dict.keys())
    long_val = 0
    long_val += reg_dict[keys[0]] << 0
    long_val += reg_dict[keys[1]] << 8
    long_val += reg_dict[keys[2]] << 16
    long_val += reg_dict[keys[3]] << 24
    return long_val

morse_translate_table = {200L: '.',
                         600L: '-',
                         400L: '/',
                         1200L: '\n'}

start_ea = 0x01b9
func_ea = idautils.ida_funcs.get_func(start_ea).start_ea
watched_regs = {i: None for i in range(22, 26, 1)}
last_called_func = None
last_called_func_arg = None
message = ''

for insn_ea in idautils.FuncItems(func_ea):  # for all instructions in the function.
    if insn_ea >= start_ea:  # we want to emulate instructions from the start_ea address.
        insn = idautils.DecodeInstruction(insn_ea)  # get the current instruction.
        insn_mnem = insn.get_canon_mnem()  # get mnemonic of the current instruction (e.g., call, add, ldi)
        insn_ops = insn.Operands  # get operands of the current instruction (e.g., registers, value, address)

        if insn_mnem == 'ldi':
            # load immediate value in reg register.
            reg = insn_ops[0].reg
            value = insn_ops[1].value
            if reg in watched_regs:
                watched_regs[reg] = value  # save register's loaded value.

        if insn_mnem == 'call':
            # call called_func address.
            called_func = idautils.ida_funcs.get_func_name(insn_ops[0].addr)

            if called_func == 'delay':
                called_func_arg = to_long(watched_regs)  # convert registers to a unsigned long integer.
                idc.set_cmt(insn_ea, 'delay(' + str(to_long(watched_regs)) + ');', 0)  # add regular comment to the current instruction.

                if ((last_called_func == 'digitalWrite' and last_called_func_arg == 'HIGH') or  # if the indicator is on, let's see how long we leave it on (. or -).
                    (last_called_func == 'delay')):                                             # two consecutive pause, this one probably indicates the next sequence (word or letter seperator).
                    message += morse_translate_table[called_func_arg]  # translate the duration to a morse code symbol.

                # save called_func and called_func_arg.
                last_called_func = called_func
                last_called_func_arg = called_func_arg

            if called_func == 'digitalWrite':  # change indicator state.
                called_func_arg_enum = {0: 'LOW', 1: 'HIGH'}  # human readable indicator's states.
                called_func_arg = called_func_arg_enum[watched_regs[24]]  # convert state to human readable value.

                idc.set_cmt(insn_ea, 'digitalWrite(LED_PIN, ' + called_func_arg + ');', 0)  # add regular comment to the current instruction.

                # save called_func and called_func_arg.
                last_called_func = called_func
                last_called_func_arg = called_func_arg

print(message)