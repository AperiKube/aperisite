#!/usr/bin/env python

from z3 import *

def sat_solve(value):
    # Get a line that allows us to get the chosen output value.

    max_line_size = 10
    s = Solver()

    line = [BitVec(f'line_{i}', 64) for i in range(max_line_size)]
    inter = [BitVec(f'inter_{i}', 64) for i in range(max_line_size)]
    out = BitVec('out', 64)

    for i in range(max_line_size):
        s.add(line[i] >= ord('0'))
        s.add(line[i] <= ord('9'))

        if i != 0:
            s.add(inter[i-1] <= 0x7fffffff)
            s.add(inter[i] == line[i] - 0x30 + 10 * inter[i-1])
        else:
            s.add(inter[i] == line[i] - 0x30 + 10 * 0)

    if value >= 0:
        s.add(inter[-1] == value)
        s.add(out == inter[-1])
    else:
        s.add(out == inter[-1]*-1)
        s.add(inter[-1] <= 0x80000000)
        s.add(out & 0xffffffff == value*-1)

    if (s.check() == sat):
        model = s.model()
        final = ''

        for i in range(max_line_size):
            curr_line_value = model[line[i]].as_long()
            curr_inter_value = model[inter[i]].as_long()
            final += chr(curr_line_value)
            print(f'signed_value: 0x{curr_inter_value:x} (current digit: 0x{curr_line_value:x})')

        print(f'Output: 0x{model[out].as_long():x}')
        final = str(int(final, 10))  # remove 0 padding
    else:
        print(f'unsat (value={value})')

    return final

sat_solve(-0xdeadbeef)
