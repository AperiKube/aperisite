# -*- coding:utf-8 -*-
from Tkinter import *
import struct

# tshark -r flag.pcapng -T fields -e frame.time_relative -e usb.capdata > flag.raw

f = open("flag.raw").read().split("\n")  # Array of time - data
f.pop()  # remove empty line

times = []
bytesl = [[] for x in range(20)]
for l in f:  # for each packet
    p1,p2 = l.split("\t")
    times.append(float(p1))  # add time of pack in an array
    sub = p2.split(":")
    for i in range(20):  # add each bytes in a specific array
        bytesl[i].append(sub[i])

fen = Tk()  # Initializing Tkinter Canvas 1200*800
screenWidth = 1200
screeHeight = 800
screen=Canvas(fen, bg="#d0d0d0", height=screeHeight, width=screenWidth)
screen.pack(expand=YES, fill=BOTH)

cursorX = screenWidth/2  # Set cursor at the middle of the screen
cursorY = screeHeight/2  # Set cursor at the middle of the screen

for i in range(len(bytesl[0])-1): # For each packet
    diff = times[i+1]-times[i] # Get time difference between current and next packet

    v1 = bytesl[7][i]  # Get bytes of x
    v2 = bytesl[6][i]  # Get bytes of x
    v3 = bytesl[9][i]  # Get bytes of y
    v4 = bytesl[8][i]  # Get bytes of y

    # Conversion
    # signed 16-bit, little-endian, north/east positive
    # x and y in [-32768;32768]   ==> [-((256**2)/2);(256**2)/2]
    # then divide by 32768 to resize interval in [-1;1]
    x =  float(struct.unpack('<h',chr(int(v2,16))+chr(int(v1,16)))[0])/32768.0
    y =  float(struct.unpack('<h',chr(int(v4,16))+chr(int(v3,16)))[0])/32768.0

    deadzone = 0.2
    velocityWeight = 300
    for t in range(int(velocityWeight*diff)): # Make multiple movement due to velocity
        if abs(x) >= deadzone:  # Check if we're not in deadzone
            cursorX += x
        if abs(y) >= deadzone:  # Check if we're not in deadzone
            cursorY -= y  #  - due to inversion of y (first flag, the image had mirror effect)

        if bytesl[3][i] != "00": # If "A" is pressed (see documentation)
            screen.create_rectangle(cursorX-1,cursorY-1,cursorX+1,cursorY+1,fill="black",outline="black") # Write Current Pixel

fen.mainloop()
