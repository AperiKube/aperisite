// Source : https://thiscouldbebetter.neocities.org/pixelarteditor.html

function main()
{
    var session = new Session
    (
        new Coords(32, 32), // imageSizeInPixelsActual
        16, // magnificationFactor
        // colors
        [
            "Black",
            "White",
            "Red",
            "Orange",
            "Yellow",
            "Green",
            "Blue",
            "Violet",
            "Brown",
            "Gray",
            "Tomato",
            "Turquoise",
            "Sienna",
            "Salmon",
            "PaleVioletRed", // Lol, wtf.
            "Olive",
            "Magenta",
            "Gold",
            "Indigo",
            "IndianRed",
            "FireBrick",
            "DarkKhaki",
            "CornflowerBlue"
        ]
    );

    session.initialize();
}

// classes

function Coords(x, y)
{
    this.x = x;
    this.y = y;
}
{
    Coords.prototype.clone = function()
    {
        return new Coords(this.x, this.y);
    }

    Coords.prototype.divide = function(other)
    {
        this.x /= other.x;
        this.y /= other.y;
        return this;
    }

    Coords.prototype.floor = function()
    {
        this.x = Math.floor(this.x);
        this.y = Math.floor(this.y);
        return this;
    }

    Coords.prototype.multiply = function(other)
    {
        this.x *= other.x;
        this.y *= other.y;
        return this;
    }

    Coords.prototype.multiplyScalar = function(scalar)
    {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

}

function Display(sizeInPixels)
{
    this.sizeInPixels = sizeInPixels;
}
{
    Display.prototype.drawImage = function(image)
    {
        this.graphics.drawImage
        (
            image,
            0, 0
        );
    }

    Display.prototype.drawImageStretched = function(image)
    {
        this.graphics.drawImage
        (
            image,
            0, 0,
            this.sizeInPixels.x, this.sizeInPixels.y
        );
    }

    Display.prototype.drawPixel = function(color, pos)
    {
        this.graphics.fillStyle = color;
        this.graphics.fillRect
        (
            pos.x, pos.y, 1, 1
        );
    }

    Display.prototype.drawRectangle = function(color, pos, size)
    {
        this.graphics.fillStyle = color;
        this.graphics.fillRect
        (
            pos.x, pos.y, size.x, size.y
        );
    }

    Display.prototype.drawRectangleM = function(color, pos, size)
    {
        this.graphics.fillStyle = color;
        this.graphics.fillRect
        (
            pos.x*16, pos.y*16, size.x*16, size.y*16
        );
    }

    Display.prototype.fillWithColor = function(color)
    {
        this.drawRectangle(color, new Coords(0, 0), this.sizeInPixels);
    }

    Display.prototype.initialize = function(domElementParent)
    {
        this.canvas = document.createElement("canvas");
        this.canvas.style = "border:1px solid;"
        this.canvas.width = this.sizeInPixels.x;
        this.canvas.height = this.sizeInPixels.y;

        domElementParent.appendChild(this.canvas);

        this.graphics = this.canvas.getContext("2d");
        this.graphics.imageSmoothingEnabled = false;

        this.fillWithColor("Red");

    }
}

function Session(imageSizeInPixelsActual, magnificationFactor, colors)
{
    this.imageSizeInPixelsActual = imageSizeInPixelsActual;
    this.magnificationFactor = magnificationFactor;
    this.colors = colors;
}
{
    // methods

    Session.prototype.initialize = function()
    {
        this.imageSizeInPixelsMagnified = this.imageSizeInPixelsActual.clone().multiplyScalar
        (
            this.magnificationFactor
        );

        this.cellSizeInPixels = new Coords(1, 1).multiplyScalar
        (
            this.magnificationFactor
        );

        this.inputFileName = 'image';
        this.inputFormat = 'JPG';

        var divMain = document.getElementById("divMain");
        divMain.innerHTML = "";


        var divImages = document.createElement("div");

        this.displayMagnified = new Display(this.imageSizeInPixelsMagnified);
        this.displayMagnified.initialize(divImages);
        this.displayMagnified.canvas.onmousemove =
            this.canvasMagnified_MouseMoved.bind(this);

        this.displayActualSize = new Display(this.imageSizeInPixelsActual);
        this.displayActualSize.initialize(divImages);

        divMain.appendChild(divImages);

        var divFileOperations = document.createElement("div");

        var labelName = document.createElement("label");
        labelName.innerHTML = "Filename:";
        divFileOperations.appendChild(labelName);

        var inputName = document.createElement("input");
        inputName.type = "text";
        inputName.value = this.inputFileName;
        inputName.maxLength = 45; // 50 - Len(Extension) - Filename will be truncated if len > 50
        inputName.onchange = this.inputName_Changed.bind(this);

        var labelFormat = document.createElement("label");
        labelFormat.innerHTML = "Format:";

        var inputFormat = document.createElement("select");
        inputFormat.onchange = this.inputFormat_Changed.bind(this);
        var availableFormats = ["JPG", "BMP", "PNG", "GIF"];

        for (var i = 0; i < availableFormats.length; i++) {
            var option = document.createElement("option");
            option.value = availableFormats[i];
            option.text = availableFormats[i];
            inputFormat.appendChild(option);
        }

        divFileOperations.appendChild(inputName);
        divFileOperations.appendChild(labelFormat);
        divFileOperations.appendChild(inputFormat);

        var buttonSave = document.createElement("button");
        buttonSave.innerHTML = "Save";
        buttonSave.onclick = this.buttonSave_Clicked.bind(this);
        divFileOperations.appendChild(buttonSave);

        divMain.appendChild(divFileOperations);

        var buttonClear = document.createElement("button");
        buttonClear.innerHTML = "Clear";
        buttonClear.onclick = this.buttonClear_Clicked.bind(this);
        divMain.appendChild(buttonClear);

        this.colorSelected = this.colors[0];

        var divColors = document.createElement("div");

        for (var i = 0; i < this.colors.length; i++)
        {
            var color = this.colors[i];
            var buttonColor = document.createElement("button");
            buttonColor.innerHTML = color;
            buttonColor.style.color = color;
            buttonColor.onclick = this.buttonColor_Clicked.bind(this);
            divColors.appendChild(buttonColor);
        }

        divMain.appendChild(divColors);

    }

    // ui events

    Session.prototype.buttonClear_Clicked = function()
    {
        this.displayMagnified.fillWithColor(this.colorSelected);
        this.displayActualSize.fillWithColor(this.colorSelected);
    }

    Session.prototype.buttonColor_Clicked = function(event)
    {
        var buttonColor = event.target;
        this.colorSelected = buttonColor.innerHTML;
    }

    Session.prototype.buttonSave_Clicked = function()
    {

        var canvas = this.displayActualSize.canvas;

        var ctx = canvas.getContext('2d');

        var imgdata = ctx.getImageData(0,0,32,32);

        var pixelarray = Array.from(imgdata.data);

        $.post("save.php", {'data': JSON.stringify(pixelarray), 'name': this.inputFileName + '.' + this.inputFormat, 'format': this.inputFormat}, function( res ){
            $('#divResult').html(res);
        });



        //  var link = document.createElement("a");
        // link.href = window.URL.createObjectURL(imageAsBlob);
        // link.download = "Image.png";
        // link.click();
    }

    Session.prototype.canvasMagnified_MouseMoved = function(event)
    {
        if (event.buttons == 0)
        {
            return;
        }

        var canvas = event.target;
        var canvasBounds = canvas.getBoundingClientRect();

        var clickPosInPixels = new Coords
        (
            event.clientX - canvasBounds.left,
            event.clientY - canvasBounds.top
        );

        var clickPosInCells = clickPosInPixels.clone().divide
        (
            this.cellSizeInPixels
        ).floor();

        var cellPosInPixels = clickPosInCells.clone().multiply
        (
            this.cellSizeInPixels
        );

        var color = this.colorSelected;

        this.displayMagnified.drawRectangle
        (
            color,
            cellPosInPixels,
            this.cellSizeInPixels
        );

        this.displayActualSize.drawPixel
        (
            color,
            clickPosInCells
        );
    }


    Session.prototype.inputName_Changed = function(event)
    {
        var inputName = event.target;
        this.inputFileName = inputName.value;
    }

    Session.prototype.inputFormat_Changed = function(event)
    {
        var inputName = event.target;
        this.inputFormat = inputName.value;
    }


}
