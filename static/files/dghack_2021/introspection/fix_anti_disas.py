import idc
import idaapi
import idautils

idc.ida_expr.compile_idc_text('static n_key() { exec_python("fix_anti_disas()"); }')

idc.add_idc_hotkey('ctrl-n', 'n_key')

def fix_anti_disas():
    """
    Basically search for the following anti-disassembly instruction set, nop it and fix function boundaries:

        call .+5
        add [rsp], val
        ret
        .db XX
        .db XX
        .db XX

    """
    # get function from cursor position.
    cursor_ea = idc.get_screen_ea()
    if idautils.ida_funcs.get_func(cursor_ea):
        # get current function boundaries.
        func_start_ea = idautils.ida_funcs.get_func(cursor_ea).start_ea
        func_end_ea = idautils.ida_funcs.get_func(cursor_ea).end_ea

        # loop through all instructions in the function.
        insn_ea = func_start_ea
        while insn_ea <= func_end_ea:
            print(f'Current ea: {insn_ea:#x}')

            # make sure we're working with instuction (even if we overlap with existing one).
            if not idc.create_insn(insn_ea):
                for ea in range(insn_ea, idc.next_head(insn_ea) + 1):
                    idc.ida_bytes.del_items(ea, 0, 1)
                idc.create_insn(insn_ea)

            # get mnemonic of the current instruction (e.g., call, add, jmp).
            insn_mnem = idautils.DecodeInstruction(insn_ea).get_canon_mnem()
            # get operands of the current instruction (e.g., registers, value, address).
            insn_ops = idautils.DecodeInstruction(insn_ea).ops

            if insn_mnem == 'call':
                print(f'Found a call at: {insn_ea:#x}')
                called_ea = insn_ops[0].addr

                # get the immediate next instruction address and operands.
                im_next_insn_ea = insn_ea + idc.get_item_size(insn_ea)
                im_next_insn_mnem = idautils.DecodeInstruction(im_next_insn_ea).get_canon_mnem()
                im_next_insn_op1 = idaapi.get_reg_name(idc.get_operand_value(im_next_insn_ea, 0), 8)
                im_next_insn_op2 = idc.get_operand_value(im_next_insn_ea, 1)

                if called_ea == im_next_insn_ea \
                    and im_next_insn_mnem == 'add' \
                    and im_next_insn_op1 == 'rsp':
                    print(f'Found anti-disas at {insn_ea:#x}')

                    # get the real instruction address.
                    im_next_insn_ea += im_next_insn_op2
                    print(f'Real next instruction {im_next_insn_ea:#x}')

                    # nop out intermediate bytes and convert them into code.
                    for ea in range(insn_ea, im_next_insn_ea):
                        idc.ida_bytes.patch_byte(ea, 0x90)
                        idc.create_insn(ea)
                        print(f'Nopped {ea:#x}')

                    insn_ea = im_next_insn_ea
                else:
                    insn_ea = insn_ea + idc.get_item_size(insn_ea)
            elif insn_mnem == 'retn':
                print(f'End at {insn_ea:#x}')
                break
            else:
                insn_ea = insn_ea + idc.get_item_size(insn_ea)

            # if necessary, change the function boundaries to cover the next instruction.
            if not idautils.ida_funcs.get_func(insn_ea):
                func_end_ea = insn_ea + idc.get_item_size(insn_ea)
                idaapi.set_func_end(func_start_ea, func_end_ea)
                print(f'New function boundaries: [{func_start_ea:#x} - {func_end_ea:#x}]')
    else:
        print(f'No function at {cursor_ea:#x}')

fix_anti_disas()