levels = [
    {
        'width': 21,
        'board': [
            '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8',
            '8', ' ', ' ', ' ', ' ', '8', '8', '8', ' ', ' ', ' ', ' ', '8', '8', '8', '8', ' ', ' ', '8', '8', '8',
            '8', ' ', '8', '8', 'o', ' ', '8', ' ', '8', '8', '8', '8', ' ', '8', '8', '8', ' ', ' ', '8', '8', '8',
            '8', ' ', '8', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', '8', '8', '8', ' ', '8', '8', ' ', '8', '8',
            '8', ' ', '8', '8', '8', ' ', '8', ' ', '8', '8', ' ', ' ', '8', '8', ' ', ' ', '8', '8', ' ', ' ', '8',
            '8', ' ', '8', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', ' ', '8', ' ', ' ', ' ', ' ', ' ', ' ', '8',
            '8', ' ', '8', '8', '0', ' ', '8', ' ', '8', '8', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', ' ', '8',
            '8', ' ', ' ', ' ', ' ', '8', '8', '8', ' ', ' ', ' ', ' ', '8', '8', ' ', '8', '8', '8', '8', ' ', '8',
            '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8'
        ]
    }, {
        'width': 17,
        'board': [
            '8', 'o', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8',
            '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', '8', ' ', '8',
            '8', ' ', '8', ' ', '8', ' ', '8', ' ', '8', '8', '8', ' ', '8', '8', ' ', ' ', '8',
            '8', '8', '8', '8', '8', '8', '8', ' ', '8', ' ', ' ', ' ', ' ', ' ', '8', '8', '8',
            '8', '8', ' ', '8', ' ', ' ', ' ', ' ', '8', ' ', '8', ' ', '8', ' ', ' ', '8', '8',
            '8', ' ', ' ', ' ', ' ', '8', ' ', '8', '8', ' ', '8', '8', '8', '8', ' ', ' ', '8',
            '8', '8', '8', '8', '8', '8', '8', '8', '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', '8',
            '8', '8', '8', ' ', '8', ' ', '8', ' ', '8', ' ', '8', '8', ' ', '8', '8', ' ', '8',
            '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', '8', ' ', '8', '8', '8', '8', '8', ' ', '8',
            '8', ' ', '8', ' ', '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8',
            '8', '8', '8', '8', '8', '8', ' ', '8', ' ', '8', '8', ' ', '8', ' ', '8', ' ', '8',
            '8', '8', ' ', '8', ' ', '8', ' ', '8', ' ', '8', '8', ' ', '8', ' ', '8', ' ', '8',
            '8', ' ', ' ', ' ', ' ', ' ', ' ', '8', ' ', '8', ' ', ' ', '8', '8', '8', ' ', '8',
            '8', ' ', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', ' ', '8', '8', ' ', ' ', '8',
            '8', '8', '8', ' ', ' ', '8', ' ', ' ', ' ', '8', '8', ' ', '8', '8', '8', ' ', '8',
            '8', '8', '8', '8', '8', '8', ' ', '8', '8', '8', ' ', ' ', ' ', '8', '8', ' ', '8',
            '8', ' ', ' ', '8', '8', ' ', ' ', ' ', ' ', '8', '8', ' ', '8', '8', ' ', ' ', '8',
            '8', '8', ' ', ' ', ' ', ' ', '8', ' ', '8', '8', ' ', ' ', ' ', '8', '8', ' ', '8',
            '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '0', '8'
        ]
    }, {
        'width': 19,
        'board': [
            '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8',
            '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', ' ', '8',
            '8', '8', '8', ' ', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', ' ', '8', ' ', '8',
            '8', '0', ' ', ' ', '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', ' ', '8', ' ', '8',
            '8', ' ', '8', '8', '8', '8', '8', 'o', '8', '8', '8', '8', '8', ' ', '8', ' ', '8', ' ', '8',
            '8', ' ', ' ', ' ', '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', ' ', '8', ' ', '8', ' ', '8',
            '8', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', '8', ' ', '8', ' ', '8', ' ', '8', ' ', '8',
            '8', ' ', ' ', ' ', '8', ' ', '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', ' ', ' ', '8',
            '8', ' ', '8', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', '8', '8', '8', '8', '8', ' ', '8',
            '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', '8',
            '8', ' ', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', ' ', '8', ' ', '8', '8', '8',
            '8', ' ', '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', '8',
            '8', ' ', '8', ' ', '8', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', '8', '8', '8', ' ', '8',
            '8', ' ', '8', ' ', '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8',
            '8', '8', '8', ' ', '8', ' ', '8', '8', '8', '8', '8', '8', '8', '8', '8', ' ', '8', '8', '8',
            '8', ' ', ' ', ' ', '8', ' ', ' ', ' ', '8', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', '8',
            '8', ' ', '8', '8', '8', ' ', '8', '8', '8', ' ', '8', '8', '8', ' ', '8', '8', '8', ' ', '8',
            '8', ' ', '8', ' ', ' ', ' ', '8', ' ', ' ', ' ', '8', ' ', '8', ' ', '8', ' ', '8', ' ', '8',
            '8', ' ', '8', '8', '8', '8', '8', '8', '8', '8', '8', ' ', '8', ' ', '8', ' ', '8', ' ', '8',
            '8', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '8', ' ', ' ', ' ', '8',
            '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8'
        ]
    }
]
