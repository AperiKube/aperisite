#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scapy.all import *
from scapy.utils import rdpcap

pcap_file = 'perdu.pcap'
packet_list = rdpcap(pcap_file)
sessions = packet_list.sessions()

flag = ''

for session in sessions:
    for packet in sessions[session]:
        if (TCP in packet and Raw in packet and packet[TCP].dport == 80 and 'GET / HTTP/1.1' in str(packet[Raw].load)):
            flag += chr(int(str(packet[TCP].sport)[1:]))

print(flag)