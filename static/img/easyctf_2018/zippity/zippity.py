import socket
import requests

# Socket connection

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('c1.easyctf.com', 12483))

# Making dictionnary from file "ZIP Code Tabulation Area"
# https://www.census.gov/geo/maps-data/data/gazetteer2010.html

with open("Gaz_zcta_national.txt","r") as f:
    l = f.readlines()
l = l[1:] # Remove first line (header)

dico = {} # dico[zipcode] = [Land Area, Water Area, Latitude, Longitude]
for elt in l:
    elt = elt.replace(" ","")
    elt = elt.replace("\r","")
    elt = elt.replace("\n","")
    m = elt.split("\t")
    dico[m[0]] = [m[3],m[4],m[7],m[8]] # Land Area, Water Area, Latitude, Longitude

# Dictionnary ready :)

rec = ""

while "easyctf" not in rec: # Keep answering until we get the flag
    rec = s.recv(2048).decode("utf-8") # Get response from server
    print(rec)
    
    rep = None # Set client response to None
    
    if "Round" in rec: # Get zipcode from servers response
        zipcode = rec[-7:-2]
        print("Zipcode: "+zipcode)
    
    # Answering questions:
    
    if " land area (m^2)" in rec: 
        rep = dico[zipcode][0] # Land Area
        print("Land Area of "+zipcode+" is "+rep+"m².")
    elif " water area (m^2)" in rec:
        rep = dico[zipcode][1] # Water Area
        print("Water Area of "+zipcode+" is "+rep+"m².")
    elif " latitude (degrees)" in rec:
        rep = dico[zipcode][2] # Latitude
        print("Latitude of "+zipcode+" is "+rep+".")
    elif " longitude (degrees)" in rec:
        rep = dico[zipcode][3] # Longitude
        print("Longitude of "+zipcode+" is "+rep+".")
    
    if rep is not None: # If there is a response to send
        s.send((rep+"\r\n").encode("utf-8")) # Send it
